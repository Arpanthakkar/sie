import IaiToken from '../../ethereum/BmtToken'
import BmtToken from '../../ethereum/BmtToken'
import FgmToken from '../../ethereum/FgmToken'
import OegToken from '../../ethereum/OegToken'
import web3 from '../web3'
import config from 'config'
import axios from 'axios'
const etherscanUrl = config.get('common.etherscanUrl')
const infuraKey = config.get('common.infuraKey')
const ethUtil = require('ethereumjs-util')
const { User, ContactUs,
} = require('../models/dbSchemas');
const bcrypt = require('bcryptjs');
const { setJWTToken } = require('../middleware/authToken');
const etherscanKey = config.get('common.etherscanKey')
var mongoose = require('mongoose');
var path = require('path');
var multer = require('multer');
const { BASEURL } = require('../config');
const { addMember } = require('../helpers/addMember');
const { VerifiedKycMail } = require('../helpers/verifyKycMail');
const { rejectedKycMail } = require('../helpers/rejectKycMail');

var moment = require('moment');

const { Sms, otpGenerate, ForgotMail, forgotPasswordNumberGenerate, RejectKycMail, } = require('../helpers/commonFunction');


const kycStatus = async data => {
    let res;
    try{
        if(data == 'notVerified' || data == 'pending')
        {
            res ='Pending'
        }
        if(data == 'reject')
        {
            res ='Not Verified'
        }
        if(data == 'verified')
        {
            res= 'Verified'
        }
        return res;
    }catch(err){

    }
};

exports.getTxList = async (address, tokenAdd) => {
    let tokenAddress = tokenAdd
    try {
        console.log('----', address);
        const contractAddress = config.get('common.' + tokenAddress)
        let txList = await axios(etherscanUrl + 'api?module=account&action=tokentx&contractaddress=' + contractAddress + '&address=' + address + '&page=1&offset=100&sort=asc&apikey=' + etherscanKey + '')
        let txnList = []
        txList = txList.data
        // console.log(txList)

        for (let i = 0; i < txList.result.length; i++) {

            let date = txList.result[i].timeStamp
            date = new Date(date * 1000)
            let details
            let fromAdd =await ethUtil.toChecksumAddress(txList.result[i].from)
			let toAdd =await  ethUtil.toChecksumAddress(txList.result[i].to)
            if (address == fromAdd) {
                details = 'sent'

            }
            if (address == toAdd) {
                details = 'received'

            }
            
            const respObj = {
                name: txList.result[i].tokenName,
                symbol: txList.result[i].tokenSymbol,
                quantity: Number.parseFloat(Number(txList.result[i].value) / (10 ** 8)).toFixed(8),
                txHash: txList.result[i].hash,
                date: moment(date).format("MM/DD/YYYY HH:mm"), 
                from: txList.result[i].from,
                to: txList.result[i].to,
                gasValue: txList.result[i].gasUsed,
                details: details,
                dateString: txList.result[i].timeStamp,
                userAddress: details == 'sent' ? txList.result[i].to : txList.result[i].from
            }
            txnList.push(respObj)
        }
        return txnList
    } catch (err) {
       // throw new Error(err)
    }
},

    exports.addKycUser = async (address, token) => {
        let tokenName = token
        try {
            let fromAddress = "0x2FF312a35e4bBD2158997D90CC81bC298059F2D1"
            let Token = require('../../ethereum/' + tokenName)
            // console.log(accounts)
            const kyc = await Token.methods.addKycUser(address).send({
                from: fromAddress,
                gas: 1000000,
                gasPrice: web3.utils.toHex(web3.utils.toWei('20', 'Gwei'))
            })
            console.log(kyc)
            return kyc
        } catch (err) {
            console.log(err)
            return false
        }
    },




    //<<<<<<<<<<<<<<<<<<<<<<........Admin Service Node Query..............>>>>>>>>>>>>>>>>>>>>

    exports.getDashboardData = async (req, response) => {
        try {

            let userCount = await User.count({ userRole: 'user' });

            console.log("USER Counter:::", userCount)
            if (userCount != null) {
                response.setStatus = 200;
                response.setMessage = "Number Of users";
                response.setData = { userCount };
                return true;
            } else {
                response.setStatus = 422;
                response.setMessage = "not user Exist";
                response.setErrorStack = "not user Exist";
                response.setData = { userCount };
                return false;
            }


        } catch (err) { console.log('Error in model', err) }
    }
exports.getMemberData = async (req, response) => {
    try {
        let userData = await User.find({ userRole: 'user' });
        return userData;
        // console.log("USER Counter:::", userData)
        // if (userData != null) {
        //     response.setStatus = 200;
        //     response.setMessage = "Number Of users";
        //     response.setData = { userData };
        //     return true;
        // } else {
        //     response.setStatus = 422;
        //     response.setMessage = "not user Exist details";
        //     response.setErrorStack = "not user Exist details";
        //     response.setData = { userData };
        //     return false;
        // }
    } catch (err) { console.log('Error in model', err) }
}

exports.userKycVerify = async (req, response, wallet='notWalletSend') => {
    try {
        console.log('req.body.userId...................', req.body);


        let conditions = {
            userRole: 'user',
            _id: mongoose.Types.ObjectId(req.body.userId)
        }

        var update = {}
        if (req.body.isApproved == 'true') {
            update = {
                "flags.kyc.isKyc": 'verified',
                "flags.kyc.status": true,
            }
        } else {
            update = {
                "flags.kyc.isKyc": 'reject',
                "flags.kyc.kycRejectReason": req.body.rejectReason,
                "flags.kyc.status": false,
            }
        }
        let userData = await User.findOneAndUpdate(conditions, update);


        if (userData) {
            let userDetails = await User.findOne({ _id: mongoose.Types.ObjectId(req.body.userId) });
      
            let email = userDetails.email;

            let Subject = 'Account verified – Congratulations!'
            let emailObj = {};
            if (req.body.isApproved == 'true') {
            emailObj = {
                firstName: userDetails.firstName,
                lastName: userDetails.lastName,
                baseurl: BASEURL,
                imgPath: BASEURL + '/assets/admin/img/right-sign.gif',
                imgURL: BASEURL + '/assets/user/img/logo-modal.png',
                mnemonic: 'wallet.mnemonic',
                walletAddress: 'wallet.walletAddress',
                email:userDetails.email,
                loginRoute : BASEURL +'/user/login',               
            }
        }else {
            // console.log("ooooooooooooooooo",userDetails.flags.kycUploadIdProof.isKyc)
            // console.log("ooooooooooooooooo",userDetails.flags.kycUploadAddressProof.isKyc)

            // console.log("ooooooooooooooooo",userDetails.flags.KycUploadWithSelfiePhoto.isKyc)
            let kycUploadIdProof                = await kycStatus(userDetails.flags.kycUploadIdProof.isKyc)
            let kycUploadAddressProof           = await kycStatus(userDetails.flags.kycUploadAddressProof.isKyc)
            let KycUploadWithSelfiePhoto        = await kycStatus(userDetails.flags.KycUploadWithSelfiePhoto.isKyc)         
            Subject = 'Your Blue Morpho Account – unable to verify identity'
            emailObj = {
                firstName: userDetails.firstName,
                lastName: userDetails.lastName,
                baseurl: BASEURL,
                imgPath: BASEURL + '/assets/admin/img/right-sign.gif',
                imgURL: BASEURL + '/assets/user/img/logo-modal.png',
                mnemonic: null,
                walletAddress: null,
                reason: req.body.rejectReason,
                email:userDetails.email,
                loginRoute : BASEURL +'/user/login',
                kycUploadAddressProof : kycUploadAddressProof,
                kycUploadIdProof : kycUploadIdProof,
                KycUploadWithSelfiePhoto : KycUploadWithSelfiePhoto
            }
        }
        if (req.body.isApproved == 'true') {
            var emailSend = await VerifiedKycMail(email, Subject, emailObj);
        }else{
            console.log(".emailObj............",emailObj)
            await rejectedKycMail(email, Subject, emailObj);
        }
            
            response.setStatus = 200;
            response.setMessage = "KYC status changed successfully";
            response.setData = { userDetails };
            return true;
        } else {
            response.setStatus = 422;
            response.setMessage = "KYC status not changed.";
            response.setErrorStack = "KYC status not changed.";
            return false;
        }
    } catch (err) { console.log('Error in model', err) }
}


exports.documentStatus = async (req, response) => {
    try {
        console.log('Admin servic document', req.body)

        let conditions = {
            userRole: 'user',
            _id: mongoose.Types.ObjectId(req.body.userId)
        }

        let update = {};
        let uploadDocFlag = "flags." + req.body.uploadData + ".isKyc";
        let uploadDocStatus = "flags." + req.body.uploadData + ".status";
        let uploadDocRejectReason = "flags." + req.body.uploadData + ".kycRejectReason";
        console.log('uploadDocFlag...............',req.body.isApproved);

        //Declare Variables.
        let kycUploadIdProof                
        let kycUploadAddressProof           
        let KycUploadWithSelfiePhoto        


        if (req.body.isApproved == 'false' || req.body.isApproved == false) {
            if (req.body.uploadData == 'kycUploadIdProof') {

                let userData = await User.findOne({ userRole: 'user', _id: mongoose.Types.ObjectId(req.body.userId) });
                kycUploadIdProof                = 'Not Verified'
                kycUploadAddressProof           = await kycStatus(userData.flags.kycUploadAddressProof.isKyc)
                KycUploadWithSelfiePhoto        = await kycStatus(userData.flags.KycUploadWithSelfiePhoto.isKyc)         
                
                let email = userData.email;
                let Subject = 'Your Blue Morpho Account – unable to verify identity'
                let emailObj = {
                    firstName: userData.firstName,
                    lastName: userData.lastName,
                    idProof: userData.kycDetails.chooseId,
                    reason: req.body.rejectReason,
                    imgPath: BASEURL + '/assets/admin/img/cancel.png',
                    imgURL: BASEURL + '/assets/user/img/logo-modal.png',
                    kycUploadAddressProof : kycUploadAddressProof,
                    kycUploadIdProof : kycUploadIdProof,
                    KycUploadWithSelfiePhoto :KycUploadWithSelfiePhoto, 
                    loginRoute : BASEURL +'/user/login',
                    email :email
                }
                var emailSend = await RejectKycMail(email, Subject, emailObj);
                console.log(emailSend)
            } else if (req.body.uploadData == 'kycUploadAddressProof') {
                let userData = await User.findOne({ userRole: 'user', _id: mongoose.Types.ObjectId(req.body.userId) });
                kycUploadIdProof                = await kycStatus(userData.flags.kycUploadIdProof.isKyc)
                kycUploadAddressProof           = 'Not Verified'
                KycUploadWithSelfiePhoto        = await kycStatus(userData.flags.KycUploadWithSelfiePhoto.isKyc)

                let email = userData.email;
                let Subject = 'Your Blue Morpho Account – unable to verify identity'
                let emailObj = {
                    firstName: userData.firstName,
                    lastName: userData.lastName,
                    idProof: userData.kycDetails.chooseAddressId,
                    reason: req.body.rejectReason,
                    imgPath: BASEURL + '/assets/admin/img/cancel.png',
                    imgURL: BASEURL + '/assets/user/img/logo-modal.png',
                    kycUploadAddressProof : kycUploadAddressProof,
                    kycUploadIdProof : kycUploadIdProof,
                    KycUploadWithSelfiePhoto :KycUploadWithSelfiePhoto, 
                    loginRoute : BASEURL +'/user/login',
                    email :email
                }
               // console.log('emailObj.....................', emailObj)
                var emailSend = await RejectKycMail(email, Subject, emailObj);
                console.log(emailSend)
            } else {
                let userData = await User.findOne({ userRole: 'user', _id: mongoose.Types.ObjectId(req.body.userId) });
                kycUploadIdProof                = await kycStatus(userData.flags.kycUploadIdProof.isKyc)
                kycUploadAddressProof           = await kycStatus(userData.flags.kycUploadAddressProof.isKyc)
                KycUploadWithSelfiePhoto        = 'Not Verified'

               
                let con = 'Selfie with Holding Document';
                let email = userData.email;
                let Subject = 'Your Blue Morpho Account – unable to verify identity';
                let emailObj = {
                    firstName: userData.firstName,
                    lastName: userData.lastName,
                    idProof: con,
                    reason: req.body.rejectReason,
                    imgPath: BASEURL + '/assets/admin/img/cancel.png',
                    imgURL: BASEURL + '/assets/user/img/logo-modal.png',
                    kycUploadAddressProof : kycUploadAddressProof,
                    kycUploadIdProof : kycUploadIdProof,
                    KycUploadWithSelfiePhoto :KycUploadWithSelfiePhoto, 
                    loginRoute : BASEURL +'/user/login',
                    email :email
                }

                var emailSend = await RejectKycMail(email, Subject, emailObj);
                console.log(emailSend)
            }

        }



        if (req.body.isApproved == "true") {
            update = {
                [uploadDocStatus]: true,
                [uploadDocFlag]: 'verified',
                [uploadDocRejectReason]: null
            }
        } else {
            update = {
                [uploadDocFlag]: 'reject',
                [uploadDocStatus]: false,
                [uploadDocRejectReason]: req.body.rejectReason,
            }
        }

        let userData = await User.findOneAndUpdate(conditions, update);

        console.log("update", userData)
        if (userData != 0) {

            response.setStatus = 200;
            response.setMessage = "User KYC Reject Reason Sent ";
            response.setData = {};

            return true;
        } else {
            response.setStatus = 422;
            response.setMessage = "User KYC Reject Reason already Sent ";
            response.setErrorStack = "User KYC Reject Reason already  Sent";
            return false;
        }
    } catch (err) { console.log('Error in model', err) }
}



exports.userDataFetch = async (req, response) => {
    try {
        let userData = await User.findOne({ userRole: 'admin', email: req.session.userDetails.email });
        console.log("USER Counter:::", userData)
        if (userData != null) {
            response.setStatus = 200;
            response.setMessage = "Admin Profile data";
            response.setData = { userData };
            return true;
        } else {
            response.setStatus = 200;
            response.setMessage = "no data available ";
            response.setErrorStack = "no data available";
            response.setData = { userData };
            return false;
        }

    }
    catch (err) {
        console.log('error.........', err)
    }
};
exports.userDetailsUpdate = async (req, response) => {
    try {
        var img = '';


        if (req.files.profilePicture == undefined) {
            img = req.session.userDetails.profileImage;
        } else {
            img = req.files.profilePicture[0].filename;
        }

        var userUpdateData = await User.updateOne({
            email: req.session.userDetails.email
        }, {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            profileImage: img
        });

        console.log(userUpdateData.nModified)
        if (userUpdateData.nModified) {
            response.setStatus = 200;
            response.setMessage = "Your Data Successfully Updated ";

            return true;
        } else {
            response.setStatus = 200;
            response.setMessage = "Not any update yet";
            response.setErrorStack = "Not any update yet";
            return false;
        }
    } catch (err) { console.log("error..........", err) }
};

exports.changePass = async (req, tokenEmail, response) => {

    try {
        let oldPassword = req.body.oldPassword;
        console.log("TOKEN EMAIL", tokenEmail)
        let newPassword = req.body.conPassword
        const userDetails = await User.findOne({ email: tokenEmail });
        if (oldPassword != newPassword) {
            if (userDetails) {
                let userPassword = await bcrypt.compare(oldPassword, userDetails.password);
                if (userPassword) {
                    let salt = await bcrypt.genSalt(10, newPassword);
                    let hash = await bcrypt.hash(newPassword, salt);
                    var { nModified } = await User.updateOne(
                        { email: userDetails.email },
                        { password: hash });
                    if (nModified) {
                        response.setStatus = 200;
                        response.setMessage = 'Successfully Changed';
                        return true;
                    } else {
                        response.setStatus = 422;
                        response.setMessage = 'Password Changed';
                        response.setErrorStack = 'Password Changed';
                    }
                } else {
                    response.setStatus = 422;
                    response.setMessage = 'Your Old Password Was Wrong';
                    response.setErrorStack = 'Your Old Password Was Wrong';
                }
            }
        } else {
            response.setStatus = 422;
            response.setMessage = 'New password can not be same as old password.';
            response.setErrorStack = 'New password can not be same as old password.';
        }

    } catch (err) {
        console.log('error', err)

    }

};

exports.userDetail = async (req, response) => {
    try {
        const userData = await User.findOne({ _id: mongoose.Types.ObjectId(req.body.userId) })
        if (userData) {
            response.setStatus = 200;
            response.setMessage = 'User Data';
            response.setData = userData;
        } else {
            response.setStatus = 422;
            response.setMessage = 'User not exist';
            response.setData = userData;

        }
    }
    catch (err) {
        console.log('Error in userDetail model', err)
    }
};

exports.userKycStatusData = async (data) => {
    try {
        const userData = await User.find({ 'flags.kyc.isKyc': data });

        return userData
        // if (userData) {
        //     response.setStatus = 200;
        //     response.setMessage = 'Kyc status data';
        //     response.setData = userData;
        // } else {
        //     response.setStatus = 422;
        //     response.setMessage = 'Kyc data not yet';
        //     response.setData = userData;

        // }
    }
    catch (err) {
        console.log('Error in userDetail model', err)
    }
}

exports.insertUser = async (registerRecord, wallet, response) => {
    let recordInset = new User(registerRecord);
    let dbStatus = await recordInset.save();
    if (dbStatus) {
        let email = registerRecord.email;
        let Subject = 'Your Blue Morpho Account Created'
        let emailObj = {
            firstName: registerRecord.firstName,
            lastName: registerRecord.lastName,
            email: registerRecord.email,
            password: registerRecord.tempField,
            imgURL: BASEURL + '/assets/user/img/logo-modal.png',
            mnemonic: wallet.mnemonic,
            walletAddress: wallet.walletAddress
        }

        var emailSend = await addMember(email, Subject, emailObj);
        console.log(emailSend);
        response.setResponse = {
            status: 200,
            message: 'Successfully Registered User',
            error_stack: [],
            data: dbStatus
        };
        return true;
    } else {
        response.setResponse = {
            status: 500,
            message: 'User Registration Failed',
            error_stack: "User Registration Failed",
            data: {}
        };
        return false;
    }
};

exports.contactUsDetails = async (req, response) => {

    // let update={

    //  }
    //  let recordInset = new ContactUs(data);
    //  let dbStatus = await recordInset.save();

    let dbStatus = await ContactUs.update(
        {},
        {
            contactUsDetails: req.body.contactUsData
        });
    console.log('update data', dbStatus);
    if (dbStatus) {

        response.setResponse = {
            status: 200,
            message: 'Successfully store Data',
            error_stack: [],
            data: dbStatus
        };
        return true;
    } else {
        response.setResponse = {
            status: 500,
            message: 'User Registration Failed',
            error_stack: "User Registration Failed",
            data: {}
        };
        return false;
    }
};
exports.contactUsDetailsView = async (req, response) => {
    try {
        let contactData = await ContactUs.find();

        if (contactData != null) {
            response.setStatus = 200;
            response.setMessage = "Contact Us page Data";
            response.setData = { contactData };
            return true;
        } else {
            response.setStatus = 422;
            response.setMessage = "No data available";
            response.setErrorStack = "No data available";
            response.setData = { userData };
            return false;
        }
    } catch (err) { console.log('Error in model', err) }
}


exports.getUserDetail = async (userId) => {
    try {
        const userData = await User.findById(userId)
        return userData
    }
    catch (err) {
        console.log('Error in userDetail model', err)
        throw err
    }
};


exports.getUserWallet = async (userId) => {
    try {      
       const user=  User.findOne({ _id:userId }, 'wallet');
       return user;
    }
    catch (err) {
        console.log('Error in userDetail model', err)
        throw err
    }
};

