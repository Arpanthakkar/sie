const { User, ContactUs
} = require('../models/dbSchemas');
const bcrypt = require('bcryptjs');
const { setJWTToken } = require('../middleware/authToken');
const { Sms, otpGenerate, ActivateMail, ForgotMail, forgotPasswordNumberGenerate } = require('../helpers/commonFunction');
exports.userDetails = async (userId, response) => {
    try {
        console.log(userId)
        let details = await User.findOne({
            _id: userId
        });
        console.log("USER DETAILS:::", details)
        if (details != null) {
            response.setStatus = 200;
            response.setMessage = "User Data";
            response.setData = details;
            return true;
        } else {
            response.setStatus = 422;
            response.setMessage = "not find data";
            response.setErrorStack = "not find data";
            return false;
        }
    } catch (err) {
        console.log('error', err)
    }

},
exports.kycUserDetails = async (req,tokenEmail, kyc, response) => {
        try {

       
            let kycUploadAddressProofStatus =(req.body.addressProofFileType == 'name')? 'verified':'pending';
            let idProofFileTStatus =(req.body.idProofFileType == 'name')? 'verified':'pending';
            let withSelfiePhotoIdStatus =(req.body.withSelfiePhotoIdFileType == 'name')? 'verified':'pending';
            // let kycStatus =flags.isKyc
       
            var responseData = await User.updateOne({
                email: tokenEmail
            }, {
                kycDetails: kyc,
                "flags.kyc.isKyc": 'pending',
                "flags.kycUploadAddressProof.isKyc":kycUploadAddressProofStatus,
                
                "flags.kycUploadIdProof.isKyc":idProofFileTStatus,
              
                "flags.KycUploadWithSelfiePhoto.isKyc": withSelfiePhotoIdStatus,
            });

            if (responseData.nModified) {
                response.setStatus = 200;
                response.setMessage = "Your Documents Successfully Uploaded ";

                return true;
            } else {
                response.setStatus = 422;
                response.setMessage = "Something wrong while upload data";
                response.setErrorStack = "Something wrong while upload data";
                return false;
            }
        } catch (err) { console.log('error', err) }

    };
exports.userDetailsUpdate = async (req, tokenEmail, response) => {
    try {
        console.log("TOKEN EMAIL", tokenEmail)
        var img = '';
        if (req.files.profilePicture == undefined) {
            img = req.session.userDetails.profileImage;
        } else {
            img = req.files.profilePicture[0].filename;
        }
        
        var responseData = await User.findOneAndUpdate({
            email: tokenEmail
        }, {
            firstName: req.body.fname,
            lastName: req.body.lname,
           
            profileImage: img,
            address:req.body.address,
            country: req.body.country,
            city: req.body.city,
            zipCode: req.body.zipCode,
            dob: req.body.dob,
        });
        if(req.body.phoneNumber == responseData.phoneNumber){
            console.log("both same ")
        }else{
            console.log("both not same ")
        }
        console.log("responseData...", responseData)
        if (responseData!=0) {
            response.setStatus = 200;
            response.setMessage = "Your Data Successfully Updated ";

            return true;
        } else {
            response.setStatus = 422;
            response.setMessage = "No any data update";
            response.setErrorStack = "No any data update";
            return false;
        }
    } catch (err) {
        console.log('error', err)
    }
};
exports.changePass = async (req, tokenEmail, response) => {

    try {
        let oldPassword = req.body.oldPassword;
        console.log("TOKEN EMAIL", tokenEmail)
        let newPassword = req.body.conPassword
        const userDetails = await User.findOne({ email: tokenEmail });
        if (oldPassword != newPassword) {
            if (userDetails) {
                let userPassword = await bcrypt.compare(oldPassword, userDetails.password);
                if (userPassword) {
                    let salt = await bcrypt.genSalt(10, newPassword);
                    let hash = await bcrypt.hash(newPassword, salt);
                    var { nModified } = await User.updateOne(
                        { email: userDetails.email },
                        { password: hash });
                    if (nModified) {
                        response.setStatus = 200;
                        response.setMessage = 'Successfully Changed';
                        return true;
                    } else {
                        response.setStatus = 422;
                        response.setMessage = 'Password Not Changed';
                        response.setErrorStack = 'Password Not Changed';
                    }
                } else {
                    response.setStatus = 422;
                    response.setMessage = 'Your Old Password is Wrong';
                    response.setErrorStack = 'Your Old Password is Wrong';
                }
            }
        } else {
            response.setStatus = 422;
            response.setMessage = 'New password can not be same as old password.';
            response.setErrorStack = 'New password can not be same as old password.';
        }
    } catch (err) {
        console.log('error', err)
    }
};
exports.agreement = async (email, response) => {

    try {
        let isKycUpload = 1;

        let { nModified } = await User.updateOne({
            email: email
        }, {
            'flags.agreement': true,
        });
        console.log("changes", nModified)
        if (nModified) {
            isKycUpload = 1
            response.setStatus = 200;
            response.setMessage = 'Successfully Upload KYC';

        } else {
            isKycUpload = 0;
            response.setStatus = 200;
            response.setData = { isKycUpload };
            response.setMessage = 'KYC Already uploaded wait for your approve';
            response.setErrorStack = 'KYC Already uploaded wait for your approve';
            return;
        }
    } catch (err) { console.log('error', err) }
};
exports.forgotTokenCheck = async (token, conPassword, response) => {
    try {
        let details = await User.findOne({
            "forgotEmailTokenDetails.emailToken": token
        });


        if (details != null) {
            var nowDataTime = new Date().getTime();

           
            if (details.forgotEmailTokenDetails.emailTokenExpire > nowDataTime) {

                let salt = await bcrypt.genSalt(10);
                let hash = await bcrypt.hash(conPassword, salt);
                let { nModified } = await User.updateOne({
                    email: details.email
                }, {
                    password: hash,
                });
                response.setStatus = 200;
                response.setMessage = "Your Password Successfully Changed";
                return true;
            } else {
                response.setStatus = 422;
                response.setMessage = "Your Email Token is Expire Please try again";
                response.setErrorStack = "Your Email Token is Expire Please try again";
                // response.setData = details;
                return false;
            }

        } else {
            response.setStatus = 422;
            response.setMessage = "Invalid Token.";
            response.setErrorStack = "Invalid Token.";
            return false;
        }
    } catch (err) {
        console.log('Error...................', err);
    }
}
exports.changeNumber = async (req, response) => {
    try {
        let countryCode=req.body.countryCode;
        let phn = req.body.phoneNumber;
        let oldDateObj = new Date();
        let newDateObj = new Date();
        const otpGenerated = await otpGenerate();
        if(phn != req.session.userDetails.phoneNumber){
            let { nModified } = await User.updateOne({
            email: req.session.userDetails.email
        }, {
            otpExpire: newDateObj.setTime(oldDateObj.getTime() + (5 * 60 * 1000)),
            tempField:phn,
            otpMobile :otpGenerated,
            tempField2:countryCode,
        });
        var no = countryCode + '' + phn;
    
        const smsGenerate=await Sms(otpGenerated, no);
        console.log(otpGenerated);
        
        
        if (nModified) {
           
            response.setStatus = 200;
            response.setMessage = 'Check please verify your number';

        } else {
            response.setMessage = 'your phone number not update';
            response.setErrorStack = 'your phone number not update';
            response.setStatus = 422;   
        }
        }else{
            response.setMessage = 'This number already verified, Please enter different phone number';
            response.setErrorStack = 'This number already verified, Please enter different phone number';
            response.setStatus = 422;
        }
        
    } catch (err) { console.log('error', err) }
};
exports.changeNumberCheck=async(req,response)=>{
    try {

        let otpCheckFlag = await User.findOne({
      //    otpMobile: req.body.otp,
          email: req.session.userDetails.email
        });
        if (otpCheckFlag != null) {
     
      //    var oldTime = otpCheckFlag.otpExpire;
     
       //   var d1 = oldTime;
     //     var d2 = new Date().getTime();
        
          //if (d1 > d2) {
               var responseData = await User.updateOne(
              {email: req.session.userDetails.email },
              {   phoneNumber:req.body.phoneNumber,
                   tempField:'', 
                   countryCode:req.body.countryCode,
                   tempField2:'',
              }
            );
            req.session.userDetails.phoneNumber=req.body.phoneNumber;
            req.session.userDetails.countryCode=req.body.countryCode;
            response.setStatus = 200;
            response.setMessage = 'Successful Changed your phone number';
           // response.setData = '';
            return true;
        //   } else {
    
        //     response.setStatus = 422;
        //     response.setMessage = 'mobile number is not updated.';
        //     response.setErrorStack = 'mobile number is not updated.';
        //   }
        } else {
          response.setStatus = 422;
          response.setMessage = ' Mobile number is not updated.';
          response.setErrorStack = 'Mobile number is not updated.';
          return false;
        }
      }
      catch (err) {
        console.log('error', err);
      }
}
exports.getUserDetails = async userId => {
    try {
        console.log(userId)
        let details = await User.findOne({
            _id: userId
        });
        return details
    } catch (err) {
        console.log('error', err)
        throw err
    }
}


exports.contactUsDetailsView = async (req, response) => {
    try {
        let contactData = await ContactUs.find();
      
        if (contactData != null) {
            response.setStatus = 200;
            response.setMessage = "Contact Us page Data";
            response.setData = { contactData };
            return true;
        } else {
            response.setStatus = 422;
            response.setMessage = "No data available";
            response.setErrorStack = "No data available";
            response.setData = { userData };
            return false;
        }
    } catch (err) { console.log('Error in model', err) }
}

exports.updateUser = async ( userId, wallet  ) => {
  try {
    await User.findByIdAndUpdate(
          userId,
          {
            $set: { wallet }  
          }
      )
  } catch (err) {
      throw err
  }
}
 
exports.userSuspend = async ( userId ) => {
    try {
    let data = await User.findByIdAndUpdate(
            userId,
            {
              $set: {
                  'flags.block_user': true,
                  'flags.kyc.isKyc': 'suspend'
              }  
            }
        )
        console.log('data......',data)
        return data.flags.block_user

    } catch (err) {
        throw err
    }
  }

  exports.changeNumberSignUp = async (req, response) => {
    try {
        let countryCode=req.body.countryCode;
        let phn = req.body.phoneNumber;
        let email = req.body.email
        let oldDateObj = new Date();
        let newDateObj = new Date();
        const otpGenerated = await otpGenerate();
        // if(phn != req.session.userDetails.phoneNumber){
            let { nModified } = await User.updateOne({
                email: email
        }, {
            otpExpire: newDateObj.setTime(oldDateObj.getTime() + (5 * 60 * 1000)),
            tempField:phn,
            otpMobile :otpGenerated,
            tempField2:countryCode,
        });
        var no = countryCode + '' + phn;
    
        const smsGenerate=await Sms(otpGenerated, no);
        console.log(otpGenerated);
        
        
        if (nModified) {
           
            response.setStatus = 200;
            response.setMessage = 'Check please verify your number';

        } else {
            response.setMessage = 'your phone number not update';
            response.setErrorStack = 'your phone number not update';
            response.setStatus = 422;   
        }
        // }else{
        //     response.setMessage = 'This number already verified, Please enter different phone number';
        //     response.setErrorStack = 'This number already verified, Please enter different phone number';
        //     response.setStatus = 422;
        // }
        
    } catch (err) { console.log('error', err) }
};

exports.getUserType = async data => {
    try {

        const userData1 = await User.findOne({tempField: data});
        return userData1
    } catch (err) {
        console.log('err........',err)
        throw err
    }
}
exports.updateSecretQRCode = async ( userId, secretCode  ) => {   
    try {
        const res = await User.findByIdAndUpdate(
            userId,
            {
                $set: { secretQRCode : secretCode }  
            }
        )
        return res
    } catch (err) {
        throw err
    }
  }
  exports.changeAuthenticatorFlag = async ( userId) => {   
    try {
        const res = await User.findByIdAndUpdate(
            userId,
            {
                $set: { isGoogleAuthorised : true ,isUserLoggedIn :false }  
            }
        )
        return res
    } catch (err) {
        throw err
    }
  }

  exports.logoutUser = async userId => {
  try {

    const data = await User.findByIdAndUpdate(
        userId,
        {
            $set: { isGoogleAuthorised : false }  
        }
    )

    return data
  } catch (err) {
      throw err
  }
  }