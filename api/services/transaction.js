import tokenService from './token.service'
import adminService from '../services/admin.service'
import { handleResponse, handleError } from '../middleware/responsehandler'
import web3 from '../web3'

module.exports = {
    getTnxHistory: async (address) => {
        try {
         
            const bmtTxList = tokenService.getTxList(address, "BmtTokenAddress")
            const fgmTxList = tokenService.getTxList(address, "FgmTokenAddress")
            const iaiTxList = tokenService.getTxList(address, "IaiTokenAddress")
            const oegTxList = tokenService.getTxList(address, "OegTokenAddress")
            let tnxHistory = await Promise.all([bmtTxList, fgmTxList, iaiTxList, oegTxList]).then(data => {

         
                let abc = []
                data.forEach(element => {
                    element.forEach(ele => {
                        abc.push(ele)
                    })
                })               
                return abc
            })
            return tnxHistory

        } catch (err) {
            console.log("ERROR => ", err)
            return false
        }
    },
    getSentToken: async (address,token) => {
        let tokenName = token
        try {
            const txList =await tokenService.getTxList(address, tokenName)
          
            let amount = 0
            for (let i in txList) {
                if (txList[i].details == 'sent') {
                    amount += Number(txList[i].quantity)
                }
            }
            return Number.parseFloat(amount).toFixed(8)
        } catch (err) {
            return err
        }
    },
    getReceiveToken: async (address,token) => {
        let tokenName = token
        try {
            const txList = await tokenService.getTxList(address, tokenName)
            let amount = 0
            for (let i in txList) {
                if (txList[i].details == 'received') {
                    amount += Number(txList[i].quantity)
                }
            }
            return Number.parseFloat(amount).toFixed(8)
        } catch (err) {
            return err
        }
    },
    getBurnedToken: async (address,token) => {
        let tokenName = token
        try {
            console.log('in burn token ... . . . . .', tokenName)
            const txList = await adminService.getTxList(address, tokenName)
            let amount = 0
            for (let i in txList) {
                if (txList[i].to == '0x0000000000000000000000000000000000000000') {
                    amount += Number(txList[i].quantity)
                }
            }
            return amount
        } catch (err) {
            return err
        }
    },
    getEtherBalance:async(address) => {
        try{
            const balance = await web3.eth.getBalance(address)
            const etherBalance = web3.utils.fromWei(balance.toString(),'ether')
        
            return etherBalance
        }catch(err){
        
            return err
        }
    },
    tokenTransactionHistory: async(address) => {
        try{
            const bmtTxList = adminService.getTxList(address,"BmtTokenAddress")
            const oegTxList = adminService.getTxList(address,"OegTokenAddress")
            const fgmTxList = adminService.getTxList(address,"FgmTokenAddress")
            const iaiTxList = adminService.getTxList(address,"IaiTokenAddress")
            let txHistory = await Promise.all([bmtTxList,fgmTxList,oegTxList,iaiTxList]).then(data => {
                console.log(data)
                let abc = []
                data.forEach(element => {
                    element.forEach(ele => {
                        abc.push(ele)
                    })
                })
                return abc

            })
            return txHistory

        }catch(err){
            return err
        }
    },
   
}