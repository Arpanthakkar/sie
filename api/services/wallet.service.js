const ethUtil = require('ethereumjs-util');
import web3 from '../web3'
const bip39 = require('bip39')
const hdkey = require('hdkey')
const Web2 = require('web3')
const config = require('config')
const infuraUrl = config.get('common.infuraUrl')
const infuraKey = config.get('common.infuraKey')
const HDWalletProvider = require("@truffle/hdwallet-provider");
const { getUserDetails } = require('./providerService')


exports.createWallet = async(req,res) => {
    try{
        const mnemonic = bip39.generateMnemonic() //generates string
      
        const seed = await bip39.mnemonicToSeed(mnemonic)//creates seed buffer
        const root = hdkey.fromMasterSeed(seed)
        const masterPrivateKey = root.privateKey.toString('hex')
        const addrNode = root.derive("m/44'/60'/0'/0/0");
        const pubKey = ethUtil.privateToPublic(addrNode._privateKey);
        const addr = ethUtil.publicToAddress(pubKey).toString('hex');
        const address = ethUtil.toChecksumAddress(addr);
      

        const respobj = {
            mnemonic:mnemonic,
            walletAddress:address
        }

        return respobj

    }catch(err){
        return err
    }
}

exports.verifyMnemonic = async req  => {
    // let mnemonic = req.body.mnemonic
    try{
        let getUser = await getUserDetails(req.session.userDetails._id)
        let userWallet = getUser.wallet        

        let mnemonic = req.body.mnemonic
        const provider = new HDWalletProvider(
          mnemonic,
          infuraUrl+infuraKey
        );
        const web2 = new Web2(provider)
        console.log('web2',web2)
        const accounts = await web2.eth.getAccounts()
        console.log("accounts.........",accounts)
        if(accounts[0] == userWallet){
            return true
        }else{
            return false
        }
    }catch(err){
        console.log('err..........',err)
        return false
    }
  }