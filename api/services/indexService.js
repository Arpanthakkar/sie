const { User, ForgotPass } = require('../models/dbSchemas');
const md5 = require('md5');
const bcrypt = require('bcryptjs');
const {ActivateMail}= require('../helpers/activateAccount');
const { Sms, otpGenerate, forgotPasswordNumberGenerate } = require('../helpers/commonFunction');
const {ForgotMail} = require('../helpers/forgotEmail');

const { setJWTToken } = require('../middleware/authToken');
const { BASEURL } = require('../config');

exports.insertUser = async (registerRecord, response) => {
  let recordInset = new User(registerRecord);
  let dbStatus = await recordInset.save();
  if (dbStatus) {
    //const otpGenerated = await otpGenerate();
   /* await User.updateOne({
      email: dbStatus.email
    }, //{ $set: { otpMobile: otpGenerated }   }
    );
    var no = dbStatus.countryCode + '' + dbStatus.phoneNumber;*/

   // await Sms(otpGenerated, no);


    response.setResponse = {
      status: 200,
      message: 'User Registered Please Login',
      error_stack: [],
      data: dbStatus
    };
    return true;
  } else {
    response.setResponse = {
      status: 500,
      message: 'User Registration Failed in Model',
      error_stack: "'User Registration Failed in Model'",
      data: {}
    };
    return false;
  }
};
exports.deleteUser = async (id, response) => {
  try {
    const deletedUser = await User.findByIdAndRemove(id);

  } catch (err) { console.log("error", err) }
}
exports.isUserRegistered = async (email, response) => {
  let emailCheckFlag = await User.findOne({
    email: email
  });
  if (emailCheckFlag) {
    response.setData = emailCheckFlag
    return true;
  } else {
    return false;
  }
};
exports.otpCheck = async (email, otp, response) => {
  try {

    let otpCheckFlag = await User.findOne({
     // otpMobile: otp,
      email: email
    });

   /* if (otpCheckFlag != null) {

      var oldTime = otpCheckFlag.otpExpire;

      var d1 = oldTime;
      var d2 = new Date().getTime();*/

      //if (d1 > d2) {

        var md5_value = await forgotPasswordNumberGenerate();
        var token = BASEURL + '/securityVerified/' + md5_value;
        var newDateObj = new Date();
        var oldDateObj = new Date();
        var responseData = await User.updateOne(
          {
            email: email
          },
          {
            $set: {
              emailToken: md5_value,
              emailTokenExpire: newDateObj.setTime(oldDateObj.getTime() + 10 * 60 * 1000),
              'flags.isVerifyMobile': true
            }
           
          }
        );
        console.log(responseData)
        let emailObj = {
          firstName: otpCheckFlag.firstName,
          lastName: otpCheckFlag.lastName,
          emailToken: token,
          imgURL:BASEURL+'/assets/user/img/logo-modal.png',
        }

        var Subject = 'Verify your email';
        var text = 'Email verification link : ' + token;
        var email = await ActivateMail(email, Subject, emailObj);
        response.setStatus = 200;
        response.setMessage = 'Successful';
        response.setData = [token];
        return false;
      /*} else {

        response.setStatus = 422;
        response.setMessage = 'Verification code expired';
        response.setErrorStack = 'Verification code expired';
      }*/
  /*  } else {
      response.setStatus = 422;
      response.setMessage = 'Invalid Verification Code';
      response.setErrorStack = 'Invalid Verification Code';
      return false;
    }*/
  }
  catch (err) {
    console.log('error', err);
  }
}


exports.emailCheck = async function (token, response) {


  let emailCheckFlag = await User.findOne({
    emailToken: token
  });


  if (emailCheckFlag.length != 0) {
    var responseData = await User.updateOne(
      {
        email: emailCheckFlag.email
      },
      {
        'flags.isVerifyEmail': true
      }
    );
    response.setStatus = 200;
    response.setMessage = 'ACCOUNT VERIFIED SUCCESSFULLY';
    response.setData = [responseData, emailCheckFlag];

    return true;
  } else {
    response.setStatus = 422;
    response.setMessage = 'Verification Failed, Please try again';
    response.setErrorStack = 'Verification Failed, Please try again';
    return false;
  }
};
exports.loginData = async (req, response, email, password) => {
  var data = await User.findOne({ email: email });

  if(!data) {
    response.setStatus = 422;
    response.setMessage = 'This account does not exist';
    response.setErrorStack = 'This account does not exist';
    return false;
  }

  if(data.flags.block_user) {
    response.setStatus = 422;
    response.setMessage = 'Your account is suspended by Administration, contact Member Service Desk';
    response.setErrorStack = 'Your account is suspended by Administration, contact Member Service Desk';
    return false;
  }


  if (data && data.userRole == req.body.type) {
    const loginToken = await setJWTToken(data);
    return new Promise(async function (resolve, reject) {
      if (data.flags.isVerifyMobile == true) {
        if (data.flags.isVerifyEmail == true) {
          bcrypt.compare(password, data.password, async function (err, res) {
            if (err) {
              response.setStatus = 422;
              response.setMessage = 'Wrong Password';
              response.setErrorStack = 'Wrong Password';
              reject();
            }

            if (res) {
              var responseData = await User.updateOne(
                {
                  email: data.email
                },
                {
                  jwtToken: loginToken.token,
                  isUserLoggedIn : true
                }
              );
              data.loginToken = loginToken;
              response.setStatus = 200;
              response.setMessage = 'Login Successful';
              response.setData = data;
              resolve();
            } else {
              response.setStatus = 422;
              response.setMessage = 'Wrong Password';
              response.setErrorStack = 'Wrong Password';
              reject();
            }
          });
        } else {

          response.setStatus = 422;
          response.setMessage = 'We already send  Verification link on your registered email';
          response.setErrorStack = 'We already send  Verification link on your registered email';;
          reject();
        }
      } else {
        const otpGenerated = await otpGenerate();

        var responseData = await User.updateOne(
          {
            email: data.email
          },
          {
            $set: {
              otpMobile: otpGenerated
            }
          }
        );
        var no = data.countryCode + '' + data.phoneNumber;

        const otpSend = await Sms(otpGenerated, no);

        response.setStatus = 422;
        response.setMessage = 'Please Verify your Mobile Number';
        response.setErrorStack = 'Please Verify your Mobile Number';
        reject();
      }
    });
  } else {
    response.setStatus = 422;
    response.setMessage = 'Invalid Credentials ';
    response.setErrorStack = 'Invalid Credentials';
    return false;
  }
};

exports.forgotPass = async  email => {
  try {
    const data = await User.findOne({ email: email });
    if (data) {
      return data;
    } else {
      return false;
    }
  }
  catch (err) {
    console.log(err)
  }
};
exports.hashCreate = async (userData, id, response) => {
  // try {
  let emailId = userData.email;

  return new Promise(async (resolve, reject) => {


    let token = await forgotPasswordNumberGenerate();
    let path = BASEURL + '/user/reset-password?token=' + token;
    //   = path + token;
    let forgotObj = {
      userId: id,
      emailToken: path
    };
    let oldDateObj = new Date();
    let newDateObj = new Date();
    let emailObj = {
      firstName: userData.firstName,
      lastName: userData.lastName,
      emailToken: path,
      email:userData.email,
      imgURL:BASEURL+'/assets/user/img/logo-modal.png',
    }
    //let data = new ForgotPass(forgotObj);

    //let dbStatus = await data.save();
    let dbStatus = await User.updateOne(
      { email: emailId },
      {
        'forgotEmailTokenDetails.emailToken': token,
        'forgotEmailTokenDetails.emailTokenExpire': newDateObj.setTime(oldDateObj.getTime() + (5 * 60 * 1000))
      });
    var Subject = 'Password reset';
    var text = 'Your Forgot Password Link : ' + token;

    var email = await ForgotMail(emailId, Subject, emailObj);
    if (email) {
      response.setStatus = 200;
      response.setMessage = 'Forgot Password Link has been sent your mail';
      response.setData = [dbStatus];


      resolve();
    } else {
      response.setStatus = 422;
      response.setMessage = ' Failed, Please enter your email Id';
      reject();
    }

  });
};
exports.setPass = async (token, password, response) => {

  var data = await User.findOne({
    'forgotEmailTokenDetails.emailToken': token
  });



  let nowDataTime = new Date().getTime();

  if (data.forgotEmailTokenDetails.emailTokenExpire > nowDataTime) {
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);

    let responseData = await User.updateOne(
      { email: data.email },
      { password: hash });
    if (responseData.nModified) {
      response.setStatus = 200;
      response.setMessage = 'Your Password Changed';
    } else {
      response.setStatus = 422;
      response.setMessage = "Password not changed";
      response.setErrorStack = "Password not changed";
    }

  } else {
    response.setStatus = 422;
    response.setMessage = "Token is Expire";
    response.setErrorStack = "Token is Expire";
  }

};
exports.logoutUser = async (token, email, response) => {
  var responseData = await User.updateOne(
    {
      email: email,
      jwtToken: token
    },
    {
      $set: {
        jwtToken: ''
      }
    }
  );

  if (responseData.nModified) {
    response.setStatus = 200;
    response.setMessage = 'User Logout';
    return true;
  } else {
    response.setStatus = 422;
    response.setMessage = 'User Logout Failed';
    return false;
  }
};
exports.signupEmailToken = async (token, res, response) => {
  try {
    let tokenCheck = await User.findOneAndUpdate({
      emailToken: token
    }, {
      "flags.isVerifyEmail": true, 
      "emailToken": null
    });
    // Your account has already been verified
    if (tokenCheck) {
      var nowDataTime = new Date().getTime();
     
      if (tokenCheck.emailTokenExpire > nowDataTime) {
        response.setStatus = 200;
        response.setMessage = 'Your email address has been successfully verified';
        return true;

      } else {

        response.setStatus = 422;
        response.setMessage = 'Link Expired.';
        return false;
      }

    } else {
      response.setStatus = 422;
      response.setMessage = 'Link Expired.';
      return false;
    }
  } catch (err) {
    console.log("Error.............", err)
  }
}

exports.resetPasswordToken = async (token, res, response) => {
  try {  
    let tokenCheck = await User.findOne({
      "forgotEmailTokenDetails.emailToken": token
    });
    if (tokenCheck) {
      var nowDataTime = new Date().getTime();
      if (tokenCheck.forgotEmailTokenDetails.emailTokenExpire > nowDataTime) {
        response.setStatus = 200;
        response.setMessage = 'Reset password';
        return true;
      } else {
        response.setStatus = 422;
        response.setMessage = 'This link has expired, please request a new link.';
        return false;
      }
    } else {    
      response.setStatus = 422;
      response.setMessage = 'This link has expired, please request a new link.';
      return false;
    }
  } catch (err) {
    console.log("Error.............", err)
  }
}