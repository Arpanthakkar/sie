import IaiToken from '../IaiToken'
import BmtToken from '../BmtToken'
import FgmToken from '../FgmToken'
import OegToken from '../OegToken'
import web3 from '../web3'
import config from 'config'
import axios from 'axios'
// import ethUtil from 'ethereumjs-util'
const ethUtil = require('ethereumjs-util')
const infuraKey = config.get('common.infuraKey')
const etherscanKey = config.get('common.etherscanKey')
const bmtAddress = config.get('common.BmtTokenAddress')


let api = require('etherscan-api').init(infuraKey, 'ropsten', '30000')
const etherscanUrl = config.get('common.etherscanUrl')
// import Tx from 'ethereumjs-tx'
const Tx = require('ethereumjs-tx').Transaction

var moment = require('moment');


module.exports = {
	getTokenInfo: async (address, token) => {
		let tokenName = token
		try {

			const Token = require('../' + tokenName)

			const name = await Token.methods.name().call()
			const symbol = await Token.methods.symbol().call()
			const balance = await module.exports.getBalance(address, tokenName)
			const resObj = {
				name: name,
				symbol: symbol,
				balance: balance
			}
			return resObj
		} catch (err) {
			return false
		}
	},
	getBalance: async (address, token) => {
		let tokenName = token
		try {
			let Token = require('../' + tokenName)
			let balance = await Token.methods.balanceOf(address).call()
			balance = Number(balance) / (10 ** 8)
			return Number.parseFloat(Number(balance)).toFixed(8)
		} catch (err) {
			return false
		}
	},
	// transfer: async(address, amount, token) => {
	// 	let tokenName = token
	// 	try{
	// 		let Token = require('../' + tokenName)
	// 		const accounts =await web3.eth.getAccounts()
	// 		console.log(accounts)
	//         const trans = await Token.methods.transfer(address, amount).send({
	//             from: accounts[0],
	//             gas: '1000000'
	// 		})

	// 		return trans
	// 	}catch(err){
	// 		return false
	// 	}
	// },
	getTxList: async (address, tokenAdd) => {
		let tokenAddress = tokenAdd
		try {
			// console.log('---o-o-o-o',address)
			const contractAddress = config.get('common.' + tokenAddress)
			let txList = await axios(etherscanUrl + 'api?module=account&action=tokentx&contractaddress=' + contractAddress + '&address=' + address + '&page=1&offset=100&sort=asc&apikey=' + etherscanKey + '')
			//https://api-ropsten.etherscan.io/api?module=account&action=tokentx&contractaddress=0xd8646663960cDc6bFf7442E0e78b874a9c4C8914&address=0x272995694Fd08920E8C93EdCac76a059D07B5c21&page=1&offset=100&sort=asc&apikey=N735S6ZYRQBYCAFC5PCAAUNSXEW4SRAJSE
			let txnList = []
			// console.log('sds', txList.data)
			txList = txList.data

			for (let i = 0; i < txList.result.length; i++) {
				// console.log('-----=-=-=-=--=-=--=-')
				let date = txList.result[i].timeStamp
				date = new Date(date * 1000)
				let details
				let fromAdd = await ethUtil.toChecksumAddress(txList.result[i].from)
				// console.log('=============' ,fromAdd)
				let toAdd = await ethUtil.toChecksumAddress(txList.result[i].to)
				if (address == fromAdd) {
					details = 'sent'

				}
				if (address == toAdd) {
					details = 'received'

				}
				const respObj = {
					name: txList.result[i].tokenName,
					symbol: txList.result[i].tokenSymbol,
					quantity: Number.parseFloat(Number(txList.result[i].value) / (10 ** 8)).toFixed(8),
					txHash: txList.result[i].hash,
					from: fromAdd,
					to: toAdd,
					dateSting: date.toLocaleString(),
					date: moment(date).format("MM/DD/YYYY HH:mm"),
					details: details,
					gasValue: txList.result[i].gasUsed
				}
				txnList.push(respObj)
			}
			return txnList
		} catch (err) {
			console.log('err', err)
			return err
		}
	},
	// addKycUser:async(req,fromAddress,toAddress,token,tokenAdd)=>{
	// 	let tokenName = token
	// 	let tokenAddress = tokenAdd
	// 	console.log(tokenName,tokenAddress)
	// 	try{
	// 		const contractAddress = config.get('common.'+ tokenAddress)
	// 		let Token = require('../' + tokenName)
	// 		console.log('------....>>>>>>>',req.privateKey)
	// 		console.log('to address => ',toAddress)
	// 		console.log('from Address >>>>>',fromAddress)
	// 		let privateKey = req.privateKey
	// 		const nonce = await  web3.eth.getTransactionCount(fromAddress,'pending')
	// 		console.log('nonce===>>>>>',nonce)
	// 		var rawTx = {
	// 			nonce: web3.utils.toHex(nonce),
	// 			from:fromAddress,
	// 			gasPrice: web3.utils.toHex(web3.utils.toWei('2', 'Gwei')),
	// 			gasLimit:  web3.utils.toHex('3000000'),
	// 			to: contractAddress,
	// 			value: '0x00',
	// 			data:Token.methods.addKycUser(toAddress).encodeABI()
	// 		  }


	// 		  var tx = new Tx(rawTx, {'chain':'ropsten'});
	// 		  tx.sign(privateKey);

	// 		  var serializedTx =await tx.serialize();
	// 		  console.log('herer.........',serializedTx)

	// 		  let result = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
	// 		  .on('receipt', console.log);

	// 		  console.log('result',result)

	// 		  return result

	// 	}catch(err){
	// 		console.log(err)
	// 		return err
	// 	}
	// },	
	getOwnerAddress: async (token) => {
		let tokenName = token
		try {
			let Token = require('../' + tokenName)
			const address = await Token.methods.owner().call()
			return address
		} catch (err) {
			return false
		}
	},
	adminTokenInfo: async (token, tokenAdd) => {
		let tokenName = token
		let tokenAddress = tokenAdd
		try {
			const contractAddress = config.get('common.' + tokenAddress)
			let Token = require('../' + tokenName)
			const name = await Token.methods.name().call()
			console.log("name", name)
			const symbol = await Token.methods.symbol().call()

			let totalSupply = await Token.methods.totalSupply().call()
			totalSupply = Number(totalSupply) / (10 ** 8)
			// const sendToken = await module.exports.getSentToken(tokenName, tokenAddress)
			// console.log(sendToken)
			// const outstandingToken = Number(totalSupply) - Number(sendToken)
			const respObj = {
				name: name,
				symbol: symbol,
				totalTokens: Number.parseFloat(totalSupply).toFixed(8),
				contractAddress: contractAddress
			}

			return respObj

		} catch (err) {
			return false
		}
	},
	getSentToken: async (token, tokenAdd) => {
		let tokenName = token
		let tokenAddress = tokenAdd
		try {
			const address = await module.exports.getOwnerAddress(tokenName)
			const txList = await module.exports.getTxList(address, tokenAddress)
			let amount = 0
			for (let i in txList) {
				if (txList[i].details == 'sent') {
					amount += Number(txList[i].quantity)
				}
			}
			// amount = Number(amount)/(10**8)
			return amount
		} catch (err) {
			return err
		}
	},
	getKycUser: async (address, token) => {
		let tokenName = token
		try {
			let Token = require('../' + tokenName)

			const kyc = await Token.methods.isKycUser(address).call()
			console.log('------', kyc)
			return kyc
		} catch (err) {
			return false
		}
	},
	burnToken: async (amount, token) => {
		let tokenName = token
		try {
			let Token = require('../' + tokenName)
			let ownerAddress = await module.exports.getOwnerAddress(tokenName)
			const trans = await Token.methods.burn(amount).send({
				from: ownerAddress,
				gas: '1000000'
			})


			return trans

		} catch (err) {
			console.log(err)
			return false
		}
	},
	bmtTransfer: async (req, fromAddress, toAddress, amount, token, tokenAdd) => {


		console.log("from.....", fromAddress)
		console.log("to.....", toAddress)
		console.log("amount.....", amount)
		console.log("token.....", token)
		console.log("tokenAdd.....", tokenAdd)


		let tokenName = token
		let tokenAddress = tokenAdd
		const contractAddress = config.get('common.' + tokenAddress)
		try {
			let Token = require('../' + tokenName)
			let Amount = parseInt(amount * (10 ** 8))
			console.log('amount =>', Amount)
			console.log('------....>>>>>>>', req.privateKey)
			console.log('to address => ', toAddress)
			console.log('from Address >>>>>', fromAddress)
			let privateKey = req.privateKey
			const nonce = await web3.eth.getTransactionCount(fromAddress)
			console.log('nonce===>>>>>', nonce)
			var rawTx = {
				nonce: web3.utils.toHex(nonce),
				from: fromAddress,
				gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'Gwei')),
				gasLimit: web3.utils.toHex('3000000'),
				to: contractAddress,
				value: '0x00',
				data: Token.methods.transfer(toAddress, Amount).encodeABI()
			}


			var tx = new Tx(rawTx, { 'chain': 'ropsten' });
			tx.sign(privateKey);

			var serializedTx = await tx.serialize();
			console.log('herer.........', serializedTx)

			let result = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
				.on('receipt', console.log);

			console.log('result', result)

			return result

		} catch (err) {
			console.log(err)
			return false
		}

	},
	getTotalEtherEstimate: async (data) => {
		try {
			var amount_of_tokens = data.amount;
			var wallet_address = data.address;
			// var ckc_per_ether = data.ckc_per_ether;
			var amount = parseInt(amount_of_tokens)
			console.log("amountssssssssss", amount);
			console.log("walletsssssssssssss", wallet_address);
			const { totalGasUsedInGwei, totalGasUsedInEther } = await module.exports.getGasEstimate(amount, wallet_address);
			return {
				totalGasUsedInGwei,
				totalGasUsedInEther
			}

		} catch (exception) {
			console.log("exception ---====-==-=-=>", exception);
			throw new Error("something_went_wrong");
		}

	},
	getGasEstimate: async (amount, sender) => {
		try {
			var amount = amount.toString();
			console.log("BN", amount);
			console.log("wei", web3.utils.toWei(amount, 'ether'));
			const gasAmount = await web3.eth.estimateGas({
				from: sender,
				to: bmtAddress,
				data: BmtToken.methods.transfer(sender, amount).encodeABI(),
			})
			let totalGasUsedInGwei = gasAmount * 2;
			let totalGasUsedInEther = (totalGasUsedInGwei / 1000000000);
			var totalAmountInEther = parseFloat(amount) + parseFloat(totalGasUsedInEther);
			console.log(gasAmount)
			console.log("gwei", totalGasUsedInGwei)
			console.log("ether", totalGasUsedInEther)
			return {
				totalGasUsedInGwei,
				totalGasUsedInEther
			}

		} catch (err) {
			console.log('error============>', err);
			throw new Error('somethng went wrong')
		}

	},


	addKycUser: async (req, address, token, tokenAdd) => {


		// console.log("from.....", fromAddress)
		console.log("to.....", address)
		// console.log("amount.....", amount)
		console.log("token.....", token)
		console.log("tokenAdd.....", tokenAdd)


		let tokenName = token
		let tokenAddress = tokenAdd
		const contractAddress = config.get('common.' + tokenAddress)
		try {
			const fromAddress = '0x2FF312a35e4bBD2158997D90CC81bC298059F2D1'
			console.log('nonce', nonce)
			let Token = require('../' + tokenName)
			// let Amount = parseInt(amount (10 * 8))
			// console.log('amount =>', Amount)
			console.log('------....>>>>>>>', req.privateKey)
			console.log('to address => ', address)
			console.log('from Address >>>>>', fromAddress)
			let privateKey = req.privateKey
			const txnonce = await web3.eth.getTransactionCount(fromAddress, 'pending')
			const nonce = txnonce
			console.log('nonce===>>>>>', nonce)
			var rawTx = {
				nonce: web3.utils.toHex(nonce),
				from: fromAddress,
				gasPrice: web3.utils.toHex(web3.utils.toWei('2', 'Gwei')),
				gasLimit: web3.utils.toHex('3000000'),
				to: contractAddress,
				value: '0x00',
				data: Token.methods.addKycUser(address).encodeABI()
			}
			console.log(rawTx)

			var tx = new Tx(rawTx, { 'chain': 'ropsten' });
			tx.sign(privateKey);

			var serializedTx = await tx.serialize();
			console.log('herer.........', serializedTx)

			let result = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
				.on('receipt', console.log);

			console.log('result', result)

			return result

		} catch (err) {
			console.log(err)
			throw new Error(err)
		}

	},
}