const web3 = require('./web3')
const myToken = require('../ethereum/build/contracts/IaiERC20Token.json')
const config = require('config')
const iaiAddress = config.get('common.IaiTokenAddress')

const instance = new web3.eth.Contract(
    JSON.parse(JSON.stringify(myToken.abi)),
    iaiAddress
)

module.exports = instance
