import Express from 'express'
import bodyParser from 'body-parser'
import dashboard from './routes/provider.routes'
import admin from './routes/admin.routes'
const app = Express()
import web3 from './web3'
var path = require('path');
var expressHbs = require('express-handlebars');
var session = require('express-session');
var handleBarHelpers = require('./modules/handlebar.helper');



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

const config = require('./config.js');
const { database } = config;
const mongoose = require("mongoose");
const dbport = database.port;
const host = database.host;
const username = database.username;
const password = database.password;
const dbname = database.name;


mongoose.connect('mongodb://' + host + ':' + dbport + '/' + dbname, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
}).then(() => console.log("Mongo Connected")).catch(() => console.log("Mongo connection Failed"));

var indexRouter = require('./routes/');
var ResponseHelper = require('./helpers/responseHelper');
const { validateSession } = require('./middleware/authToken');

app.use(async function (req, res, next) {
  req.response = new ResponseHelper();
  next();
});
app.use(session({
  secret: 'sieProject',
  resave: false,
  saveUninitialized: true,
  cookie: {
    path: '/',
    httpOnly: false,
    secure: false,
    maxAge: 31536000000
  }
}));

app.use(Express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/admin', admin)
app.use(validateSession);
app.use('/user', dashboard)


// app.use(validateSession);
// app.use('/user', dashboard)

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     next(createError(404));
//   });
app.use(bodyParser.urlencoded({
  limit: "250mb",
  extended: false
}));
app.use(bodyParser.json({ limit: "250mb" }));

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err)
  res.render('error');
});

// view engine setup
app.engine('hbs', expressHbs({
  // defaultLayout: 'layout',
  extname: '.hbs',
  helpers: handleBarHelpers.helperFunction,
  layoutsDir: path.join(__dirname, './views'),
  partialsDir: [
    __dirname + '/views/partials',
  ]
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, './views'));

module.exports = app