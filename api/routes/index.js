var express = require('express');
var router = express.Router();
var path = require('path');
var multer = require('multer');
const { validateToken } = require('../middleware/authToken');
const {
  registrationValidation,
  registerDataCreate,
  loginValidation,
  loginDataCreate,
  forgotValidation,
  setPasswordValidation
} = require('../middleware/globalFunction');
const { 
  userRegister, 
  otpGenerate, 
  emailTokenCheck, 
  login, 
  forgot, 
  setPassword, 
  logout,
  checkToken ,
  token,resetPass
} = require('../controller/indexController');

var storage = multer.diskStorage({
  destination: function(req, file, cd) {
    cd(null, 'public/media/profilePhotos');
  },
  filename: function(req, file, cd) {
    cd(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

var upload = multer({
  storage: storage,
  preservePath: true
});
router.post('/register', registrationValidation, registerDataCreate, userRegister);
router.get('/securityVerified', emailTokenCheck);
router.post('/otpCheck', otpGenerate);
router.post('/login', loginValidation, loginDataCreate, login);
router.post('/forgotPassword', forgotValidation, forgot);

router.get('/resetPassword',resetPass)
router.post('/setPassword', setPasswordValidation, setPassword);
router.post('/logout', validateToken, logout);
router.get('/securityVerified/:Token',token);
/* GET home page. */
router.get('/', function(req, res, next) {

  res.send('SIE working');
});
module.exports = router;
