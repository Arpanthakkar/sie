import express from 'express'
const router = express.Router()
var path = require('path');
var multer = require('multer');
import keyMiddleware from '../middleware/keys'
const { validateKyc, validateChangePassword,addUserValidation,addUserAdminSide } = require('../middleware/globalFunction');
var storage = multer.diskStorage({
  destination: (req, files, cb) => {
    if (files.fieldname == `profilePicture`) { cb(null, './public/media/profilePicture'); }

  },
  filename: (req, file, cb) => {
    if (file.originalname.indexOf(' ')) {
      file.originalname = file.originalname.replace(/ /g, '_');
    }
    var ext = file.originalname.split('.');
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    var today = dd + '-' + mm + '-' + yyyy;
    cb(null, req.session.userDetails._id + '_' + file.fieldname + '-' + today + '.' + ext[1]);
  }
});
const fileFilter = (req, file, cb) => {

  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype === "application/pdf"
  ) {

    cb(null, true);
  } else {

    req.errorMessage = "File format should be PNG,JPG,JPEG";
    cb(null, false);
    //cb(new Error("File format should be PNG,JPG,JPEG"), false); // if validation failed then generate error
  }
};

var upload = multer({ storage: storage, fileFilter: fileFilter });

import adminController from '../controller/admin.controller'
const { dashboard,
  dashboard1,
  members,
  members1,
  memberDetails,
  token,
  kycRequest,
  transactions,
  myAccount,
  kycStatus,
  kycReject,
  userProfileData,
  updateAdminProfile,
  changePassword,
  userData,
  kycRequests,
  getTransactionHistory,
  logout,
  login,
  adminDashboard,
  addKycUser,
  getTokens,
  isKycUser,
  burnToken,
  contactUs, contactUsView, statusChange,
  documentStatusChange,
  addUser,addNewUser,
  userSuspend,
  wallet,MultiSend
} = require('../controller/admin.controller')
const { validateToken, validateSession, validateAdminSession } = require('../middleware/authToken');
router.post('/addkyc',addKycUser)
router.get('/tokenInfo', getTokens)
router.get('/iskyc', isKycUser)
router.post('/burn', keyMiddleware.getPrivateKey,burnToken)
router.get('/txnhistory', getTransactionHistory)
router.get('/adminDashboard', adminDashboard)

router.post('/multi', keyMiddleware.getPrivateKey,MultiSend)
//Node API
router.get('/dashboard', dashboard);
router.get('/members', validateAdminSession, members);
router.get('/userDetails', validateAdminSession, userData)
// router.post('/kycStatus', validateSession, kycStatus);
// router.post('/kycStatusRejectReason', validateSession, kycReject);
router.post('/changePassword', validateAdminSession, validateChangePassword, changePassword);
router.get('/userProfile', validateAdminSession, userProfileData)
router.post('/kycRequests', validateAdminSession, kycRequests);
router.post('/statusChange', validateAdminSession, statusChange);
router.post('/documentStatusChange', documentStatusChange)
router.post('/contactUs', validateAdminSession, contactUs);
router.get('/contactUs', validateAdminSession, contactUsView);
router.post('/userProfile', validateAdminSession, function (req, res, next) {
  try {

    req.errorMessage = "";
    next();
  } catch (error) {
    console.log("error...............", err)
    res.status(422).send(error.message)
  }
}, upload.fields([
  { name: 'profilePicture', maxCount: 1 },
]), function (req, res, next) {
  if (req.errorMessage != "") {
    console.log("error...............")
    var response = req.response;
    response.setMessage = req.errorMessage
    response.setStatus = 422;
    response.setErrorStack = req.errorMessage
    res.status(422).send(response);
  } else { next(); }

}, updateAdminProfile);
router.post('/addUser',validateAdminSession,addUserValidation,addUserAdminSide,addNewUser)

//Admin 

router.get('/login', login);

router.post('/userSuspend', validateAdminSession, userSuspend);


// router.get('/login', function (req, res, next) {
//   console.log("yessssssssssssssss")

//   console.log("req.session.isLoggedIn......",req.session.isLoggedIn)
//   console.log("req.session.loggedInType........",req.session.loggedInType)

//   if (req.session.isLoggedIn  && req.session.loggedInType == 'admin') {
//     console.log("1111111")
//     res.redirect('dashboard1')
//   } else {
//     console.log("222222222")
//     res.redirect('login')

//   }
// });


router.get('/dashboard1', validateAdminSession, dashboard1);
router.get('/members1', validateAdminSession, members1);
//router.get('/token', token);
router.get('/kycRequest', validateAdminSession, kycRequest);
router.get('/transactions', validateAdminSession, transactions);
router.get('/myAccount', validateAdminSession, myAccount);
router.get('/logout', logout);
router.get('/login', login);
router.get('/memberDetails', validateAdminSession, memberDetails);
router.get('/wallet', validateAdminSession, wallet);

//router.get('/addMember',validateAdminSession,addUser)

module.exports = router