import express from 'express';
const router = express.Router();
import keyMiddleware from '../middleware/keys'
import dashboardController from '../controller/provider.controller';
import { otpCheck } from '../services/indexService';
const { validateToken, validateSession , checkMnemonic, checkMnemonicKyc } = require('../middleware/authToken');
const { validateKyc, validateChangePassword, } = require('../middleware/globalFunction');
var path = require('path');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, files, cb) => {
  
    if (files.fieldname == `idProofFile`) cb(null, './public/media/userKyc/idProof');
    else if (files.fieldname == `addressProofFile`) cb(null, './public/media/userKyc/addressProof');
    else if (files.fieldname == `withSelfiePhotoIdFile`) cb(null, './public/media/userKyc/withSelfiePhotoIdFile');
    else cb(null, 'public/media/profilePicture');

  },
  filename: (req, file, cb) => {
    if (file.originalname.indexOf(' ')) {
      file.originalname = file.originalname.replace(/ /g, '_');
    }
  
    var ext = file.originalname.split('.');
  
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    var today = dd + '-' + mm + '-' + yyyy;
    cb(null, req.session.userDetails._id + '_' + file.fieldname + '-' + today + '.' + ext[1]);
  }
});
const fileFilter = (req, file, cb) => {

  
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype == "application/pdf"
  ) {
  
    cb(null, true);
  } else {
    
    req.errorMessage = "File format should be PNG,JPG,JPEG";
    cb(null, false);
    //cb(new Error("File format should be PNG,JPG,JPEG"), false); // if validation failed then generate error
  }
};

var upload = multer({ storage: storage, fileFilter: fileFilter });


const {
  dashboard,
  getTransactionHistory,
  transferToken,
  getAdminWallet,
  signUp,
  dashboard1,
  history,
  wallet,
  contactUs,
  kycVerificationStep1,
  kycVerificationStep2,
  kycVerificationStep3,
  kycVerification,
  myAccount,
  kycVerificationDetails,
  kyc,
  updateUserProfile,
  changePassword,
  kycAgreement,
  resetPassword,
  tokenCheck,
  logout,
  linkExpired,
  verified,
  getBalance,
  changePhoneNumber, otpCheckPhnChange,
  genrateWallet,contactUsView,
  userSuspend,
  etherEstimate,
  mnemonic,
  addMnemonic,
  verifyLogin,
  changePhoneNumberSignUp,
  verification,
  googleAuthenticator,
  generateQRCode,
  sendSecretORCode,
  createUserWallet,
} = require('../controller/provider.controller');

router.get('/login', function (req, res, next) {

  if (req.session.isLoggedIn  && req.session.loggedInType == 'user') {
    res.redirect('dashboard1')
  } else {
    res.render('login', {
      layout: 'login'
    });
  }
});

router.get('/dashboard', dashboard);
router.get('/transaction', getTransactionHistory);
router.post('/transfer', keyMiddleware.getPrivateKey, transferToken);
router.get('/ownerinfo', getAdminWallet);
router.get('/balance', getBalance);
router.post('/createWallet', genrateWallet)
router.post('/getEstimate',etherEstimate)
router.post('/verifyMnemonic',verifyLogin)

router.get('/signUp', signUp);
router.get('/dashboard1', checkMnemonic, dashboard1);
router.get('/history', checkMnemonic, history);
router.get('/wallet', checkMnemonic, wallet);
router.get('/contactUs', contactUs);
router.get('/kyc-verification-step-1',  checkMnemonicKyc,kycVerificationStep1);
router.get('/kyc-verification-step-2',checkMnemonicKyc, kycVerificationStep2);
router.get('/kyc-verification-step-3',  kycVerificationStep3);
router.get('/kyc-verification',  kycVerification);
router.get('/my-account', myAccount);
router.get('/reset-password/:Token?', resetPassword);
router.get('/logout', logout);
router.get('/linkExpired', linkExpired);
router.get('/successfullyVerified', verified);
router.get('/mnemonic', mnemonic);
router.get('/verification', validateSession,verification);

router.get('/googleAuthenticator/:Token?', googleAuthenticator);
router.get('/generateQRCode', generateQRCode);
router.get('/createWallet',keyMiddleware.getPrivateKey2, createUserWallet)

router.post('/addMnemonic', addMnemonic);


//Kyc api
router.get('/contact',validateSession,contactUsView);
router.get('/kycVerificationDetails', validateSession, kycVerificationDetails);
router.post('/changePassword', validateSession, validateChangePassword, changePassword);
router.post('/kycDocument', validateSession, function (req, res, next) {
  try {

    req.errorMessage = "";
    next();
  } catch (error) {
    console.log("error...............", err)
    res.status(422).send(error.message)
  }
},
  upload.fields([
    { name: 'addressProofFile', maxCount: 1 },
    { name: 'idProofFile', maxCount: 1 },
    { name: 'withSelfiePhotoIdFile', maxCount: 1 }
  ]), function (req, res, next) {
    if (req.errorMessage != "") {
      console.log("error...............")
      var response = req.response;
      response.setMessage = req.errorMessage
      response.setStatus = 422;
      response.setErrorStack = req.errorMessage
      res.status(422).send(response);
    } else { next(); }

  }, kyc
);
router.get('/userProfile', validateSession, kycVerificationDetails)
router.post('/userProfile', validateSession, function (req, res, next) {
  try {
    req.errorMessage = "";
    next();
  } catch (error) {
    console.log("error...............", err)
    res.status(422).send(error.message)
  }
}, upload.fields([
  { name: 'profilePicture', maxCount: 1 },
]), function (req, res, next) {
  if (req.errorMessage != "" && typeof req.errorMessage != "undefined") {
    console.log("error...............", req.errorMessage)
    var response = req.response;
    response.setMessage = req.errorMessage
    response.setStatus = 422;
    response.setErrorStack = req.errorMessage
    res.status(422).send(response);
  } else { next(); }
}, updateUserProfile);



router.post('/agreement', validateSession, kycAgreement);
router.post('/reset-password', tokenCheck);
router.post('/changePhoneNumber', validateSession, changePhoneNumber);
router.post('/checkOtpChangePhoneNo', validateSession, otpCheckPhnChange);
router.post('/changePhoneNumberSignUp', validateSession, changePhoneNumberSignUp);
router.post('/sendSecretORCode', validateSession, sendSecretORCode);

router.get('/test', function (req, res, next) {

  let renderParams = {
    layout: 'layouts/main',
    title: 'history',
    //   pageTilte: 'Dashboard',
    //  BASE_URL: CONST.BASE_URL
  };
   
    res.render('user/test',renderParams);

});
module.exports = router;

