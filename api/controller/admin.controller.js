import tokenService from '../services/token.service';
import txService from '../services/transaction';
import adminService from '../services/admin.service';
import { handleResponse, handleError } from '../middleware/responsehandler';
import { createWallet } from '../services/wallet.service';
import { updateUser, userSuspend, getUserDetails } from '../services/providerService';
const { User} = require('../models/dbSchemas');
var moment = require('moment');


const {
  getDashboardData,
  getMemberData,
  userKycData,
  userKycDataRejectReason,
  userDetailsUpdate,
  changePass,
  userDetail,
  userKycStatusData,
  userDataFetch,
  insertUser,
  contactUsDetails,
  contactUsDetailsView,
  userKycVerify,
  documentStatus,
  getUserWallet
} = require('../services/admin.service');
const { isUserRegistered } = require('../services/indexService');
(exports.getTokens = async (req, res) => {
  try {
    const bmtInfo = tokenService.adminTokenInfo('BmtToken', 'BmtTokenAddress');

    const fgmInfo = tokenService.adminTokenInfo('FgmToken', 'FgmTokenAddress');
    const iaiInfo = tokenService.adminTokenInfo('IaiToken', 'IaiTokenAddress');
    const oegInfo = tokenService.adminTokenInfo('OegToken', 'OegTokenAddress');

    await Promise.all([bmtInfo, fgmInfo, iaiInfo, oegInfo])
      .then(data => {
        const respObj = {
          bmt: data[0],
          fgm: data[1],
          iai: data[2],
          oeg: data[3]
        };

        handleResponse({ res, data: respObj });
      })
      .catch(err => {
        console.log(err);
        handleError({ res, data: 'Something went wrong' });
      });
  } catch (err) {
    handleError({ res, data: err });
  }
}),
(exports.bmtDashboard = async address => {
  try {
    const BmtTokenInfo = await tokenService.adminTokenInfo('BmtToken', 'BmtTokenAddress');
    console.log(BmtTokenInfo);
    // const sent = txService.getSentToken(address, 'BmtTokenAddress');
    // console.log(sent);
    // const recieve = txService.getReceiveToken(address, 'BmtTokenAddress');
    // const burn = txService.getBurnedToken(address, 'BmtTokenAddress');

    // let result = await Promise.all([BmtTokenInfo, sent, recieve, history, burn]).then(data => {
    const history = await  adminService.getTxList(address, 'BmtTokenAddress');
    // let sentAmount = 0
    let receiveAmount = 0
    let burnAmount = 0
    for (let i in history) {
      // if (history[i].details == 'sent') {
      //   sentAmount += Number(history[i].quantity)
      // }
      if (history[i].details == 'received') {
        receiveAmount += Number(history[i].quantity)
      }
      if (history[i].to == '0x0000000000000000000000000000000000000000') {
        burnAmount += Number(history[i].quantity)
      }
    }
    // sentAmount = Number.parseFloat(sentAmount).toFixed(8)
    receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
    burnAmount = Number.parseFloat(burnAmount).toFixed(8)
    const outstandingToken = await tokenService.getBalance(address,'BmtToken')
    const sentAmount = BmtTokenInfo.totalTokens - outstandingToken
    const respObj = {
      token: BmtTokenInfo,
      sendTokens: sentAmount,
      received: receiveAmount,
      burnToken: burnAmount,
      outstandingTokens : Number.parseFloat(outstandingToken).toFixed(8),
      history: history
    };
    console.log('respobj===============', respObj);
      return respObj;
    // });

    // return result;
  } catch (err) {
    console.log(err);
    return err;
  }
});

exports.fgmDashboard = async address => {
try {
  const FgmTokenInfo = await tokenService.adminTokenInfo('FgmToken', 'FgmTokenAddress');
  // const sent = txService.getSentToken(address, 'FgmTokenAddress');
  // console.log(sent);
  // const burn = txService.getBurnedToken(address, 'FgmTokenAddress');
  // let result = await Promise.all([FgmTokenInfo, sent, recieve, history, burn]).then(data => {
  // const recieve = txService.getReceiveToken(address, 'FgmTokenAddress');
  const history = await adminService.getTxList(address, 'FgmTokenAddress');
  // let sentAmount = 0
  let receiveAmount = 0
  let burnAmount = 0
  for (let i in history) {
    // if (history[i].details == 'sent') {
    //   sentAmount += Number(history[i].quantity)
    // }
    if (history[i].details == 'received') {
      receiveAmount += Number(history[i].quantity)
    }
    if (history[i].to == '0x0000000000000000000000000000000000000000') {
      burnAmount += Number(history[i].quantity)
    }
  }
  // sentAmount = Number.parseFloat(sentAmount).toFixed(8)
  receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
  burnAmount = Number.parseFloat(burnAmount).toFixed(8)
  const outstandingToken = await tokenService.getBalance(address,'FgmToken')
  const sentAmount = FgmTokenInfo.totalTokens - outstandingToken
  const respObj = {
    token: FgmTokenInfo,
    sendTokens: sentAmount,
    received: receiveAmount,
    burnToken: burnAmount,
    outstandingTokens: Number.parseFloat(outstandingToken).toFixed(8),
    history: history
  };
  console.log('respobj===============', respObj);
  return respObj;

  // return result;
} catch (err) {
  console.log(err);
  return err;
}
};

exports.oegDashboard = async address => {
try {
  const OegTokenInfo = await tokenService.adminTokenInfo('OegToken', 'OegTokenAddress');
  // const sent = txService.getSentToken(address, 'OegTokenAddress');
  // console.log(sent);
  // const recieve = txService.getReceiveToken(address, 'OegTokenAddress');
  const history = await adminService.getTxList(address, 'OegTokenAddress');
  // const burn = txService.getBurnedToken(address, 'OegTokenAddress');
  // let result = await Promise.all([OegTokenInfo, sent, recieve, history, burn]).then(data => {
  // let sentAmount = 0
  let receiveAmount = 0
  let burnAmount = 0
  for (let i in history) {
    // if (history[i].details == 'sent') {
    //   sentAmount += Number(history[i].quantity)
    // }
    if (history[i].details == 'received') {
      receiveAmount += Number(history[i].quantity)
    }
    if (history[i].to == '0x0000000000000000000000000000000000000000') {
      burnAmount += Number(history[i].quantity)
    }
  }
  // sentAmount = Number.parseFloat(sentAmount).toFixed(8)
  receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
  burnAmount = Number.parseFloat(burnAmount).toFixed(8)
  const outstandingToken = await tokenService.getBalance(address,'OegToken')
  const sentAmount = OegTokenInfo.totalTokens - outstandingToken
  const respObj = {
    token: OegTokenInfo,
    sendTokens: sentAmount,
    received: receiveAmount,
    burnToken: burnAmount,
    outstandingTokens: Number.parseFloat(outstandingToken).toFixed(8),
    history: history
  };
  console.log('respobj===============', respObj);
  return respObj;
} catch (err) {
  console.log(err);
  return err;
}
};

exports.iaiDashboard = async address => {
try {
  const IaiTokenInfo = await tokenService.adminTokenInfo('IaiToken', 'IaiTokenAddress');
  // const sent = txService.getSentToken(address, 'IaiTokenAddress');
  // console.log(sent);
  // const recieve = txService.getReceiveToken(address, 'IaiTokenAddress');
  const history =await adminService.getTxList(address, 'IaiTokenAddress');
  // const burn = txService.getBurnedToken(address, 'IaiTokenAddress');
  // let result = await Promise.all([IaiTokenInfo, sent, recieve, history, burn]).then(data => {
  // let sentAmount = 0
  let receiveAmount = 0
  let burnAmount = 0
  for (let i in history) {
    // if (history[i].details == 'sent') {
    //   sentAmount += Number(history[i].quantity)
    // }
    if (history[i].details == 'received') {
      receiveAmount += Number(history[i].quantity)
    }
    if (history[i].to == '0x0000000000000000000000000000000000000000') {
      burnAmount += Number(history[i].quantity)
    }
  }
  // sentAmount = Number.parseFloat(sentAmount).toFixed(8)
  receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
  burnAmount = Number.parseFloat(burnAmount).toFixed(8)
  // console.log('============', IaiTokenInfo.)
  const outstandingToken = await tokenService.getBalance(address,'IaiToken')
  const sentAmount = IaiTokenInfo.totalTokens - outstandingToken
  const respObj = {
    token: IaiTokenInfo,
    sendTokens: sentAmount,
    received: receiveAmount,
    burnToken: burnAmount,
    outstandingTokens: Number.parseFloat(outstandingToken).toFixed(8),
    history: history
  };
  console.log('respobj===============', respObj);
  return respObj;
  // });

  // return result;
} catch (err) {
  console.log(err);
  return err;
}
};

exports.adminDashboard = async (req, res) => {
  try {
    req.body.address = '0x2FF312a35e4bBD2158997D90CC81bC298059F2D1';
    const BmtTokenInfo = module.exports.bmtDashboard(req.body.address);
    const OegTokenInfo = module.exports.oegDashboard(req.body.address);
    const FgmTokenInfo = module.exports.fgmDashboard(req.body.address);
    const IaiTokenInfo = module.exports.iaiDashboard(req.body.address);

    await Promise.all([BmtTokenInfo, OegTokenInfo, IaiTokenInfo, FgmTokenInfo])
      .then(data => {
        const respObj = {
          bmttoken: data[0],
          oegtoken: data[1],
          fgmtoken: data[3],
          iaitoken: data[2]
        };

        handleResponse({ res, data: respObj });
      })
      .catch(err => {
        handleError({ res, data: err });
      });
  } catch (err) {
    handleError({ res, data: err });
  }
};

(exports.addKycUser = async (req, res) => {
  try {
    const bmtKyc = adminService.addKycUser(req.body.address, 'BmtToken');

    const fgmKyc = adminService.addKycUser(req.body.address, 'FgmToken');

    const iaiKyc = adminService.addKycUser(req.body.address, 'IaiToken');

    const oegKyc = adminService.addKycUser(req.body.address, 'OegToken');

    await Promise.all([bmtKyc, fgmKyc, iaiKyc, oegKyc])
      .then(data => {
        handleResponse({ res, data: 'Added Kyc Successfully' });
      })
      .catch(err => {
        handleError({ res, data: 'Something went wrong' });
      });
  } catch (err) {
    handleError({ res, data: err });
  }
}),
  (exports.isKycUser = async (req, res) => {
    //console.log("sssssssssssssssssss9090",req)
    console.log("sssssssssssssssssss9090",req.body)
    console.log("sssssssssssssssssss params",req.query)
    try {
      //get wallet .. //check kyc
      let wallet = req.query.toAddress;

      let userDetails = await User.findOne({ wallet: wallet });
      console.log("sssssssssssssssssss",userDetails)
      console.log("sssssssssssssssssss",userDetails.flags.kyc.status)
      if (userDetails.flags.kyc.status === true ) {
        console.log("sssssssssssssssssss",userDetails.flags.kyc.status)

        handleResponse({ res, data: true });
      }
      else
      handleResponse({ res, data: false });

    } catch (err) {
      console.log("er2222", err)
      handleError({ res, data: err });
    }
  }),
  (exports.burnToken = async (req, res) => {
    try {
      let token = req.body.tokenName;

      if (token == 'bmt') {
        const burnedToken = await tokenService.burnToken(req.body.amount, 'BmtToken');

        if (burnedToken == false) {
          handleError({ res, data: 'Something went wrong , Reverted by EVM' });
        } else {
          handleResponse({ res, data: 'Burned tokens successfully' });
        }
      }

      if (token == 'fgm') {
        const burnedToken = await tokenService.burnToken(req.body.amount, 'FgmToken');

        if (burnedToken == false) {
          handleError({ res, data: 'Something went wrong , Reverted by EVM' });
        } else {
          handleResponse({ res, data: 'Burned tokens successfully' });
        }
      }

      if (token == 'oeg') {
        const burnedToken = await tokenService.burnToken(req.body.amount, 'OegToken');

        if (burnedToken == false) {
          handleError({ res, data: 'Something went wrong , Reverted by EVM' });
        } else {
          handleResponse({ res, data: 'Burned tokens successfully' });
        }
      }

      if (token == 'iai') {
        const burnedToken = await tokenService.burnToken(req.body.amount, 'IaiToken');

        if (burnedToken == false) {
          handleError({ res, data: 'Something went wrong , Reverted by EVM' });
        } else {
          handleResponse({ res, data: 'Burned tokens successfully' });
        }
      }
    } catch (err) {
      handleError({ res, data: err });
    }
  });

exports.getTransactionHistory = async (req, res) => {
  try {
    req.body.address = '0x2FF312a35e4bBD2158997D90CC81bC298059F2D1';
    if (req.body.address) {
      const history = await txService.tokenTransactionHistory(req.body.address);

      handleResponse({ res, data: history });

    } else {
      handleError({ res, data: { message: 'Invalid wallet address' } })
    }
  } catch (err) {
    handleError({ res, data: err });
  }
};

//node
exports.dashboard = async (req, res) => {
  try {
    var response = req.response;
    await getDashboardData(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('Error', err);
  }
};
exports.members = async (req, res) => {
  try {
    var response = req.response;
    await getMemberData(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) { console.log('Error', err) }
}
exports.userProfileData = async (req, res) => {
  try {
    var response = req.response;
    let userData = await userDataFetch(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('Error', err);
  }
};
exports.updateAdminProfile = async (req, res) => {
  try {
    var response = req.response;

    await userDetailsUpdate(req, response);

    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log("error..................", err)
  }
}

exports.changePassword = async (req, res) => {
  try {
    var response = req.response;

    await changePass(req, req.session.userDetails.email, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.userData = async (req, res) => {
  try {
    var response = req.response;

    await userDetail(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.kycRequests = async (req, res) => {
  try {
    var response = req.response;
    await userKycStatusData(req, response);

    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.addNewUser = async (req, res) => {
  try {

    var registerRecord = req.body.registerData;
    var response = req.response;
    var emailCheckFlag = await isUserRegistered(registerRecord.email, response);
    console.log("emailCheckFlag>>>>>>>>>>>>>>>>>>>>>>>>>>>>", emailCheckFlag);
    if (emailCheckFlag == true) {


      response.setStatus = 422
      response.setMessage = 'User with the same email id already exists.';
      response.setErrorStack = 'User with the same email id already exists.';
      //response
      res.status(response.getStatus).send(response.getResponse);

    } else {

      console.log('here....................', registerRecord.flags.kyc.status)

      console.log('registerRecord.....................', registerRecord);
      const walletAdd = await createWallet();
      const walletUpdate = updateUser(req.body.userId, walletAdd.walletAddress);

      const bmtKyc = adminService.addKycUser(walletAdd.walletAddress, 'BmtToken');

      const fgmKyc = adminService.addKycUser(walletAdd.walletAddress, 'FgmToken');

      const iaiKyc = adminService.addKycUser(walletAdd.walletAddress, 'IaiToken');

      const oegKyc = adminService.addKycUser(walletAdd.walletAddress, 'OegToken');
      console.log('here..............', walletAdd.walletAddress);
      registerRecord.wallet = walletAdd.walletAddress;


      const insert = insertUser(registerRecord, walletAdd, response);

      await Promise.all([bmtKyc, fgmKyc, iaiKyc, oegKyc, walletUpdate, insert])

        .then(data => {


          res.status(response.getStatus).send(response.getResponse);

          // res.status(response.getStatus).send(response.getResponse);
          // handleResponse({ res, data: 'Added Kyc Successfully' });
        })
        .catch(err => {
          handleError({ res, data: 'Something went wrong' });
        });


    }


  } catch (err) {

    console.log('here eror in', err);
  }

}

exports.statusChange = async (req, res) => {
  try {

    var response = req.response;

    //Update Kyc status and Send Email.
    await userKycVerify(req, response, null);

    //Get User Wallet Address.
    //     const userwallet = await adminService.getUserWallet(req.body.userId);
    // console.log("userrrrrrrrrrr",userwallet)
    //     //Add KYC.
    //     const bmtKyc = adminService.addKycUser(userwallet.wallet, 'BmtToken');

    //     const fgmKyc = adminService.addKycUser(userwallet.wallet, 'FgmToken');

    //     const iaiKyc = adminService.addKycUser(userwallet.wallet, 'IaiToken');

    //     const oegKyc = adminService.addKycUser(userwallet.wallet, 'OegToken');
    // console.log("hereeeeeeeeeee")
    // await Promise.all([bmtKyc, fgmKyc, iaiKyc, oegKyc])
    //       .then(data => {
    //         console.log("dataaaaaaaaaaaaaaaaaaaaaaaaaaaa",data[0])
    //     res.status(response.getStatus).send(response.getResponse);
    //   })
    //   .catch(err => {
    //     console.log("catchccccccccccccccccccccccccccccccccc")

    //     handleError({ res, data: 'Something went wrong' });
    //   }); 
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.documentStatusChange = async (req, res) => {
  try {
    var response = req.response;
    let data = await documentStatus(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.contactUs = async (req, res) => {
  try {
    var response = req.response;
    let reason = await contactUsDetails(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('Error', err);
  }
};
exports.contactUsView = async (req, res) => {
  try {
    var response = req.response;
    let reason = await contactUsDetailsView(req, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('Error', err);
  }
};

//Admin
exports.dashboard1 = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/admin_main',
      title: 'dashboard'
    };
    const userCount =  await User.count({ userRole: 'user' });

    const BmtTokenInfo = module.exports.bmtDashboard(req.session.userDetails.wallet);
    const OegTokenInfo = module.exports.oegDashboard(req.session.userDetails.wallet);
    const FgmTokenInfo = module.exports.fgmDashboard(req.session.userDetails.wallet);
    const IaiTokenInfo = module.exports.iaiDashboard(req.session.userDetails.wallet);

    await Promise.all([BmtTokenInfo, OegTokenInfo, IaiTokenInfo, FgmTokenInfo])
      .then(data => {
        const respObj = {
          bmttoken: data[0],
          oegtoken: data[1],
          fgmtoken: data[3],
          iaitoken: data[2]
        };

        const adminData = req.session.userDetails.wallet ? respObj : {};

        adminData.userCount = userCount;
        data.userData = req.session.userDetails;
        renderParams.data = adminData;
        renderParams.data.userData = req.session.userDetails;

        //console.log('renderParams........', renderParams);
        res.render('admin/dashboard', renderParams);

        //   handleResponse({res,data:respObj})
      })
      .catch(err => {
        handleError({ res, data: err });
      });
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};
exports.addUser = async (req, res) => {
  try {
    console.log('here');
    let renderParams = {
      layout: 'layouts/admin_main',
      title: 'addMember'
    }
    const data = {};
    data.userData = req.session.userDetails;
    renderParams.data = data;
    res.render('admin/addMember', renderParams);

  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};
exports.members1 = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/admin_main',
      title: 'members'
    };
    let response = req.response;
    let userData = await getMemberData(req, response);

    console.log('userData.............', userData);

    let userArr = [];
    let userObj = {};
    let kycStatus = '';
    userData.forEach(element => {
      if (element.flags.kyc.isKyc == 'verified') {
        kycStatus = "Approved";
      }
      if (element.flags.kyc.isKyc == 'reject') {
        kycStatus = "Rejected";
      }
      if (element.flags.kyc.isKyc == 'suspend') {
        kycStatus = "Disabled";
      }
      if (element.flags.kyc.isKyc == 'notVerified') {
        kycStatus = "Not verified";
      }
      userObj = {
        email: element.email,
        name: element.firstName + ' ' + element.lastName,
        phone: element.phoneNumber,
        addedBy: element.addedBy,
        userId: element._id,
        status: kycStatus,
      };
      userArr.push(userObj);
    });

    const data = {};
    data.userData = req.session.userDetails;
    data.userDetails = userArr;
    req.session.userDetails.prevUrl = req.path;
    renderParams.data = data;

    res.render('admin/members', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

/*exports.token = async (req, res) => {
    try {
        let renderParams = {
            layout: 'layouts/admin_main'
        };
        const data = {};
        data.userData = req.session.userDetails;
        renderParams.data = data;
        res.render('admin/token', renderParams);
    } catch (err) {
        handleError({
            res,
            data: err
        });
    }
};*/
exports.kycRequest = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/admin_main',
      title: 'kycRequest'
    };
    let pending = await userKycStatusData('pending');
    let verified = await userKycStatusData('verified');
    let reject = await userKycStatusData('reject');
    let pendingArr = []
    let pendingObj = {}

    pending.forEach(element => {
      pendingObj = {
        email: element.email,
        name: element.firstName + " " + element.lastName,
        phone: element.phoneNumber,
        addedBy: element.addedBy,
        userId: element._id,
        timestamp :moment(element.timestamp).format("DD/MM/YYYY"),
      }
      pendingArr.push(pendingObj)
    });

    let verifiedArr = []
    let verifiedObj = {}

    verified.forEach(element => {
      verifiedObj = {
        email: element.email,
        name: element.firstName + " " + element.lastName,
        phone: element.phoneNumber,
        addedBy: element.addedBy,
        userId: element._id,
        wallet : element.wallet,
        timestamp :moment(element.timestamp).format("DD/MM/YYYY"),
      }
      verifiedArr.push(verifiedObj)
     

    });
    console.log("KKKKKKKKKKKKK",verifiedArr)
    let rejectArr = []
    let rejectObj = {}

    reject.forEach(element => {
      rejectObj = {
        email: element.email,
        name: element.firstName + " " + element.lastName,
        phone: element.phoneNumber,
        addedBy: element.addedBy,
        userId: element._id,
        reason: element.flags.kyc.kycRejectReason,
        timestamp :moment(element.timestamp).format("DD/MM/YYYY"),
        
      }
      rejectArr.push(rejectObj)
    });

    const data = {
      pending: pendingArr,
      verified: verifiedArr,
      reject: rejectArr
    };

    console.log("KYC Pending",data.pending)
    console.log("KYC Verified",data.verified)

    console.log("KYC Reject",data.reject)

    // const data = {};
    data.userData = req.session.userDetails;
    req.session.userDetails.prevUrl = req.path;
    renderParams.data = data;
    console.log("renderParams........", renderParams)
    res.render('admin/kycRequest', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};
exports.transactions = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/admin_main',
      title: 'transactions'
    };

    let walletAddress = req.session.userDetails.wallet
    const history = await txService.tokenTransactionHistory(walletAddress);

    var historyData = history.map(el => {
      let o = Object.assign({}, el);
      o.newDate = el.date;
      return o;
    })

    const data = {};
    data.userData = req.session.userDetails;
    data.history = walletAddress ? historyData : {}
    data.walletAddress = walletAddress
    renderParams.data = data;

    res.render('admin/transactions', renderParams);
  } catch (err) {
    console.log("errr.............", err)
    handleError({
      res,
      data: err
    });
  }
};

exports.myAccount = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/admin_main'
    };
    const data = {};
    data.userData = req.session.userDetails;
    renderParams.data = data;
    res.render('admin/my-account', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.logout = async (req, res) => {
  try {
    req.session.isLoggedIn = false;
    res.redirect('login');
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.login = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/login'
    };
    const data = {};
    data.userData = req.session.userDetails;
    renderParams.data = data;
    res.render('admin/login', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.memberDetails = async (req, res) => {
  try {
    let renderParams = {
      title: req.session.userDetails.prevUrl,
      layout: 'layouts/admin_main'
    };

    let user = await adminService.getUserDetail(req.query.userId);

    const transactions = txService.getTnxHistory(user.wallet);
    const bmtBalance = tokenService.getBalance(user.wallet, 'BmtToken')
    const fgmBalance = tokenService.getBalance(user.wallet, 'FgmToken')
    const oegBalance = tokenService.getBalance(user.wallet, 'OegToken')
    const iaiBalance = tokenService.getBalance(user.wallet, 'IaiToken')
    const bal = await txService.getEtherBalance(user.wallet)
    await Promise.all([transactions, bmtBalance, fgmBalance, oegBalance, iaiBalance, bal])
      .then(historyData => {
        const respObj = {
          transactionHistory: historyData[0],
          bmtBalance: historyData[1],
          fgmBalance: historyData[2],
          oegBalance: historyData[3],
          iaiBalance: historyData[4],
          bal: historyData[5]
        }
        let kycDisabled = (user.flags.kycUploadAddressProof.isKyc == 'verified' && user.flags.kycUploadIdProof.isKyc == 'verified' && user.flags.KycUploadWithSelfiePhoto.isKyc == 'verified' && user.flags.kyc.isKyc != 'verified') ? '' : 'disabled'
        let approveOrReject = (user.flags.kyc.isKyc == 'verified' || user.flags.kyc.isKyc == 'reject') ? 'yes' : 'no';
        let kycStatus = '';
        if (user.flags.kyc.isKyc == 'verified') {
          kycStatus = "Approved";
        }
        if (user.flags.kyc.isKyc == 'reject') {
          kycStatus = "Rejected";
        }
        if (user.flags.kyc.isKyc == 'suspend') {
          kycStatus = "Disabled";
        }
        if (user.flags.kyc.isKyc == 'notVerified') {
          kycStatus = "Not verified";
        }
        let userObj = {
          name: user.firstName + ' ' + user.lastName,
          phoneNumber: user.phoneNumber,
          email: user.email,
          dob: user.dob,
          address: user.address,
          country: user.country,
          city: user.city,
          zipCode: user.zipCode,
          countryCode: user.countryCode,
          kycIdProof: user.kycDetails.kycIdProof,
          kycSelfieProof: user.kycDetails.KycPhotoId,
          kycAddressProof: user.kycDetails.KycAddressProof,
          kycDisabled: kycDisabled,
          kycIdProofType: user.kycDetails.kycIdProof ? user.kycDetails.kycIdProof.substring(user.kycDetails.kycIdProof.length - 3) : 0,
          kycKycUploadWithSelfiePhotoType: user.kycDetails.KycPhotoId ? user.kycDetails.KycPhotoId.substring(user.kycDetails.KycPhotoId.length - 3) : 0,
          kycAddressProofType: user.kycDetails.KycAddressProof ? user.kycDetails.KycAddressProof.substring(user.kycDetails.KycAddressProof.length - 3) : 0,
          kycIdProofStatus: user.flags.kycUploadIdProof.isKyc,
          kycAddressProofStatus: user.flags.kycUploadAddressProof.isKyc,
          KycUploadWithSelfiePhotoStatus: user.flags.KycUploadWithSelfiePhoto.isKyc,
          kycStatus: kycStatus,
          approveOrReject: approveOrReject,
          blockUser: user.flags.block_user,
          wallet: user.wallet
        };


        let imgArr = [];

        console.log("userObj.kycIdProofType.....", userObj.kycIdProofType)
        console.log("userObj.kycKycUploadWithSelfiePhotoType........", userObj.kycKycUploadWithSelfiePhotoType)
        console.log("userObj.kycAddressProofType.......", userObj.kycAddressProofType)

        if (userObj.kycIdProofType != 'pdf') {
          imgArr.push("idProof/" + userObj.kycIdProof)
        }

        if (userObj.kycAddressProofType != 'pdf') {
          imgArr.push("addressProof/" + userObj.kycAddressProof)
        }

        if (userObj.kycKycUploadWithSelfiePhotoType != 'pdf') {
          imgArr.push("withSelfiePhotoIdFile/" + userObj.kycSelfieProof)
        }

        userObj.imgArr = imgArr
        const data = {};
        data.userData = req.session.userDetails;
        data.user = userObj;
        data.history = user.wallet ? respObj : {};
        renderParams.data = data;

        console.log("***********************1111....", renderParams.data)

        res.render('admin/members-details', renderParams);

      })
      .catch(err => {
        console.log("err1...", err)
        handleError({ res, data: err })
      })
  } catch (err) {
    console.log("err2....", err)
    handleError({
      res,
      data: err
    });
  }
};

exports.userSuspend = async (req, res) => {
  try {

    let userSuspendData = await userSuspend(req.body.userId)
    let user = await adminService.getUserDetail(req.body.userId);

    if (user.flags.block_user) {
      res.status(200).send(userSuspendData);
    } else {
      res.status(400).send(userSuspendData);
    }

  } catch (err) {
    console.log('err...', err)
    throw err
  }
}

exports.wallet = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/admin_main',
      title: 'wallet',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    let getUser = await getUserDetails(req.session.userDetails._id)
    let userWallet = getUser.wallet

    const bal = await txService.getEtherBalance(userWallet)

    req.session.userDetails.wallet = getUser.wallet;

    const data = { bal };
    data.userData = req.session.userDetails;
    renderParams.data = data;

    res.render('user/wallet', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};
exports.MultiSend = async(req, res) => {
  try{
    console.log("reqqqqqqqqq",req.body)

    let walletsStr = req.body.wallets
    let wallets   = walletsStr.split(",");
    console.log("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwww",wallets)
    for(let i=0 ; i<wallets.length; i++){
      // let getUser = await getUserDetails(req.session.userDetails._id)
      // with mnemonicAddredss
      let userWallet = '0x2FF312a35e4bBD2158997D90CC81bC298059F2D1'
  
      // without mnemonicAddredss
      // let userWallet = '0x2FF312a35e4bBD2158997D90CC81bC298059F2D1'
  
        let token = req.body.token;console.log("tokeeeeeeen",token)
        let tokenName  = token+'Token'
        let tokenAddress = token+'TokenAddress'
        console.log('token name ---->',tokenName)
        console.log('token Address--->',tokenAddress)
        // console.log("req",req);
        console.log("userWallet",userWallet);
        console.log("req.body.toAddress",req.body.toAddress);
        console.log("req.body.amount",req.body.amount);
        console.log("tokenName",tokenName);
        console.log("tokenAddress",tokenAddress);
  
        const transaction = await tokenService.bmtTransfer(req,userWallet,wallets[i], req.body.amount,tokenName,tokenAddress);
        console.log('trans ======', transaction)
        // if (transaction == false) {
        //   handleError({
        //     res,
        //     data: {
        //       message: 'Transaction has been reverted by evm'
        //     }
        //   });
        // } else {
        //   handleResponse({
        //     res,
        //     data: transaction
        //   });
        // }
  
    }
    handleResponse({
          res,
          data: 'success'
        });
  } catch (err){
    console.log('error==-=-=-=-=-=-=-=-=-=>',err)
  }
}