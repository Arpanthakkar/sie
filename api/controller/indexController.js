var session = require('express-session');

const {
  insertUser,
  isUserRegistered,
  otpCheck,
  emailCheck,
  loginData,
  forgotPass,
  hashCreate,
  setPass,
  logoutUser,
  tokenCheck,
  signupEmailToken,
  deleteUser,
} = require('../services/indexService');
const { BASEURL } = require('../config');
exports.userRegister = async (req, res) => {
  try {
    var registerRecord = req.body.registerData;
    var response = req.response;
    var emailCheckFlag = await isUserRegistered(registerRecord.email, response);
    console.log("emailCheckFlag>>>>>>>>>>>>>>>>>>>>>>>>>>>>", emailCheckFlag);
    if (emailCheckFlag == true) {

      console.log("EMAIL CHECK FLAG TRUE", response.Response.data.flags.isVerifyMobile)
      if (response.Response.data.flags.isVerifyMobile == false) {
        console.log("true", response.Response.data.flags.isVerifyMobile)
        const deletedUser = await deleteUser(response.Response.data._id, response);
        console.log("DELETE USER", deleteUser);
        await insertUser(registerRecord, response);
        console.log("INSERT AGAIN USER")
      } else {
        console.log("response.Response.data.flags.isVerifyMobile", response.Response.data.flags.isVerifyMobile)
        response.setStatus = 422
        response.setMessage = 'You are already registered, Please Login.';
        response.setErrorStack = 'You are already registered, Please Login.';
        //response
        res.status(response.getStatus).send(response.getResponse);
      }
    } else {
      await insertUser(registerRecord, response);
      //await mail(registerRecord.email, "Ap Developement", "Successfully Registerd with Ap Developement Please Verify your Email");
    }
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('error', err);
  }
};
exports.otpGenerate = async (req, res) => {
  try {
    var response = req.response;
    // if(req.body.otp == ''){
    //   response.setStatus = 422;
    //   response.setMessage = 'Enter Verification Code';
    //   response.setErrorStack = 'Enter Verification Code';
    //   }else{
      await otpCheck(req.body.email, req.body.otp, response);
    //  }
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('error', err);
  }
};

exports.emailTokenCheck = async (req, res) => {
  try {
    var response = req.response;

    await emailCheck(req.body.token, response);
    if (response.getStatus == 200) {

      res.redirect('../user/successfullyVerified')

      res.status(response.getStatus).send(response.getResponse);
    } else {
      res.redirect('../user/successfullyVerified')

      res.status(response.getStatus).send(response.getResponse);

    }

  } catch (err) {
    console.log('error', err);
  }
};
exports.login = async (req, res) => {
  try {
    
    var response = req.response;
    var email = req.body.loginData.email.trim();
    var password = req.body.loginData.password;
    await loginData(req,response, email, password);

    if (response.Response.status == 200 ){
      let obj = {}
      obj.username = response.Response.data.firstName
      obj.id = response.Response.data._id
      req.session.isLoggedIn = true
      req.session.loggedInType = req.body.type === 'admin' ? 'admin' : 'user'
    }

    req.session.userDetails = response.Response.data


    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Login Failed';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.forgot = async (req, res) => {
  try {
    var response = req.response;
    const data = await forgotPass(req.body.email);
    console.log(data)
    if (data) {
      await hashCreate(data, data._id, response)
        .then(() => {
          res.status(response.getStatus).send(response.getResponse);
        })
        .catch(() => {
          res.status(response.getStatus).send(response.getResponse);
        });
    } else {
      response.setStatus = 422;
      response.setMessage = 'Email Id does not exist.';
      response.setErrorStack = 'Email Id does not exist.';
      res.status(response.getStatus).send(response.getResponse);
    }
  } catch (err) {
    console.log('error', err);
  }
};
exports.setPassword = async (req, res) => {
  try {
    var response = req.response;
    var token = req.body.token;
   
    var password = req.body.conPassword;
    await setPass(token, password, response)
      .then(() => {
        res.status(response.getStatus).send(response.getResponse);
      })
      .catch(() => {
        res.status(response.getStatus).send(response.getResponse);
      });
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Password Not changed';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.logout = async (req, res) => {
  try {
    var response = req.response;
    console.log(req.decode);
    var token = req.decode.token;
    var email = req.decode.tokenEmail;
    await logoutUser(token, email, response);

    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Logout Failed';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.token = async (req, res) => {
  try {
    var response = req.response;
    let token = req.params.Token;

    await signupEmailToken(token, res, response);
    let renderParams = {
      layout: 'layouts/login',
      // title: 'dashboard',
      // pageTilte: 'Dashboard',
      // BASE_URL: CONST.BASE_URL
      
    };
    if (response.getStatus == 200) {
      console.log("RESPONSE...........", response)
      console.log('in controller')
      let renderParams = {
        layout: 'layouts/login',
        // title: 'dashboard',
        // pageTilte: 'Dashboard',
        // BASE_URL: CONST.BASE_URL
        
      };
      renderParams.data = response.Response.message;
     
      res.render('user/linkExpired',renderParams)

      // res.render('login', renderParams);
    } else {
      console.log('else part/..................')
      renderParams.data = response.Response.message;
     
      res.render('user/linkExpired',renderParams)
      // res.status(response.getStatus).send(response.getResponse);
    }
  } catch (err) { console.log("error................", err); }
}
exports.resetPass = async (req, res) => {
  try {
 
    let renderParams = {
      layout: 'layouts/login'
      //   title: 'dashboard',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    res.render('user/resetPassword', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
}