import tokenService from '../services/token.service';
import txService from '../services/transaction';
import { handleResponse, handleError } from '../middleware/responsehandler';
import { constants } from 'crypto';
import { TIMEOUT } from 'dns';
import { createWallet,verifyMnemonic } from '../services/wallet.service'
import { get } from 'http';
const { userDetails, kycUserDetails, userDetailsUpdate, changePass, agreement,contactUsDetailsView, 
  forgotTokenCheck,changeNumber,changeNumberCheck, getUserDetails, changeNumberSignUp, getUserType ,updateSecretQRCode,changeAuthenticatorFlag, logoutUser, updateUser} = require('../services/providerService');
const { BASEURL } = require('../config');

var QRCode = require('qrcode');
var speakeasy = require("speakeasy");
import adminService from '../services/admin.service';
// import { updateUser } from './admin.controller';

const {resetPasswordToken } = require('../services/indexService');

// exports.getUserDashboard = async (req, res) => {
//   try {
//     req.body.address = '0x272995694Fd08920E8C93EdCac76a059D07B5c21';

//     const BmtTokenInfo = tokenService.getTokenInfo(req.body.address, 'BmtToken');
  
//     const FgmTokenInfo = tokenService.getTokenInfo(req.body.address, 'FgmToken');
   
//     const IaiTokenInfo = tokenService.getTokenInfo(req.body.address, 'IaiToken');
   
//     const OegTokenInfo = tokenService.getTokenInfo(req.body.address, 'OegToken');
   
//     const receiveAmount = txService.getReceiveToken(req.body.address);
   
//     const sendAmount = txService.getSentToken(req.body.address);
//     const txHistory = txService.dashboardTxnHistory(req.body.address)
//     await Promise.all([BmtTokenInfo, FgmTokenInfo, IaiTokenInfo, OegTokenInfo, receiveAmount, sendAmount, txHistory])
//       .then(data => {
       
//         let totalToken = 0;
//         for (let i = 0; i < 4; i++) {
//           console.log(data[i].balance);
//           totalToken += Number(data[i].balance);
//         }
    
//         const respObj = {
//           bmt: data[0],
//           fgm: data[1],
//           iai: data[2],
//           oeg: data[3],
//           send: data[5],
//           received: data[4],
//           total: totalToken,
//           history: data[6]
//         };
      
//         handleResponse({
//           res,
//           data: respObj
//         });
//       })
//       .catch(err => {
//         console.log('ERROR => ', err)
//         handleError({ res, data: err })
//       });
//   } catch (err) {
//     console.log('error=>', err)
//     handleError({
//       res,
//       data: err
//     });
//   }
// };

exports.bmtDashboard = async(address) => {
  try{
    const BmtTokenInfo = await tokenService.getTokenInfo(address, 'BmtToken')
   
    // const sent = txService.getSentToken(address, 'BmtTokenAddress')
   
    // const recieve = txService.getReceiveToken(address, 'BmtTokenAddress')
    const history = await tokenService.getTxList(address, "BmtTokenAddress")

    // let result = await Promise.all([BmtTokenInfo,sent,recieve,history]).then(data => {
      let sentAmount = 0
      let receiveAmount = 0
      // let burnAmount = 0
      for (let i in history) {
        if (history[i].details == 'sent') {
          sentAmount += Number(history[i].quantity)
        }
        if (history[i].details == 'received') {
          receiveAmount += Number(history[i].quantity)
        }
        // if (history[i].to == '0x0000000000000000000000000000000000000000') {
        //   burnAmount += Number(history[i].quantity)
        // }
      }
      sentAmount = Number.parseFloat(sentAmount).toFixed(8)
      receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
      // burnAmount = Number.parseFloat(burnAmount).toFixed(8)
      // const outstandingToken = Number(BmtTokenInfo.totalTokens) - Number(sentAmount)
      const respObj = {
        token: BmtTokenInfo,
        sent: sentAmount,
        received: receiveAmount,
        // burnToken: burnAmount,
        // outstandingToken : outstandingToken,
        history: history
      };
      console.log('respobj===============', respObj);
        return respObj;
    
  }catch(err){
    console.log(err)
    return err
  }
}

exports.fgmDashboard = async(address) => {
  try{
    const FgmTokenInfo = await tokenService.getTokenInfo(address, 'FgmToken')
    // const sent = txService.getSentToken(address, 'FgmTokenAddress')
   
    // const recieve = txService.getReceiveToken(address, 'FgmTokenAddress')
    const history = await tokenService.getTxList(address, "FgmTokenAddress")

    // let result = await Promise.all([FgmTokenInfo,sent,recieve,history]).then(data => {
      let sentAmount = 0
      let receiveAmount = 0
      // let burnAmount = 0
      for (let i in history) {
        if (history[i].details == 'sent') {
          sentAmount += Number(history[i].quantity)
        }
        if (history[i].details == 'received') {
          receiveAmount += Number(history[i].quantity)
        }
        // if (history[i].to == '0x0000000000000000000000000000000000000000') {
        //   burnAmount += Number(history[i].quantity)
        // }
      }
      sentAmount = Number.parseFloat(sentAmount).toFixed(8)
      receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
      // burnAmount = Number.parseFloat(burnAmount).toFixed(8)
      // const outstandingToken = Number(BmtTokenInfo.totalTokens) - Number(sentAmount)
      const respObj = {
        token: FgmTokenInfo,
        sent: sentAmount,
        received: receiveAmount,
        // burnToken: burnAmount,
        // outstandingToken : outstandingToken,
        history: history
      };
      console.log('respobj===============', respObj);
        return respObj;
    // })

    // return result
    
  }catch(err){
    console.log(err)
    return err
  }
}

exports.oegDashboard = async(address) => {
  try{
    const OegTokenInfo = await tokenService.getTokenInfo(address, 'OegToken')
    // const sent = txService.getSentToken(address, 'OegTokenAddress')
  
    // const recieve = txService.getReceiveToken(address, 'OegTokenAddress')
    const history = await tokenService.getTxList(address, "OegTokenAddress")

    // let result = await Promise.all([OegTokenInfo,sent,recieve,history]).then(data => {
    let sentAmount = 0
    let receiveAmount = 0
    let burnAmount = 0
    for (let i in history) {
      if (history[i].details == 'sent') {
        sentAmount += Number(history[i].quantity)
      }
      if (history[i].details == 'received') {
        receiveAmount += Number(history[i].quantity)
      }
    }
    sentAmount = Number.parseFloat(sentAmount).toFixed(8)
    receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
    // burnAmount = Number.parseFloat(burnAmount).toFixed(8)
    // const outstandingToken = Number(BmtTokenInfo.totalTokens) - Number(sentAmount)
    const respObj = {
      token: OegTokenInfo,
      sent: sentAmount,
      received: receiveAmount,
      // burnToken: burnAmount,
      // outstandingToken : outstandingToken,
      history: history
    };
    console.log('respobj===============', respObj);
      return respObj;
    // })

    // return result
    
  }catch(err){
    console.log(err)
    return err
  }
}

exports.iaiDashboard = async(address) => {
  try{
    const IaiTokenInfo =await tokenService.getTokenInfo(address, 'IaiToken')
    // const sent = txService.getSentToken(address, 'IaiTokenAddress')
   
    // const recieve = txService.getReceiveToken(address, 'IaiTokenAddress')
    const history = await tokenService.getTxList(address, "IaiTokenAddress")

    // let result = await Promise.all([IaiTokenInfo,sent,recieve,history]).then(data => {
      let sentAmount = 0
      let receiveAmount = 0
      // let burnAmount = 0
      for (let i in history) {
        if (history[i].details == 'sent') {
          sentAmount += Number(history[i].quantity)
        }
        if (history[i].details == 'received') {
          receiveAmount += Number(history[i].quantity)
        }
        // if (history[i].to == '0x0000000000000000000000000000000000000000') {
        //   burnAmount += Number(history[i].quantity)
        // }
      }
      sentAmount = Number.parseFloat(sentAmount).toFixed(8)
      receiveAmount = Number.parseFloat(receiveAmount).toFixed(8)
      // burnAmount = Number.parseFloat(burnAmount).toFixed(8)
      // const outstandingToken = Number(BmtTokenInfo.totalTokens) - Number(sentAmount)
      const respObj = {
        token: IaiTokenInfo,
        sent: sentAmount,
        received: receiveAmount,
        // burnToken: burnAmount,
        // outstandingToken : outstandingToken,
        history: history
      };
      console.log('respobj===============', respObj);
        return respObj;
    // })

    // return result
    
  }catch(err){
    console.log(err)
    return err
  }
}

exports.dashboard = async(req,res) => {
  try{
    const BmtTokenInfo = module.exports.bmtDashboard(req.body.address)
    const OegTokenInfo = module.exports.oegDashboard(req.body.address)
    const FgmTokenInfo = module.exports.fgmDashboard(req.body.address)
    const IaiTokenInfo = module.exports.iaiDashboard(req.body.address)
    

    await Promise.all([BmtTokenInfo,OegTokenInfo,IaiTokenInfo,FgmTokenInfo]).then(data => {
      const respObj = {
        bmttoken:data[0],
        oegtoken:data[1],
        fgmtoken:data[2],
        iaitoken:data[3]
      }

      handleResponse({res,data:respObj})
    })
    .catch(err => {
          handleError({res,data:err})
        })
  }catch(err){
    handleError({res,data:err})
  }
}

exports.getTransactionHistory = async (req, res) => {
  try {
    req.body.address = '0x272995694Fd08920E8C93EdCac76a059D07B5c21';

    if(req.body.address){

      const transactions =txService.getTnxHistory(req.body.address);
      const bmtBalance = tokenService.getBalance(req.body.address,'BmtToken')
      const fgmBalance = tokenService.getBalance(req.body.address,'FgmToken')
      const oegBalance = tokenService.getBalance(req.body.address,'OegToken')
      const iaiBalance = tokenService.getBalance(req.body.address,'IaiToken')
      
      await Promise.all([transactions, bmtBalance, fgmBalance, oegBalance, iaiBalance])
          .then(data => {
            const respObj = {
              transactionHistory: data[0],
              bmtBalance: data[1],
              fgmBalance: data[2],
              oegBalance: data[3],
              iaiBalance: data[4]
            }
            console.log("responsssssssssssssssssssssssssssse",res)
                handleResponse({
                  res,
                  data: respObj
                });
                
          })
          .catch(err => {
            handleError({res,data:err})
          })
    }else{
      handleError({res,data:{message:'Invalid wallet address'}})
    }
 
    
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.transferToken = async (req, res) => {
  try {
    let getUser = await getUserDetails(req.session.userDetails._id)
    // with mnemonicAddredss
    let userWallet = getUser.wallet

    // without mnemonicAddredss
    // let userWallet = '0x2FF312a35e4bBD2158997D90CC81bC298059F2D1'

      let token = req.body.token;console.log("tokeeeeeeen",token)
      let tokenName  = token+'Token'
      let tokenAddress = token+'TokenAddress'
      console.log('token name ---->',tokenName)
      console.log('token Address--->',tokenAddress)
      console.log("req",req);
      console.log("userWallet",userWallet);
      console.log("req.body.toAddress",req.body.toAddress);
      console.log("req.body.amount",req.body.amount);
      console.log("tokenName",tokenName);
      console.log("tokenAddress",tokenAddress);

      const transaction = await tokenService.bmtTransfer(req,userWallet,req.body.toAddress, req.body.amount,tokenName,tokenAddress);
    
      if (transaction == false) {
        handleError({
          res,
          data: {
            message: 'Transaction has been reverted by evm'
          }
        });
      } else {
        handleResponse({
          res,
          data: transaction
        });
      }
    // if (token == 'fgm') {
    //   const transaction = await tokenService.transfer(req.body.address, req.body.amount, 'FgmToken');
    
    //   if (transaction == false) {
    //     handleError({
    //       res,
    //       data: {
    //         message: 'Transaction has been reverted by evm'
    //       }
    //     });
    //   } else {
    //     handleResponse({
    //       res,
    //       data: transaction
    //     });
    //   }
    // }
    // if (token == 'iai') {
    //   const transaction = await tokenService.transfer(req.body.address, req.body.amount, 'IaiToken');
    
    //   if (transaction == false) {
    //     handleError({
    //       res,
    //       data: {
    //         message: 'Transaction has been reverted by evm'
    //       }
    //     });
    //   } else {
    //     handleResponse({
    //       res,
    //       data: transaction
    //     });
    //   }
    // }
    // if (token == 'oeg') {
    //   const transaction = await tokenService.transfer(req.body.address, req.body.amount, 'OegToken');
    
    //   if (transaction == false) {
    //     handleError({
    //       res,
    //       data: {
    //         message: 'Transaction has been reverted by evm'
    //       }
    //     });
    //   } else {
    //     handleResponse({
    //       res,
    //       data: transaction
    //     });
    //   }
    // }
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.etherEstimate = async (req, res) => {
  try{

    const {totalGasUsedInGwei, totalGasUsedInEther} = await tokenService.getTotalEtherEstimate(req.body)
    console.log('gwei', totalGasUsedInGwei)
    console.log('obj ether',totalGasUsedInEther)
    handleResponse({res,data:{
 message:'gas amount in gwei and ether.',  Gas: totalGasUsedInGwei, GasinEther: totalGasUsedInEther}});
  } catch (err) {
    handleError({res,data:{message:'Some error occured: ' + err, err}})
  }
}

exports.getAdminWallet = async (req, res) => {
  try {
    const bmtOwner = tokenService.getOwnerAddress('BmtToken');
    const fgmOwner = tokenService.getOwnerAddress('FgmToken');
    const oegOwner = tokenService.getOwnerAddress('OegToken');
    const iaiOwner = tokenService.getOwnerAddress('IaiToken');

    await Promise.all([bmtOwner, fgmOwner, oegOwner, iaiOwner]).then(data => {
      const respObj = {
        bmt: data[0],
        fgm: data[1],
        oeg: data[2],
        iai: data[3]
      };

      handleResponse({
        res,
        data: respObj
      });
    });
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.getBalance = async (req, res) => {
  try {
    const bal = await txService.getEtherBalance(req.body.address)
  
    handleResponse({ res, data: bal })
  } catch (err) {
  
    handleError({ res })
  }
}

exports.genrateWallet = async(req,res) => {
  try{
    console.log("sesssisonnnnnn",req.session.userDetails)
    const userwallet = await adminService.getUserWallet(req.session.userDetails._id);
    console.log("userrrrwallet",userwallet.wallet);
    if(userwallet.wallet!=null)
    {

      handleResponse({res,data:"true"})
    }
    else{
      //const wallet = await createWallet()
   //   const walletUpdate = await updateUser(req.session.userDetails._id, wallet.walletAddress);
      handleResponse({res,data:"false"})
      //display copy toclipnoard
    }
   

  }catch(err){
    handleError({res,data:err})
  }
}

exports.verifyLogin = async(req,res) => {
  try{
    const result = await verifyMnemonic(req)
    if(result == true){
      handleResponse({res,data:"Mnemonic Value Matched!!"})
    }else{
      handleError({res,data:"Incorrect Mnemonic value!!"})
    }
  }catch(err){
    console.log(err)
    handleError({res,data:err})
  }
}

exports.dashboard1 = async (req, res) => {
  try {

    let getUser = await getUserDetails(req.session.userDetails._id)

    if (getUser.isGoogleAuthorised == true && getUser.wallet == null) {
      res.redirect('createWallet')
    } else {

    let userWallet = getUser.wallet

    let renderParams = {
      layout: 'layouts/main',
      title: 'dashboard',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    const BmtTokenInfo = module.exports.bmtDashboard(userWallet)
    const OegTokenInfo = module.exports.oegDashboard(userWallet)
    const FgmTokenInfo = module.exports.fgmDashboard(userWallet)
    const IaiTokenInfo = module.exports.iaiDashboard(userWallet)
    const bal = txService.getEtherBalance(userWallet)

    await Promise.all([BmtTokenInfo,OegTokenInfo,IaiTokenInfo,FgmTokenInfo, bal]).then(data => {
      const respObj = {
        bmttoken:data[0],
        oegtoken:data[1],
        fgmtoken:data[2],
        iaitoken:data[3],
        bal: data[4]
      }
      
      respObj.userData = req.session.userDetails;
      renderParams.data = respObj;

      res.render('user/dashboard', renderParams);
    })
    .catch(err => {
      console.log("11111111", err)
          handleError({res,data:err})
        })
     }
  } catch (err) {
    console.log('2222222', err)
    handleError({
      res,
      data: err
    });
  }
};

exports.history = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/main',
      title: 'history',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    let getUser = await getUserDetails(req.session.userDetails._id)
    let userWallet = getUser.wallet

    const transactions =txService.getTnxHistory(userWallet);
    const bmtBalance = tokenService.getBalance(userWallet,'BmtToken')
    const fgmBalance = tokenService.getBalance(userWallet,'FgmToken')
    const oegBalance = tokenService.getBalance(userWallet,'OegToken')
    const iaiBalance = tokenService.getBalance(userWallet,'IaiToken')
    await Promise.all([transactions, bmtBalance, fgmBalance, oegBalance, iaiBalance])
        .then(data => {
          const respObj = {
            transactionHistory: data[0],
            bmtBalance: data[1],
            fgmBalance: data[2],
            oegBalance: data[3],
            iaiBalance: data[4]
          }

          console.log("historyyyyyyyyyyyyyyyyyyyyy",transactions)

          const walletdata        = respObj;
          walletdata.userData     = req.session.userDetails;
          renderParams.data = walletdata;
          renderParams.data.userWallet = userWallet
          res.render('user/history', renderParams);
        })
        .catch(err => {
          handleError({res,data:err})
        })
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.wallet = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/main',
      title: 'wallet',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    let getUser = await getUserDetails(req.session.userDetails._id)
    let userWallet = getUser.wallet

    const bal = await txService.getEtherBalance(userWallet)

    req.session.userDetails.wallet = getUser.wallet;

    const data        = { bal };
    data.userData     = req.session.userDetails;
    renderParams.data = data;

    res.render('user/wallet', renderParams);   
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.contactUs = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/main',
      title: 'contactUs',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    const data        = {};
    data.userData     = req.session.userDetails;
    renderParams.data = data;
    res.render('user/contactUs', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.signUp = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/login'
      //   title: 'dashboard',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    res.render('user/signUp', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.kycVerificationStep1 = async (req, res) => {
  try {
    console.log("kyc...........")
    let renderParams = {
      layout: 'layouts/main',
      title: 'kycVerification',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    const data = {};
    data.userData = req.session.userDetails;
    renderParams.data = data;

    res.render('user/kycVerificationStep1', renderParams);


    // if (
    //   kyc.flagsKycUploadAddressProofIsKyc == 'pending' &&
    //   kyc.flagsKycUploadIdProofIsKyc == 'pending' &&
    //   kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'pending'
    // ) {
    //   //true
    //   let kycData = {
    //     message: 'Your Kyc Documents Are Under Review',
    //     smily: 'smilyPending.png'
    //   };
    //   renderParams.data.kyc = kycData;
    //   res.render('user/kycVerification', renderParams);
    // } else if (
    //   kyc.flagsKycUploadAddressProofIsKyc == 'notVerified' &&
    //   kyc.flagsKycUploadIdProofIsKyc == 'notVerified' &&
    //   kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'notVerified'
    // ) {
    //   res.render('user/kycVerificationStep1', renderParams);

    // } else if (
    //   kyc.flagsKycUploadAddressProofIsKyc == 'reject' ||
    //   kyc.flagsKycUploadIdProofIsKyc == 'reject' ||
    //   kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'reject'
    // ) {
    //   res.render('user/kycVerificationStep1', renderParams);
    // } else if (kyc.kycStatus == 'reject') {
    //   let kycData = {
    //     message: 'Your Kyc Request Is Rejected',
    //     smily: 'smilyReject.png'
    //   };
    //   renderParams.data.kyc = kycData;
    //   res.render('user/kycVerification', renderParams);
    // } else if (kyc.kycStatus == 'pending') {
    //   let kycData = {
    //     message: 'Your Kyc Request Is Pending',
    //     smily: 'smilyPending.png'
    //   };
    //   renderParams.data.kyc = kycData;
    //   res.render('user/kycVerification', renderParams);
    //   // with mnemonicAddredss
    // } else {
    //   let kycData = {
    //     message: 'Your Kyc Documents Are Approved',
    //     smily: 'smilyVerified.png'
    //   };
    //   renderParams.data.kyc = kycData;
    //   res.render('user/kycVerification', renderParams);
    // }



  } catch (err) {
    //console.log("111111111111111111", err)
    handleError({
      res,
      data: err
    });
  }
};

exports.kycVerificationStep2 = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/main',
      title: 'kycVerification',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };

    let user = await getUserDetails(req.session.userDetails._id);

    let arr = [];
    arr.push(user)

    let userArr = []
    let userObj = {}

    arr.forEach(e => {      
      userObj = {

        flagsKycUploadIdProofIsKyc : e.flags.kycUploadIdProof.isKyc,
        flagsKycUploadAddressProofIsKyc : e.flags.kycUploadAddressProof.isKyc,
        flagsKycUploadWithSelfiePhotoIsKyc : e.flags.KycUploadWithSelfiePhoto.isKyc,
        kycDetailsChooseId : e.kycDetails.chooseId,
        kycDetailsChooseAddressId : e.kycDetails.chooseAddressId,
        kycDetailsKycIdProof : e.kycDetails.kycIdProof,
        kycDetailsKycAddressProof : e.kycDetails.KycAddressProof,
        kycDetailsKycPhotoId : e.kycDetails.KycPhotoId,
        firstName: e.firstName,
        lastName: e.lastName,
        profileImage: e.profileImage
      }
      userArr.push(userObj)
    });

    const data = {};
    data.userData = userArr[0];
    renderParams.data = data;
    res.render('user/kycVerificationStep2', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.kycVerificationStep3 = async (req, res) => {
  //console.log("step3333333")
  try {
    let renderParams = {
      layout: 'layouts/main',
      title: 'kycVerification',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    //console.log("kycccccccccccccccccc 3")

    const data = {};
    data.userData = req.session.userDetails;
    renderParams.data = data;
    res.render('user/kycVerificationStep3', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.kycVerification = async (req, res) => {
  //console.log("verifyyyyyyyyyy")

  try {
    let renderParams = {
      layout: 'layouts/main',
      title: 'kycVerification',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    //console.log("kycccccccccccccccccc verify")

    let data = {};
    let kycData = {
        message: 'Your KYC/AML documents are under review.',
        message2: 'Once verified, your account will be activated.',
        smily: 'smilyPending.png',
        flag:false
    }
    
    data.userData     = req.session.userDetails;
    renderParams.data = data;
    renderParams.data.kyc = kycData;

    res.render('user/kycVerification', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.myAccount = async (req, res) => {
  try {
   
    let renderParams = {
      layout: 'layouts/main',
      title: 'My Account',
      //   pageTilte: 'Dashboard',
      BASEURL: BASEURL
    };



   // let users = await getUserDetails(req.session.userDetails._id);
    let data= {};
    data.userData = req.session.userDetails

    //ternery in handlebar
    let enabled = data.userData.flags.kyc.status
    data.enabled=enabled;
    renderParams.data = data;   
    res.render('user/myAccount', renderParams);
  } catch (err) {
    console.log(err);
    handleError({
      res,
      data: err
    });
  }
};

exports.resetPassword = async (req, res) => {

  try {
    let renderParams = {
      layout: 'layouts/login'
     
    };
    var response = req.response;
    let token = req.query.token;
    await resetPasswordToken(token, res, response);
    console.log("getstatus ",response.getStatus)
    if (response.getStatus == 200) {          
      renderParams.data = response.Response.message;
      res.render('user/resetPassword', renderParams);
    } else {
      renderParams.data = response.Response.message;
      console.log("paramssssssssssss",renderParams)
      res.render('user/linkExpired',renderParams)
    }
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.kycVerificationDetails = async (req, res) => {
  try {
  
    var response = req.response;

    var userId = req.session.userDetails;
   
    await userDetails(userId, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};

exports.kyc = async (req, res) => {
  try {
    var response = req.response;

    let idFile = req.body.idProofFileType == 'name' ? req.body.idProofFile : req.files.idProofFile[0].filename
    console.log("1111111",idFile)
    let addressFile = req.body.addressProofFileType == 'name' ? req.body.addressProofFile : req.files.addressProofFile[0].filename
    console.log("22222222222",addressFile)
    let withSelfiePFile = req.body.withSelfiePhotoIdFileType == 'name' ? req.body.withSelfiePhotoIdFile : req.files.withSelfiePhotoIdFile[0].filename
  
  
  console.log('333333333333',withSelfiePFile)
  // let fff1 = req.files.withSelfiePhotoIdFile[0].filename ? req.files.withSelfiePhotoIdFile[0].filename : req.files.withSelfiePhotoIdFile.filename
    let kycDetails = {
      chooseId: req.body.idProof,
      kycIdProof:idFile,
      chooseAddressId: req.body.addressProof,
      KycAddressProof: addressFile,
      KycPhotoId: withSelfiePFile
    };
    // let kycDetails = {
    //   chooseId: req.body.idProof,
    //   kycIdProof: req.files.idProofFile[0].filename,
    //   chooseAddressId: req.body.addressProof,
    //   KycAddressProof: req.files.addressProofFile[0].filename,
    //   KycPhotoId: req.files.withSelfiePhotoIdFile[0].filename
    // };
    let userId = req.session.userDetails.email;
    await kycUserDetails(req,userId, kycDetails, response);
    if(response.Response.status == 200) {
      let users = await getUserDetails(req.session.userDetails._id);
      req.session.userDetails = users;}
      
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'KYC verification unsuccessful ';

    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};

exports.updateUserProfile = async (req, res) => {
  try {
  
    var response = req.response;
    await userDetailsUpdate(req, req.session.userDetails.email, response);
    if(response.Response.status == 200) {
      let users = await getUserDetails(req.session.userDetails._id);
      req.session.userDetails = users;
    }
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};

exports.changePassword = async (req, res) => {
  try {
    var response = req.response;

    await changePass(req, req.session.userDetails.email, response);
    if(response.Response.status == 200) {
      let users = await getUserDetails(req.session.userDetails._id);
      req.session.isLoggedIn = false
      req.session.mnemonicAddredss = false
      req.session.userDetails = users;}
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    res.setStatus = 422;
    res.setMessage = 'Data is not available';
    console.log(err);
    res.status(response.getStatus).send(response.getResponse);
  }
};
exports.kycAgreement = async (req, res) => {
  try {
    
    var response = req.response;
    await agreement(req.session.userDetails.email, response);
    res.status(response.getStatus).send(response.getResponse);
  } catch (err) {
    console.log('error', err);
  }
};
exports.tokenCheck = async (req, res) => {
  try {
    let response = req.response;
   
    let token = req.body.Token;
    let password = req.body.newPassword;

    let conPassword = req.body.conPassword;
    if (password == conPassword) {
      await forgotTokenCheck(token, conPassword, response);
      res.status(response.getStatus).send(response.getResponse);
    } else {
      response.setStatus = 422;
      response.setMessage = 'Your Password Field and Confirm password not same';
      response.setErrorStack = 'Your Password Field and Confirm password not same';
      // response.setData = details;
      res.status(response.getStatus).send(response.getResponse);

      return false;
    }
  } catch (err) {
    console.log('error', err);
  }
};

exports.logout = async (req, res) => {
  try {
    await logoutUser(req.session.userDetails._id)

    req.session.isLoggedIn = false
    req.session.mnemonicAddredss = false    
    res.redirect('login')
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.linkExpired = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/login'
      //   title: 'dashboard',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    res.render('user/linkExpired', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
}
exports.verified = async (req, res) => {
  try {
    let renderParams = {
      layout: 'layouts/login'
      //   title: 'dashboard',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    res.render('user/successfullyVerified', renderParams);
  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};
exports.changePhoneNumber=async(req,res)=>{
  try{
    var response = req.response;
    await changeNumber(req, response);
    res.status(response.getStatus).send(response.getResponse);
  }catch(err){console.log('error........',err)}
}
exports.otpCheckPhnChange=async(req,res)=>{
  try{
    var response = req.response;
    await changeNumberCheck(req, response);
    res.status(response.getStatus).send(response.getResponse);
    
  }catch(err){
    console.log('error........',err)
  }
};
exports.contactUsView=async(req,res)=>{
  try{
    var response = req.response;
    await contactUsDetailsView(req, response);
    res.status(response.getStatus).send(response.getResponse);
    
  }catch(err){
    console.log('error........',err)
  }
}

exports.mnemonic = async (req, res) => {
  try {
// let user = {}
    var secret = speakeasy.generateSecret();
  //  console.log("secret...........",secret)
   // console.log("secret.base32.........",secret.base32)
    let two_factor_temp_secret = secret.base32;

    console.log("user.two_factor_temp_secret........",two_factor_temp_secret)

    QRCode.toDataURL(secret.otpauth_url, function(err, data_url) {
  console.log(data_url);

  let renderParams = {
    layout: 'layouts/main'
  };
  let data = {}
  data.userData     = req.session.userDetails;
  data.pic = data_url
  // data.verified = verified
  renderParams.data = data;
  res.render('user/mnemonicAddress', renderParams);
});

  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};
exports.verification = async (req, res) => {
  try {

    var secret = speakeasy.generateSecret();
    console.log('---------',secret)
    // var url = speakeasy.otpauthURL({ secret: secret.ascii, label: 'Blue Morpho Tokens', algorithm: 'sha512' });
    var url = speakeasy.otpauthURL({
      secret: secret.base32,
      label: 'BlueMorphoTokens',
      encoding: "base32"
    });
    console.log('----------', url)
    let secretCode = secret.base32;
    QRCode.toDataURL(url, function(err, data_url) {
    console.log('--=-=-=',data_url);

  let renderParams = {
    layout: 'layouts/main'
  };
  let data = {}
  data.userData     = req.session.userDetails;
  data.pic = data_url
  data.secretCode = secretCode
  renderParams.data = data;

  // if(data.userData.secretQRCode=='' || data.userData.secretQRCode==null)
  // {
    res.render('user/verificationCode', renderParams);
  // }
  // else{
  //  res.redirect('dashboard1')
  // }

});

  } catch (err) {

    handleError({
      res,
      data: err
    });
  }
};
exports.addMnemonic = async (req, res) => {
  try {

    req.session.mnemonicAddredss = req.body.mnemonicAddredss

    if (req.session.mnemonicAddredss) {
      res.status(200).send(true);
    } else {
      res.status(400).send(false);
    }
    

  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.changePhoneNumberSignUp=async(req,res)=>{
  try{
    var response = req.response;
    console.log("****************************", req.body)
    await changeNumberSignUp(req, response);
    res.status(response.getStatus).send(response.getResponse);
  }catch(err){console.log('error........',err)}
}



exports.googleAuthenticator = async(req, res) => {
  try {

  let userToken = req.query.token;

  let user = await getUserDetails(req.session.userDetails._id);
  let base32secret = user.secretQRCode;

  let verified = speakeasy.totp.verify({ secret: base32secret,
                                       encoding: 'base32',
                                       token: userToken });
    
    if (verified) {
      let flag        = true
      let updateFlag  = await changeAuthenticatorFlag(req.session.userDetails._id);
      if(updateFlag)
      {
        res.status(200).send(true);
      }
      res.status(200).send(false);
    } else {
      res.status(400).send(false);
    }
    

  } catch (err) {
    handleError({
      res,
      data: err
    });
  }

}

exports.generateQRCode = async (req, res) => {
  try {
// let user = {}
    var secret = speakeasy.generateSecret();

   // console.log("secret.base32..........",secret.base32)

    let secretCode = secret.base32;

    QRCode.toDataURL(secret.otpauth_url, function(err, data_url) {
  console.log(data_url);


  let renderParams = {
    layout: 'layouts/login'
  };
  let data = {}
  data.userData     = req.session.userDetails;
  data.pic = data_url
  data.secretCode = secretCode
  // data.verified = verified
  renderParams.data = data;
  res.render('user/generateQRCode', renderParams);
});

  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
};

exports.sendSecretORCode = async (req, res) => {
  try {
    const secretCode  = req.body.secretCode;
    const userId      = req.session.userDetails._id
    const result      = await updateSecretQRCode(userId,secretCode)
    if (result) {
      res.status(200).send(true);
    } else {
      res.status(400).send(false);
    }
  } catch (err) {  
    handleError({
      res,
      data: err
    });
  }
};

exports.createUserWallet = async (req, res, next) => {
  try {
    console.log(">>>>>>>>>>>>>.")
      const wallet = await createWallet();
      
      const walletUpdate = updateUser(req.session.userDetails._id, wallet.walletAddress);

    

    //  await Promise.all([bmtKyc, fgmKyc, iaiKyc, oegKyc, walletUpdate])
    //    .then(data1 => {
          let renderParams = {
            layout: 'layouts/main'
          };
          let data = {}
          data.userData     = req.session.userDetails;
          data.mnemonic = wallet.mnemonic
          // data.verified = verified
          renderParams.data = data;
          res.render('user/userMnemonic', renderParams);

      //  })
      //   .catch(err => {
      //     console.log("err", err)
      //     handleError({ res, data: 'Something went wrong' });
      //   });

  } catch (err) {
    handleError({
      res,
      data: err
    });
  }
}

