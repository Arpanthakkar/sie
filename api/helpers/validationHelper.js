var path = require('path');
var nameRegex = /^[a-zA-Z ]{2,30}$/;
var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
var numberRegex = /^\d*$/;
var dateRegex= /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/;
var dateTimeRegex = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])$/;
var timeRegex = /(?:[01]\d|2[0123]):(?:[012345]\d):(?:[012345]\d)/;
var validationOutcome = (response, error, status = 422) => {
    response.setStatus = status;
    response.setErrorStack = error;
}

module.exports = {

    nameValidation: (value, response) => {
     
        if (value.length === 0) {
            validationOutcome(response, "Name Can not Empty");
        } else if (!nameRegex.test(value)) {
            validationOutcome(response, "Only Characters are Allowed in First and Last Name.");
        }
        return true;
    }, 

    emailValidation: (value, response) => {
        if (value.length === 0) {
        
            validationOutcome(response, "Please enter email.");
        } else if (!emailRegex.test(value)) {
        
            validationOutcome(response, "Email is not valid");
        }
        
        return true;
    },

    passwordValidation: (value, confirmvalue, response) => {
        if (value.length === 0) {
            validationOutcome(response, "Please enter password.");
        } else if (value != confirmvalue) { 
            validationOutcome(response, "Your password and confirmation password do not match.");

        } else {
            if (value.length < 8) {
                validationOutcome(response, "Password should be more then 8 character");
            }
        }
        return true;
    },

    contactNumberValidation: (value, response) => {
     
        if (value.length === 0) {
            validationOutcome(response, "Please enter phone number.");
        } else if (value.length > 15) {
            validationOutcome(response, "Please enter phone number less than 15 digits.");
        } else if (!numberRegex.test(value)) {
            validationOutcome(response, "Please enter digits in phone number.");
        }
        return true;
    },

    notEmptyValidation: (value, field, response) => {
        if (value.length === 0) {
            validationOutcome(response, field);
        }
        return true;
    },

    fileValidation: (value, response) => {

        if (value.length == 0) {
           
            validationOutcome(response, "Please Select File")
        } else {

        

            //extention = path.extname(value.originalname);
           let extention = value[0].path.split('.').pop();
          
          
         
           
            if (!(extention == '.jpg' || extention == '.JPG' || extention == '.pdf' || extention == '.PDF' || extention == '.jpeg' || extention == '.JPEG')) {
                validationOutcome(response, "Only jpg , jpeg and pdf file accepted");
            }
        }
        return true;
    },

    usernameValidation: (value, response) => {

    },

    numberValidation: (value, msg, response) => {
        if (!numberRegex.test(value)) {
            validationOutcome(response, "only allow number ");
        }
        return true;
    },

    dateValidation: (value, msg, response) => {
        if (!dateRegex.test(value)) {
            validationOutcome(response, msg);
        }
        return true;
    },
    dateTimeValidation: (value, msg, response) => {
        if (!dateTimeRegex.test(value)) {
            validationOutcome(response, msg);
        }
        return true;
    },
    timeValidation: (value, msg, response) => {
        if (!timeRegex.test(value)) {
            validationOutcome(response, msg);
        }
        return true;
    },
    arrayLengthValidation: (value, msg, response) => {
        if (!value.length != 0) {
            validationOutcome(response, msg);
        }
    },
    arrayValidation: (value, length, msg, expectedArray, response) => {
        WorkingDay = expectedArray;
      
        if (value.length < length) {
            validationOutcome(response, msg);
        }
        if (WorkingDay != null) {
            value.forEach(element => {

                if (!WorkingDay.includes(element)) {
                    validationOutcome(response, element + "is not valid" + msg);
                }
            });
        }
    },
    checkErr: (response) => {
        if (response.getStatus === 200) {
            return true;
        } else {
            response.setMessage = "validation Error";
            return false;
        }
    },
    undefinedError: (status, errormsg, response) => {
        response.setStatus = status;
        response.setErrorStack = errormsg;
    },
    notEmptyArrayValidation: (arrayLength, arrayValue, errMessage, response) => {
        if (arrayValue.length != arrayLength) {
            validationOutcome(response, errMessage + " only accept 2 indexes");
        }
    },
    isInValidation: (value, msg, expectedArray, response) => {
        if (!expectedArray.includes(value)) {
            validationOutcome(response, msg);
        }
    }

}