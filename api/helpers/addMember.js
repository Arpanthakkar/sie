const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');


var addMememberAdminSide = {
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/emailTemplate',
        defaultLayout: 'addNewUser',
        partialsDir: 'views/emailTemplate'
    },
    viewPath: 'views/emailTemplate/',
    extName: '.hbs'
};
var mailer = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    secure: false,
    port: 587,
    auth: {
        user: 'bluemorphotokens@gmail.com',
        pass: 'Solulab@123'
    }
});
exports.addMember= async function (receiverEmail = "none", subject = "none", data = any) {
    try {
        mailer.use('compile', hbs(addMememberAdminSide));
        mailer.sendMail({
            from: 'no-reply@sie.com',
            to: receiverEmail,
            subject: subject,
            template: 'addNewUser',
            context: data
        }, function (error, response) {
        
            console.log('errors', error);
            mailer.close();
        });

    } catch (err) {
        console.log("Error In Main", err);
    }
    return true;
}