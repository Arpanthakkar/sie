const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');


var verifiedKycTemp  ={
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/emailTemplate',
        defaultLayout: 'verifiedKyc',
        partialsDir: 'views/emailTemplate'
    },
    viewPath: 'views/emailTemplate/',
    extName: '.hbs'
};
var mailer = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    secure: false,
    port: 587,
    auth: {
        user: 'bluemorphotokens@gmail.com',
        pass: 'Solulab@123'
    }
});
exports.VerifiedKycMail= async function (receiverEmail = "none", subject = "none", data = any) {
    try {
        console.log("dataaaaaaaaaaaaaaaaaaaaaaa",data)
        mailer.use('compile', hbs(verifiedKycTemp));
        mailer.sendMail({
            from: 'no-reply@sie.com',
            to: receiverEmail,
            subject: subject,
            template: 'verifiedKyc',
            context: data

        }, function (error, response) {
        
            console.log('errors', error);
            mailer.close();
        });

    } catch (err) {
        console.log("Error In Main", err);
    }
    return true;
}