const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');



var rejectKyc  ={
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/emailTemplate',
        defaultLayout: 'rejectKyc',
        partialsDir: 'views/emailTemplate'
    },
    viewPath: 'views/emailTemplate/',
    extName: '.hbs'
};

var mailer = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    secure: false,
    port: 587,
    auth: {
        user: 'bluemorphotokens@gmail.com',
        pass: 'Solulab@123'
    }
});
module.exports = {
  
 
    RejectKycMail: async function (receiverEmail = "none", subject = "none", data = any) {
        try {
            mailer.use('compile', hbs(rejectKyc));
            mailer.sendMail({
                from: 'no-reply@sie.com',
                to: receiverEmail,
                subject: subject,
                template: 'rejectKyc',
                context: data
            }, function (error, response) {
            
                console.log('errors', error);
                mailer.close();
            });

        } catch (err) {
            console.log("Error In Main", err);
        }
        return true;
    },

   
    otpGenerate: async function () {
        var arr = '0123456789';
        var length = 4;
        var ans = '';
        for (var i = length; i > 0; i--) {
            ans += arr[Math.floor(Math.random() * arr.length)];
        }
        return ans;
    },
    forgotPasswordNumberGenerate: async function () {
        
        var arr = '1A2B3C4D5E6F7G8H9I0J1K2L3M4N5O6P7Q8R9S0T1U2V3W4X5Y6Z';
        var length = 8;
        var ans = '';
        for (var i = length; i > 0; i--) {
            ans += arr[Math.floor(Math.random() * arr.length)];
        }
        return ans;
    },
    Sms: async function (otp, no) {
    //    const accountSid = 'AC804379b665f35b25bb1285734aac62c0';
        const accountSid = 'ACcbcbfa4a331b4be66959b5553b4eade9';

//        const authToken = '01da85f025dad7ea14e92d1e62c827f2';
        const authToken = '4763524ef8689befa0868e0e03569466';
        const client = require('twilio')(accountSid, authToken);
        client.messages
        //    .create({ body: 'Your Verification Code is : ' + otp, from: '+15203326239', to: '+'+no })
            .create({ body: 'Your Blue Morpho verification code is : ' + otp, from: '+18563404457', to: '+'+no })
            .then(message => console.log(message.sid)).catch(err => console.log(err));

    }
}