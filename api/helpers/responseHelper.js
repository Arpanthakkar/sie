
class ResponseHelper {

    constructor() {
        this.Response = {
            "status": 200,
            "message": "success",
            "error_stack": [],
            "data": {}
        };
    }
    set setResponse(Response) {
        this.Response.status = Response.status;
        this.Response.message = Response.message;
        this.Response.data = Response.data;
        this.Response.error_stack = Response.error_stack;
    }
    get getResponse() {
        return this.Response;
    }
    set setStatus(status) {
        this.Response.status = status;
    }
    get getStatus() {
        return this.Response.status;
    }
    set setMessage(message) {
        this.Response.message = message;
    }
    get getMessage() {
        return this.Response.message;
    }
    set setErrorStack(errorStack) {
        this.Response.error_stack.push(errorStack);
    }
    get getErrorStack() {
        return this.Response.error_stack;
    }
    set setData(data) {
        for (var i in data) {
            this.Response.data[i] = data[i];
        }
    }
    get getData() {
        return this.Response.data;
    }
}

module.exports = ResponseHelper;