const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
var forgotPass ={
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/emailTemplate',
        defaultLayout: 'forgotPassword',
        partialsDir: 'views/emailTemplate'
    },
    viewPath: 'views/emailTemplate/',
    extName: '.hbs'
};
var mailer = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    secure: false,
    port: 587,
    auth: {
        user: 'bluemorphotokens@gmail.com',
        pass: 'Solulab@123'
    }
});
exports.ForgotMail= async function (receiverEmail = "none", subject = "none", data = any) {
    try {
        
        mailer.use('compile', hbs(forgotPass));
        mailer.sendMail({
            from: 'no-reply@sie.com',
            to: receiverEmail,
            subject: subject,
            template: 'forgotPassword',
            context: {
                firstName: data.firstName,
                lastName: data.lastName,
                token: "" + data.emailToken,
                imgURL:data.imgURL,
                email:data.email
            }
        }, function (error, response) {
        
            console.log('errors', error);
            mailer.close();
        });

    } catch (err) {
        console.log("Error In Main", err);
    }
    return true;
};