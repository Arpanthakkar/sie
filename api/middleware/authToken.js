const jwt = require('jsonwebtoken');
const config = require('../config.js');
//const {checkTokenInDB,removeTokenInDB,} = require('../models/commonModel');
const responseHelper = require('../helpers/responseHelper');
const { checkTokenInDB } = require('../models/commonModel');
const { getUserDetails } = require('../services/providerService');

module.exports = {
  validateToken: async (req, res, next) => {
    var response = req.response;
    const authorizationHeader = req.headers.authorization;
    if (authorizationHeader) {
      var token = req.headers.authorization.split(' ')[1];
      await new Promise(async function(resolve, reject) {
        let result = await checkToken(token);

        if (result == false) {
          reject('Token Expired Please Login');
        } else {
          if (['/login',  '/register', '/forgotPassword', '/resetPassword'].includes(req.originalUrl)) {
            reject('Token Active');
          } else {
            req.decode = {
              token: token,
              tokenEmail: result.email, // for getting email id from jsontoken
              tokenId: result.id,
              tokenType: result.userRole
            };

            resolve();
          }
        }
      })
        .then(() => {
          next();
        })
        .catch(err => {
          response.setStatus = 422;
          response.setMessage = err;
          res.status(response.getStatus).send(response.getResponse);
        });
    } else {
      if (
        ['/login', '/register', '/forgotPassword', '/setnewPassword', '/resetPassword'].includes(req.originalUrl) ||
        req.originalUrl.includes('/setnewPassword')
      ) {
        next();
      } else {
        response.setStatus = 422;
        response.setMessage = 'Please send the Authentication Token With Request';
        res.status(response.getStatus).send(response.getResponse);
      }
    }
  },

  validateSession: async (req, res, next) => {
    

    let url = req.originalUrl.split('/');
    if(url[2]){
      let resetPasswordStr = url[2].startsWith('reset-password')
      url[2] = resetPasswordStr ? 'reset-password' : url[2]
    }

    if (req.originalUrl == '/user/generateQRCode'){
      url[2] = 'generateQRCode'
    }


    if(req.originalUrl == '/user/login' || req.originalUrl == '/user/sendSecretORCode'){
      next()
   } else if (
      (req.session.isLoggedIn && (req.session.loggedInType == 'user')) ||
      ['/user/login', '/favicon.ico', '/user/signUp','/user/forgotPassword', '/user/setnewPassword', '/user/resetPassword', '/user/transfer', '/user/getEstimate', '/user/changePhoneNumber', '/user/changePhoneNumberSignUp'].includes(req.originalUrl)
    ) {
      next();
    } else if (['reset-password', 'securityVerified', 'linkExpired', 'googleAuthenticator', 'generateQRCode'].includes(url[2])) {
      next();
    } else {
      console.log("1111")
      res.redirect('/user/login');
    }
    // next();
  },

  validateAdminSession: async (req, res, next) => {
    if (req.session.isLoggedIn && req.session.loggedInType == 'admin') {
      next();
    } else {
      res.redirect('/admin/login');
    }
  },

  checkMnemonic: async (req, res, next) => {
    console.log('.........checkMnemonic......', req.originalUrl);

    let user = await getUserDetails(req.session.userDetails._id);


    let arr = [];
    arr.push(user);

    let userArr = [];
    let userObj = {};

    arr.forEach(e => {
      userObj = {
        flagsKycUploadIdProofIsKyc: e.flags.kycUploadIdProof.isKyc,
        flagsKycUploadAddressProofIsKyc: e.flags.kycUploadAddressProof.isKyc,
        flagsKycUploadWithSelfiePhotoIsKyc: e.flags.KycUploadWithSelfiePhoto.isKyc,
        firstName: e.firstName,
        lastName: e.lastName,
        profileImage: e.profileImage,
        kycStatus: e.flags.kyc.isKyc,
        secretQRCode: e.secretQRCode,
        isGoogleAuthorised: e.isGoogleAuthorised
      };
      userArr.push(userObj);
    });

    console.log('userArr[0]', userArr[0]);

    let renderParams = {
      layout: 'layouts/main',
    };

    let kyc = userArr[0];
    renderParams.data = {};
    renderParams.data.userData = userArr[0];
    console.log("oooooooooooooooooooooooooooooooo",user)

    if (kyc.secretQRCode == null){
      res.redirect('/user/verification');
    } else if (kyc.secretQRCode && user.isUserLoggedIn==true ){
      res.redirect('/user/mnemonic');
   } else if (
      kyc.flagsKycUploadAddressProofIsKyc == 'pending' &&
      kyc.flagsKycUploadIdProofIsKyc == 'pending' &&
      kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'pending'
    ) {
      //console.log("nodeeeeeeeeeeeeeeeeeeee")

      let kycData = {
        message: 'Your KYC/AML documents are under review.',
        message2: 'Once verified, your account will be activated.',
        smily: 'smilyPending.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    } else if (
      kyc.flagsKycUploadAddressProofIsKyc == 'notVerified' &&
      kyc.flagsKycUploadIdProofIsKyc == 'notVerified' &&
      kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'notVerified' &&
      !(['/user/kyc-verification-step-1', '/user/kyc-verification-step-2', '/user/kyc-verification-step-3',].includes(req.originalUrl))
      // req.originalUrl != '/user/kyc-verification-step-1'
      ) {
        //pls upload kyc.
      let kycData = {
        message: 'Click here to Continue',
        message2:'',
        smily: 'smilyPending.png',
        flag:true
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    } else if (
      (kyc.flagsKycUploadAddressProofIsKyc == 'reject' ||
      kyc.flagsKycUploadIdProofIsKyc == 'reject' ||
      kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'reject') &&
      !(['/user/kyc-verification-step-1', '/user/kyc-verification-step-2', '/user/kyc-verification-step-3',].includes(req.originalUrl))

    ) {
      let kycData = {
        message: 'Some or all of your KYC/AML documents have not been accepted. Please resubmit the rejected ones.',
        smily: 'smilyReject.png',
        rejectFlag :true,
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    } else if (kyc.kycStatus == 'reject') {
      let kycData = {
        message: 'Your Kyc Request is Rejected',
        message2:'',
        smily: 'smilyReject.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    } else if (kyc.kycStatus == 'pending') {
      let kycData = {
        message: 'Your KYC/AML request is pending',
        message2:'',
        smily: 'smilyPending.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
      // with mnemonicAddredss
    // } else {
    //   next();
    // }
    // without mnemonicAddredss
    } else if (kyc.isGoogleAuthorised) {
      next();
    } else {
      console.log("no")
      res.redirect('/user/mnemonic');
    }
  },

  checkMnemonicKyc: async (req, res, next) => {
    let renderParams = {
      layout: 'layouts/main',
      title: 'kycVerification',
      //   pageTilte: 'Dashboard',
      //  BASE_URL: CONST.BASE_URL
    };
    let user = await getUserDetails(req.session.userDetails._id);

    let arr = [];
    arr.push(user);

    let userArr = [];
    let userObj = {};

    arr.forEach(e => {
      userObj = {
        flagsKycUploadIdProofIsKyc: e.flags.kycUploadIdProof.isKyc,
        flagsKycUploadAddressProofIsKyc: e.flags.kycUploadAddressProof.isKyc,
        flagsKycUploadWithSelfiePhotoIsKyc: e.flags.KycUploadWithSelfiePhoto.isKyc,
        firstName: e.firstName,
        lastName: e.lastName,
        profileImage: e.profileImage,
        kycStatus: e.flags.kyc.isKyc,
        secretQRCode: e.secretQRCode,
        isGoogleAuthorised: e.isGoogleAuthorised
      };
      userArr.push(userObj);
    });


    const data = {};
    data.userData = req.session.userDetails;
    renderParams.data = data;

    let kyc = userArr[0]

    console.log("kyc.....",kyc)

    if (kyc.secretQRCode == null){
      res.redirect('/user/verification');
    } else if (kyc.secretQRCode && kyc.isGoogleAuthorised == false){
      res.redirect('/user/mnemonic');
    } else if (      
      kyc.flagsKycUploadAddressProofIsKyc == 'pending' &&
      kyc.flagsKycUploadIdProofIsKyc == 'pending' &&
      kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'pending'
    ) {
      //true
      //console.log("sesiiionnnnnnnnnnn ",req.session.userDetails)
      let kycData = {
        message: 'Your Kyc Documents Are Under Review',
        message2: 'Once verified, your account will be activated.',
        smily: 'smilyPending.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    
    } else if (
      kyc.flagsKycUploadAddressProofIsKyc == 'notVerified' &&
      kyc.flagsKycUploadIdProofIsKyc == 'notVerified' &&
      kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'notVerified'
    ) {
      console.log("111111111")
      next()

    } else if (
      kyc.flagsKycUploadAddressProofIsKyc == 'reject' ||
      kyc.flagsKycUploadIdProofIsKyc == 'reject' ||
      kyc.flagsKycUploadWithSelfiePhotoIsKyc == 'reject'
    ) {
      console.log("22222222")
      next()
    } else if (kyc.kycStatus == 'reject') {
      let kycData = {
        message: 'Your Kyc Request Is Rejected',
        smily: 'smilyReject.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    } else if (kyc.kycStatus == 'pending') {
      let kycData = {
        message: 'Your KYC/AML request is pending',
        smily: 'smilyPending.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
      // with mnemonicAddredss
    } else {
      let kycData = {
        message: "Your KYC/AML documents are approved.",
        message2:'Your Account is verified and fully active.',
        smily: 'smilyVerified.png',
        flag:false
      };
      renderParams.data.kyc = kycData;
      res.render('user/kycVerification', renderParams);
    }

  },

  setJWTToken: async data => {
    var tk = {
      token: '',
      email: data.email
    };
    const { secretForJWT } = config;
    return new Promise(function(resolve, reject) {
      //{ expiresIn: '1d' }

      jwt.sign({ id: data._id, email: data.email, userRole: data.userRole }, secretForJWT, function(err, token) {
        if (err) console.log(err);
        tk.token = token;
        if (tk.token != undefined) {
          resolve();
        } else {
          reject();
        }
      });
    })
      .then(() => {
        return tk;
      })
      .catch(() => {
        return false;
      });
  }
};
async function checkToken(token) {
  try {
    const { secretForJWT } = config;
    var result = jwt.verify(token, secretForJWT);

    let checkTokenInDBFlag = await checkTokenInDB(result.email, token);

    if (checkTokenInDBFlag) {
      return result;
    } else {
      return false;
    }
  } catch (err) {
    console.log(err);
    // await removeTokenInDB(token);
    return new Error('Token Expired Please Login', err);
  }
}
