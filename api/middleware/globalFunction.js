var { nameValidation,
    fnameValidation,
    lnameValidation,
    emailValidation,
    passwordValidation,
    contactNumberValidation,
    notEmptyValidation,
    checkErr,
    fileValidation,
    undefinedError,
    arrayLengthValidation,
    dateTimeValidation,
    numberValidation,
    arrayWorkingDays, dateValidation,
    notEmptyArrayValidation
} = require('../helpers/validationHelper');
var bcrypt = require('bcryptjs');
const {
    checkToken,

} = require('../middleware/authToken');
const {forgotPasswordNumberGenerate} = require('../helpers/commonFunction');
const BASEURL = require('../config');
exports.registrationValidation = async (req, res, next) => {
    let response = req.response;
    try {
        req.body.fname === undefined ? undefinedError(422, " first name Parameter Required", response) : notEmptyValidation(req.body.fname,"Please enter first name.", response);
        req.body.lname === undefined ? undefinedError(422, " last name Parameter Required", response) : notEmptyValidation(req.body.lname,"Please enter last name." ,response);
        req.body.email === undefined ? undefinedError(422, "  Parameter Required", response) : emailValidation(req.body.email, response);
        req.body.phoneNumber === undefined ? undefinedError(422, " phone number Parameter Required", response) : contactNumberValidation(req.body.phoneNumber, response);
        //req.body.countryCode === undefined ? undefinedError(422, " Country code Parameter Required", response) : notEmptyValidation(req.body.city, "Please select country code.", response);
        req.body.address === undefined ? undefinedError(422, " address Parameter Required", response) : notEmptyValidation(req.body.address.trim(), "Please enter address.", response);
        req.body.country === undefined ? undefinedError(422, " country Parameter Required", response) : notEmptyValidation(req.body.country, "Please select country.",response);
        req.body.city === undefined ? undefinedError(422, " city Parameter Required", response) : notEmptyValidation(req.body.city, "Please select city.", response);
        req.body.dob === undefined ? undefinedError(422, " Date Of Birth Parameter Required", response) : dateValidation(req.body.dob, "Please select date of birth.", response)
        req.body.password === undefined ? undefinedError(422, " password Parameter Required", response) : passwordValidation(req.body.password, req.body.conPassword, response);

        if (checkErr(response) != true) {
            res.status(response.getStatus).send(response.getResponse);
            return;
        } else {
            next();
        }
    } catch (err) {
        console.log('err......', err)
        response.setStatus = 422;
        response.setMessage = "Registration Validation Failed";
        res.status(response.getStatus).send(response.getResponse);
        return;
    }
};
exports.registerDataCreate = async (req, res, next) => {
    
    let oldDateObj = new Date();
    let newDateObj = new Date();

    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(req.body.conPassword, salt)

    let registerObj = {
        firstName: req.body.fname,
        lastName: req.body.lname,
        country: req.body.country,
        email: req.body.email,
        password: hash,
        address: req.body.address.trim(),
        dob: req.body.dob,
        city: req.body.city,
        phoneNumber: Number(req.body.phoneNumber),
        countryCode: req.body.countryCode,
        flags: {
            block_user: false,
            isVerifyMobile: true,
            isVerifyEmail: false,
            
            isKyc: false
        },
        addedBy: 'user',
        userRole: 'user',
        otpExpire: newDateObj.setTime(oldDateObj.getTime() + (5 * 60 * 1000)),
        zipCode:req.body.zipCode,
        profileImage:'logo-modal.png',
    }
    req.body.registerData = registerObj;
    next();
};
exports.loginValidation = async (req, res, next) => {
    var response = req.response;
    try {

        req.body.email === undefined ? undefinedError(422, " email Parameter Required", response) : emailValidation(req.body.email, response);
        req.body.password === undefined ? undefinedError(422, " password Parameter Required", response) : notEmptyValidation(req.body.password, "Please Enter Password", response);

        if (checkErr(response) != true) {
            res.status(response.getStatus).send(response.getResponse);
            return;
        } else {
            next();
        }
    } catch (err) {
        console.log(err)
        response.setStatus = 422;
        response.setMessage = "Login  Validation Failed";
        res.status(response.getStatus).send(response.getResponse);
        return;
    }
};
exports.loginDataCreate = async (req, res, next) => {


    var loginObj = {

        email: req.body.email,
        password: req.body.password,

    }
    req.body.loginData = loginObj;

    next();

};
exports.forgotValidation = async (req, res, next) => {
        var response = req.response;
        try {

            req.body.email === undefined ? undefinedError(422, " email Parameter Required", response) : emailValidation(req.body.email, response);
            if (checkErr(response) != true) {
                res.status(response.getStatus).send(response.getResponse);
                return;
            } else {
                next();
            }
        } catch (err) {

            response.setStatus = 422;
            response.setMessage = "Forgot Password Validation Failed";
            res.status(response.getStatus).send(response.getResponse);
            return;
        }
};
exports.setPasswordValidation = async (req, res, next) => {
        var response = req.response;
        try {
            req.body.password === undefined ? undefinedError(422, " password Parameter Required", response) : passwordValidation(req.body.password, req.body.conPassword, response);
            if (checkErr(response) != true) {
                res.status(response.getStatus).send(response.getResponse);
                return;
            } else {
                next();
            }
        } catch (err) {
            console.log(err)
            response.setStatus = 422;
            response.setMessage = "Set Passsword Validation Failed";
            res.status(response.getStatus).send(response.getResponse);
            return;
        }
};

exports.validateKyc = async (req, res, next) => {
        let response = req.response;
        try {
            console.log(req.files);
            req.body.idproof === undefined ? undefinedError(422, " Please Choose ID Proof ", response) : notEmptyValidation(req.body.idproof, response);
            req.body.addressProof === undefined ? undefinedError(422, " Please Choose Address Proof ", response) : notEmptyValidation(req.body.addressProof, response);
            req.files.withSelfiePhotoIdFile === undefined ? undefinedError(422, " Please Upload Selfie with Holding Document ", response) : fileValidation(req.files.withSelfiePhotoIdFile, response);
            req.files.idProofFile === undefined ? undefinedError(422, " Please Upload Id Proof Document ", response) : fileValidation(req.files.idProofFile, response);
            req.files.addressProofFile === undefined ? undefinedError(422, " Please Upload Address Proof Document ", response) : fileValidation(req.files.addressProofFile, response);
            if (checkErr(response) != true) {
                res.status(response.getStatus).send(response.getResponse);
                return;
            } else {
                next();
            }
        } catch (err) {
            console.log(err)
            response.setStatus = 422;
            response.setMessage = "File Validation Failed";
            res.status(response.getStatus).send(response.getResponse);
            return;
        }
};
exports.validateChangePassword=async (req, res, next) => {

    let response = req.response;
    try {
        
        req.body.oldPassword === undefined ? undefinedError(422, " Please Enter Old Password ", response) : notEmptyValidation(req.body.oldPassword, 'Please enter old password ',response);
        req.body.password === undefined ? undefinedError(422, " password Parameter Required", response) : passwordValidation(req.body.password, req.body.conPassword, response);
      
        if (checkErr(response) != true) {
            res.status(response.getStatus).send(response.getResponse);
            return;
        } else {
            next();
        }
    } catch (err) {
        console.log(err)
        response.setStatus = 422;
        response.setMessage = "Change Validation Failed";
        res.status(response.getStatus).send(response.getResponse);
        return;
    }
};
exports.addUserValidation= async(req,res,next)=>{
    let response = req.response;
    try {
        req.body.fname === undefined ? undefinedError(422, " first name Parameter Required", response) : notEmptyValidation(req.body.fname,"Please enter first name.", response);
        req.body.lname === undefined ? undefinedError(422, " last name Parameter Required", response) : notEmptyValidation(req.body.lname,"Please enter last name." ,response);
        req.body.email === undefined ? undefinedError(422, "  Parameter Required", response) : emailValidation(req.body.email, response);
        req.body.phoneNumber === undefined ? undefinedError(422, " phone number Parameter Required", response) : contactNumberValidation(req.body.phoneNumber, response);
     //   req.body.countryCode === undefined ? undefinedError(422, " Country code Parameter Required", response) : notEmptyValidation(req.body.city, "Please select country code.", response);
        req.body.address === undefined ? undefinedError(422, " address Parameter Required", response) : notEmptyValidation(req.body.address.trim(), "Please enter address.", response);
        req.body.country === undefined ? undefinedError(422, " country Parameter Required", response) :notEmptyValidation(req.body.country, "Please select country.",response);
        req.body.city === undefined ? undefinedError(422, " city Parameter Required", response) : notEmptyValidation(req.body.city, "Please select city.", response);
        req.body.dob === undefined ? undefinedError(422, " Date Of Birth Parameter Required", response) : dateValidation(req.body.dob, "Please select date of birth.", response)
        if (checkErr(response) != true) {
            res.status(response.getStatus).send(response.getResponse);
            return;
        } else {
            next();
        }
    } catch (err) {
        console.log('err......', err)
        response.setStatus = 422;
        response.setMessage = "Registration Validation Failed";
        res.status(response.getStatus).send(response.getResponse);
        return;
    }
}
exports.addUserAdminSide = async (req, res, next) => {
    
 
    // let salt = await bcrypt.genSalt(10);
    // let hash = await bcrypt.hash(req.body.conPassword, salt)
    const pass = await forgotPasswordNumberGenerate();
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(pass, salt)

    let registerObj = {
        firstName: req.body.fname,
        lastName: req.body.lname,
        country: req.body.country,
        email: req.body.email,
        password: hash,
        address: req.body.address.trim(),
        dob: req.body.dob,
        city: req.body.city,
        phoneNumber: Number(req.body.phoneNumber),
        countryCode: req.body.countryCode,
        flags: {
            block_user: false,
            isVerifyMobile: true,
            isVerifyEmail: true,
            kyc:{
                status:true,
                isKyc:'verified',
            },
            kycUploadAddressProof:{
                status:true,
                isKyc:'verified',
            },
            KycUploadWithSelfiePhoto:{
                status:true,
                isKyc:'verified',
            },
            kycUploadIdProof:{
                status:true,
                isKyc:'verified',
            },
        },
        addedBy: 'admin',
        userRole: 'user',
        otpExpire: 'null',
        zipCode:req.body.zipCode,
        profileImage:'logo-modal.png',
        tempField:pass,
    }
    req.body.registerData = registerObj;
    next();
};