$(document).ready(function () {
    $.validator.addMethod('passwordRule', function (value) {
        return /^^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\d)(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/.test(value);
    }, 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number');

    var formChanged = false;
    //Update profile.
    $('form[id="myaccount_form"]').validate({
        rules: {
            firstName: {
                required: true,
            },
            lastName: {
                required: true,
            }
        },
        submitHandler: function (form) {
            formChanged = true;
            $("#error").html('');
            var baseurl = $("input[name=baseurl]").val();
            var form = new FormData();
            form.append("firstName", $("input[name=firstName]").val().trim());
            form.append("lastName", $("input[name=lastName]").val().trim());
            form.append("profilePicture", $('#profile_pic').get(0).files[0]);
            $.ajax({
                type: "POST",
                url: "/admin/userProfile",
                data: form, 
                processData: false,
                contentType: false,
                async: true,
                crossDomain: true,
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    $("#content").html('');
                    $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Profile Updated Successfully!!</h2>")
                    $("#cong-modal").modal("show");
                    $("#loader").hide();
                },
                error: function (result, status, err) {
                    $("#content").html('');
                    $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Profile Not Updated!!</h2>")
                    $("#cong-modal").modal("show");
                    $("#loader").hide();
                }
            });
        }
    });
    $('#myaccount_form').on('keyup change paste', 'input, select, textarea', function(){
        formChanged = true;
    });
    $("#cong-modal").on('hide.bs.modal', function () {
        formChanged = false;
        location.reload(true);
    });
    window.onbeforeunload = function () {
        if (formChanged) {
            return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        }
    };

});
