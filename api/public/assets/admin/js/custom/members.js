$(document).ready(function () {

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
      }, "Please enter only characters."); 
    $('.input-group.date').datepicker({ format: "dd/mm/yyyy" ,endDate: "today"});

    $.validator.addMethod("zipcode", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9-]*$/i.test(value);
    }, "No special characters allowed."); 
    
    //Populate Country dropdowns.
    let countryDropdown = $('#country');
    countryDropdown.empty();
    countryDropdown.append('<option selected="true" disabled>Select Country</option>');
    countryDropdown.prop('selectedIndex', 0);

    let countryCodeDropdown = $('#country_code');
    countryCodeDropdown.empty();
    const url = 'https://restcountries.eu/rest/v2/all';

    // Populate dropdown with list of provinces
    $.getJSON(url, function (data) {
        $.each(data, function (key, entry) {
            if (entry.callingCodes != '') {
                countryDropdown.append($('<option></option>').attr('value', entry.name).text(entry.name));
                countryCodeDropdown.append($('<option></option>').attr('value', entry.callingCodes).text("+" + entry.callingCodes + "   (" + entry.alpha2Code + ")"));
            }
        })
    });

})
jQuery.validator.setDefaults({
    errorPlacement: function(error, element) {
        console.log(error)
        if(element[0].name=="dob")
        {;
            $('.date').after(error);
        }
        else
        {
            $(element).after(error);

        }
    }
});
$('form[id="addMemberForm"]').validate({
    rules: {
        first_name: {
            required: true,
            lettersonly:true,
        },
        last_name: {
            required: true,
            lettersonly:true
        },
        phone: {
            digits: true,
            required: true,
            maxlength :15,
            minlength:10
        },
        country_code: {
            required: true,
        },
        dob: {
            required: true,
        },
        email: {
            required: true,
            email: true,
        },
        address1: {
            required: true
        },
        country: {
            required: true
        },
        city: {
            required: true,
            lettersonly:true
        },
        zipCode: {
           // digits: true
           zipcode:true
        }
    },

    submitHandler: function (form) {
        $("#error").html('');
        var form = $(this);
        var url = '/admin/addUser';
        var address = $("input[name=address1]").val() + " " + $("input[name=address2]").val();
        $.ajax({
            type: "POST",
            url: url,
            data: {
                "fname": $("input[name=first_name]").val().trim(),
                "lname": $("input[name=last_name]").val().trim(),
                "country": $("#country").val(),
                "email": $("input[name=email]").val().toLowerCase(),
                "address": address,
                "city": $("#city").val(),
                "countryCode": $("#country_code").val(),
                "phoneNumber": $("input[name=phone]").val(),
                "dob": $("input[name=dob]").val(),
                "zipCode": $("#zipCode").val()
            },
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                //open modal.                    
                $('#cong-modal').modal('show');
                $("#loader").hide();
            },
            error: function (result) {
                console.log(result)
                $("#error").html(result.responseJSON.error_stack[0]);
                $("#loader").hide();
            }
        });
    }
});

$("#cong-modal").on('hide.bs.modal', function () {
    //   location.reload(true);
    $(location).attr('href', '/admin/members1')
});
