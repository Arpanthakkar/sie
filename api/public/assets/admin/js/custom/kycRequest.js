
$(document).ready(function () {

  var table= $('.dataTables-example').DataTable({
    dom: 'Blftip',
    autoWidth: false,
    // scrollX :true,
    language: {
        searchPlaceholder: "Search"
    },
    buttons: [
        {
            extend: 'csv',
            text: 'Download CSV',
            title: 'KYC Requests'
        }
    ],

})
    $('form[id="form_token"]').validate({
        rules: {           
            amount: {
                required: true
            },
            token: {
                required: true
            }
        },
        submitHandler: function (form) {
               //Transfer Token.
        $.ajax({
            type: 'POST',
            url: '/admin/multi',
            data: $('#form_token').serialize(), // serializes the form's elements.
            beforeSend: function() {
              $('#loader1').show()
            },
            success: function(data) {
              $('#sent-modal').modal('hide');
              $('#content').html('');
              $('#content').append(
                "<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Tokens Sent Successfully</h2>"
              );
              $('#cong-modal').modal('show');
              $('#loader1').hide();
            },
            error: function(result, status, err) {
              $('#sent-modal').modal('hide');
              $('#content').html('');
              $('#content').append(
                "<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Token(s) not sent, the number of token(s) sent exceeds the available balance</h2>"
              );
              $('#cong-modal').modal('show');
              $('#loader1').hide();
            }
          });
        }
    });
    $("#transferToken").click(function () {
      table.page.len( -1 ).draw();
      
       // var membernames = [];
        var k = 0;
        $.each($("input[name='memberCheckbox']:checked"), function () {
           
           // membernames.push($(this).data('member'));
            k++;
        });
        if(k==0)
        {
          $("#error").text("Please select Member(s).").show();
          $("#transferTokenBtn").prop("disabled",true)
          $("#members").html("No members selected.");
        }
        else
        {
          $("#error").text("").hide();
          $("#transferTokenBtn").prop("disabled",false)
          $("#members").html(membernames.join())
        }
        console.log(k)       
           
        $("#sent-modal").modal('show');
       
    });
 
      //Display Gas Value in Transfer Modal.
  $('#amount').focusout(function() {
    console.log()

    //call api
    $.ajax({
      type: 'POST',
      url: '../user/getEstimate',
      data: {
        address: wallets[0],
        amount: $('#amount').val()
      },
      beforeSend: function() {},
      success: function(data) {
        console.log(wallets.length)
        $('#gasValue')
          .text('Gas Value : ' + (wallets.length * data.data.Gas) + ', Gas Value in ETH : ' + (wallets.length * data.data.GasinEther))
          .show();
      },
      error: function(result, status, err) {}
    });
  });
    $("input[name='memberCheckbox']").on('click', function(){
      $("input[name='memberCheckbox']").trigger('change');

   });

      $("#cong-modal").on('hide.bs.modal', function(){
      location.reload()
    });
    $("#sent-modal").on('hide.bs.modal', function(){    
      table.page.len( 10 ).draw();
    });
});