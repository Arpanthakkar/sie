$(document).ready(function () {
    $.validator.addMethod('passwordRule', function (value) {
        return /^^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\d)(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/.test(value);
    }, 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number');

    $('form[id="login_form"]').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            }
        },
        submitHandler: function (form) {
            $("#error").html('');
            var url = '/login';
            $.ajax({
                type: "POST",
                url: url,
                data: $("#login_form").serialize(), // serializes the form's elements.
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    $.each(data, function( index, value ) {
                        if(value.userRole=="admin")
                        {
                            $("#error").html('');
                            window.location.href = "dashboard1";
                        }
                        else { $("#error").html("Invalid Credentials.");}
                      });
                },
                error: function (result, status, err) {
                    console.log(result.responseJSON.error_stack[0]);
                    $("#error").html(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            });
        }
    });
});
