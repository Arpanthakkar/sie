$(document).ready(function () {

    $.validator.addMethod('passwordRule', function (value) {
        return /^^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\d)(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/.test(value);
    }, 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number');

    $('form[id="change_password_form"]').validate({
        rules: {
            old_password: {
                required: true,
            },
            new_password: {
                required: true,
                passwordRule: true,
            },
            confirm_password: {
                required: true,
                //passwordRule: true,
                equalTo: "#new_password",
            },
        },
        submitHandler: function (form) {
            $("#change-password-error").html('');
            var url = '/admin/changePassword';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'oldPassword': $("#old_password").val(),
                    'password': $("#new_password").val(),
                    'conPassword': $("#confirm_password").val()
                },
                success: function (data) {
                    $('#pwd-changed-modal').modal('show');
                    $('#modal-change-pw').modal('hide');
                    $('.modal-backdrop').removeClass("in");
                    $('.modal-backdrop').addClass("out");
                },
                error: function (result, status, err) {
                    $("#change-password-error").html(result.responseJSON.error_stack[0]);
                }
            });
        }
    });

 //Close of pwd-changed-modal modal redirct to login.
 $("#pwd-changed-modal").on('hide.bs.modal', function () {
 //   location.reload(true);
    $.ajax({
        type: "GET",
        url: "/admin/logout",       
        beforeSend: function () {
            $("#loader").show();
        },
        success: function (data) {
            location.reload(true);
        },
        error: function (result, status, err) {
            //location.reload(true);
        }
    });
});

});
