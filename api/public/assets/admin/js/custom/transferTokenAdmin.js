$(document).ready(function() {
  $('#loader1').hide();
  $('form[id="form_token"]').validate({
    rules: {
      toAddress: {
        required: true
      },
      amount: {
        required: true
      },
      token: {
        required: true
      }
    },
    submitHandler: function(form) {
      //check if wallet address is valid or not.
      $.ajax({
        type: 'GET',
        url: '../admin/iskyc',
        data: { toAddress: $('#toAddress').val() },
        beforeSend: function() {
          $('#loader1').show();
        },
        success: function(data) {
          //If all or any of the token is true.
          if (data.data == true) {
            //Transfer Token.
            $.ajax({
              type: 'POST',
              url: '../user/transfer',
              data: $('#form_token').serialize(), // serializes the form's elements.
              beforeSend: function() {
                $('#loader1').show()
              },
              success: function(data) {
                $('#sent-modal').modal('hide');
                $('#content').html('');
                $('#content').append(
                  "<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Tokens Sent Successfully</h2>"
                );
                $('#cong-modal').modal('show');
                $('#loader1').hide();
              },
              error: function(result, status, err) {
                $('#sent-modal').modal('hide');
                $('#content').html('');
                $('#content').append(
                  "<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Token(s) not sent, the number of token(s) sent exceeds the available balance</h2>"
                );
                $('#cong-modal').modal('show');
                $('#loader1').hide();

              }
            });
          } else {
            $('#sent-modal').modal('hide');
            $('#content').html('');
            $('#content').append(
              "<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>The wallet address that you have entered is not valid, only wallet addresses whitelisted by this platform are permitted</h2>"
            );
            $('#cong-modal').modal('show');
            $('#loader1').hide();

          }
        },
        error: function(result, status, err) {
          console.log("resssssssssssssssss",result)
          $('#sent-modal').modal('hide');
          $('#content').html('');
          $('#content').append(
            "<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Token(s) not sent, the number of token(s) sent is less than the minimum allowed</h2>"
          );
          $('#cong-modal').modal('show');
          $('#loader1').hide();

        }
      });
    }
  });  
  //Reload page after successful update.
  $("#cong-modal").on('hide.bs.modal', function () {
    location.reload(true);
    });

  //Display Gas Value in Transfer Modal.
  $('#amount').focusout(function() {
    //call api
    $.ajax({
      type: 'POST',
      url: '../user/getEstimate',
      data: {
        address: $('#toAddress').val(),
        amount: $('#amount').val()
      },
      beforeSend: function() {},
      success: function(data) {
        $('#gasValue')
          .text('Gas Value : ' + data.data.Gas + ', Gas Value in ETH : ' + data.data.GasinEther)
          .show();
      },
      error: function(result, status, err) {}
    });
  });
  //toAddress and formAddress not same.
  $('#toAddress').focusout(function() {
    //call api
    let a = $('#from_address').val();
    let b = $('#toAddress').val();

    let compare = a == b ? true : false;

    if (compare) {
      $('#transferTokenBtn').prop('disabled', true);
      $('#wrongWallet').text('From and To wallet address should not be same');
    } else {
      $('#transferTokenBtn').prop('disabled', false);
      $('#wrongWallet').text('');
    }
  });
  //Reset Modal on Closing.
  $('.modal').on('hidden.bs.modal', function(e) {
    $('#wrongWallet').text('');
    $('#gasValue').text('');

    $(this)
      .find('form')
      .trigger('reset');
  });
  $.extend($.validator.messages, {
    min: 'Please enter a value greater than or equal to 0.00000001'
  });
});
