
$(document).ready(function () {
    // Get the modal
    var modal = document.getElementsByClassName("myModal");
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementsByClassName("myImg");
    var modalImg = document.getElementsByClassName("img01");
    // var captionText = document.getElementById("caption");
    for (var i = 0; i < img.length; i++) {
        img[i].onclick = function () {
            modal[0].style.display = "block";
            modalImg[0].src = this.src;
            // captionText.innerHTML = this.alt;
        }
    }
    //Call KYC update API.
    $(".kyc_submit").click(function () {
        let isApproved      = $(this).data('isapproved');
        let params          = new window.URLSearchParams(window.location.search);
        let uploadData      = $("#uploadData").val();
        let kycStatus       = $("#kycStatus").val();
       
        if (isApproved == true) { 
            uploadData      = $(this).data('uploaddata');           
            kycStatus       = $(this).data('kycstatus');
        }    
        if (isApproved == false && $("#reason").val().trim() == '') {
            $("#error").html("Reason is required.");
            return false;
        }
        //if true then Change Main Kyc Status.
        if (kycStatus == true || kycStatus == "true") { 
            //change main kyc status
            $.ajax({
                type: "POST",
                url: '/admin/statusChange',
                data: {
                    'userId'        : params.get('userId'),
                    'isApproved'    : isApproved,
                    'rejectReason'  : $("#reason").val()
                },
                beforeSend: function () {
                    $(".loader_div").show();
                },
                success: function (data) {
                    $("#content").html('');
                    if(isApproved){
                        $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Account Approved Successfully</h2>")
                    } else {
                        $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Account Rejected Successfully</h2>")
                    }                    $("#cong-modal").modal("show");
                    $(".loader_div").hide();
                },
                error: function (result, status, err) {
                    $("#content").html('');
                    $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>KYC Status Not Changed</h2>")
                    $("#cong-modal").modal("show");
                    $(".loader_div").hide();
                }
            });
        }
        //Change Document Kyc Status.
        else {
            //If clicked on approve button then get from data attribute otherwise from modal.   
            $.ajax({
                type: "POST",
                url: '/admin/documentStatusChange',
                data: {
                    'userId': params.get('userId'),
                    'isApproved': isApproved,
                    'uploadData': uploadData,
                    'rejectReason': $("#reason").val()
                },
                beforeSend: function () {
                    $(".loader_div").show();
                },
                success: function (data) { 
                    $("#content").html('');
                    if(isApproved){
                        $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Document has been Approved Succesfully</h2>")
                    } else {
                        $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Document has been Rejected Succesfully</h2>")
                    }
                    $("#cong-modal").modal("show");
                    $(".loader_div").hide();
                },
                error: function (result, status, err) {
                    $("#content").html('');
                    $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>KYC Status Not Changed</h2>")
                    $("#cong-modal").modal("show");
                    $(".loader_div").hide();
                }
            });
        }
    });

    //Call User block.
    $(".userSuspend").click(function () {
        let params = new window.URLSearchParams(window.location.search);

            $.ajax({
                type: "POST",
                url: '/admin/userSuspend',
                data: {
                    'userId' : params.get('userId'),
                },
                beforeSend: function () {
                    $(".loader_div").show();
                },
                success: function (data) {
                    $("#content").html('');
                    $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>User Disabled Successfully</h2>")
                    $("#cong-modal").modal("show");
                    $(".loader_div").hide();
                },
                error: function (result, status, err) {
                    $("#content").html('');
                    $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Failed</h2>")
                    $("#cong-modal").modal("show");
                    $(".loader_div").hide();
                }
            });

    });

    

    //Call Main KYC status change API
    $(".kyc_status").click(function () {
        let isApproved = $(this).data('isapproved');
        let params = new window.URLSearchParams(window.location.search);


    });

    $('#reject-modal').on('show.bs.modal', function (e) {
        $("#error").html("");
        $("#reason").val("");
        //get data-id attribute of the clicked element
        var uploadData = $(e.relatedTarget).data('uploaddata');
        var kycStatus = $(e.relatedTarget).data('kycstatus');

        //populate the textbox
        $("#uploadData").val(uploadData);
        $("#kycStatus").val(kycStatus);
    });

    //Reload page after successful update.
    $("#cong-modal").on('hide.bs.modal', function () {
        location.reload(true);
    });
});
