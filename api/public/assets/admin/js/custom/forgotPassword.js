$(document).ready(function () {
    $("#forgot-pw-modal").on('show.bs.modal', function () {
        $("#forget_email").val("");
    });
    $("#forget_form").validate({
        rules: {
            forget_email: {
                required: true,
                email: true,
            }
        },
        submitHandler: function (form) {
            $("#forget_error").html('');
            var url = '/forgotPassword';
            var email = $("input[name=forget_email]").val();
            $.ajax({
                type: "POST",
                url: url,
                data: { email: email },
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    $('#forgot-pw-modal').modal('hide');
                    $('#success-msg').modal('show');
                    $("#loader").hide();
                },
                error: function (result, status, err) {
                    $("#forget_error").text(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            });
        }
    })
});
