$(document).ready(function () {

    $("#loader").hide();
    $("#kyc_form").submit(function (event) {
        event.preventDefault();
        //flags of verified
        let kycId = $('#kycId').val();
        let kycAddress = $('#kycAddress').val();
        let kycPhoto = $('#kycPhoto').val();
        let idproof='',addressproof='';
        //files      
        let idProofFile = '';
        let addressProofFile = '';
        let withSelfiePhotoIdFile = '';
        //Types
        let addressProofFileType = 'file';
        let idProofFileType = 'file';
        let withSelfiePhotoIdFileType = 'file';

        if (kycId == "verified") {
            idProofFileType = 'name';
            idProofFile =$('#kycIdProof').val();
            idproof = $("#chooseId").val();
        }else{
            idProofFile =$('#idProofFile').get(0).files[0];
            idproof = $('#idProof').val();
        }
        if (kycAddress == "verified") {
            addressProofFileType = 'name';
            addressProofFile=$('#KycAddressProof').val();
            addressproof =$("#chooseAddressId").val();
        }else{
            addressProofFile = $('#addressProofFile').get(0).files[0];
            addressproof = $('#addressProof').val();
        }
        
        if (kycPhoto == "verified") {
            withSelfiePhotoIdFileType = 'name';
            withSelfiePhotoIdFile =$('#KycPhotoId').val();
        }else{
            withSelfiePhotoIdFile = $('#selfieWithPhoto').get(0).files[0];
        }

    //    alert(idproof +" " +addressproof +" "+idProofFile+" "+addressProofFile+" "+withSelfiePhotoIdFile+" ")
      //  alert(addressProofFileType +" "+ idProofFileType +" "+withSelfiePhotoIdFileType)

        var fd = new FormData();
        fd.append('idProof', idproof);
        fd.append('idProofFile', idProofFile);

        fd.append('addressProof', addressproof);
        fd.append('addressProofFile', addressProofFile);

        fd.append('withSelfiePhotoIdFile', withSelfiePhotoIdFile);

        fd.append('addressProofFileType', addressProofFileType);
        fd.append('idProofFileType', idProofFileType);
        fd.append('withSelfiePhotoIdFileType', withSelfiePhotoIdFileType);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/user/kycDocument",
            data: fd,
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                window.location.href = "kyc-verification-step-3";
            },
            error: function (e) {
                $("#upload_error").text("Document Upload Unsuccessful.");
                $("#loader").hide();
            },

        });

    });

    $('#agree1').change(function () {
        if ($("#agree1").is(":checked") && $("#agree2").is(":checked") && $("#agree3").is(":checked")) {
            $('#agree_submit').attr("disabled", false);
        }
        else {
            $('#agree_submit').attr("disabled", true);
        }
    });

    $('#agree2').change(function () {
        if ($("#agree1").is(":checked") && $("#agree2").is(":checked") && $("#agree3").is(":checked")) {
            $('#agree_submit').attr("disabled", false);
        }
        else {
            $('#agree_submit').attr("disabled", true);
        }

    });

    $('#agree3').change(function () {
        if ($("#agree1").is(":checked") && $("#agree2").is(":checked") && $("#agree3").is(":checked")) {
            $('#agree_submit').attr("disabled", false);
        }
        else {
            $('#agree_submit').attr("disabled", true);
        }

    });

    $("#agree_form").submit(function (event) {
        event.preventDefault();
        if ($("#agree1").is(":checked") && $("#agree2").is(":checked") && $("#agree3").is(":checked")) {
            $.ajax({
                type: "POST",
                url: "/user/agreement",
                data: '',
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    window.location.href = "kyc-verification";
                },
                error: function (e) {
                    $("#loader").hide();
                }
            });
        }
    });

});
