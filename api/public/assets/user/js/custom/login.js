$(document).ready(function () {
    $("#loader").hide();

    $.validator.addMethod('passwordRule', function (value) {
        return /^^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\d)(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/.test(value);
    }, 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number');

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
      }, "Please enter only characters."); 
    
    $.validator.addMethod("zipcode", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9-]*$/i.test(value);
    }, "No special characters allowed."); 

    $('form[id="login_form"]').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            }
        },
        submitHandler: function (form) {
            $("#error").html('');
            var url = '/login';
            $.ajax({
                type: "POST",
                url: url,
                data: $("#login_form").serialize(), // serializes the form's elements.
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    window.location.href = "dashboard1"
                },
                error: function (result, status, err) {
                    console.log(result.responseJSON.error_stack[0]);
                    $("#error").html(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            });
        }
    });
    jQuery.validator.setDefaults({
        errorPlacement: function(error, element) {
            console.log(error)
            if(element[0].name=="dob")
            {;
                $('.date').after(error);
            }
            else
            {
                $(element).after(error);

            }
        }
    });   
    $('form[id="signup_form"]').validate({
     rules: {
            first_name: {
                required: true,
                lettersonly:true
            },
            last_name: {
                required: true,
                lettersonly:true
            },
            phone: {
                digits: true,
                required: true,
            },
            country_code: {
                required: true,
            },
            dob: {
                 required: true,
             },
            email: {
                required: true,
                email: true,
            },
            address1: {
                required: true
            },
            country: {
                required: true
            },
            city: {
                required: true,
                lettersonly:true
            },
            password: {
                required: true,
                passwordRule: true
            },
            confirm_password: {
                equalTo: "#password",
            },
            zipCode: {
                //digits: true
                zipcode:true
            }

        },
        messages: {
            passwordRule: 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number',
            confirm_password : 'Passwords do not match, please enter the same value again.'
        },
        submitHandler: function (form) {
           $("#error").html('');
            var form = $(this);
            var url = '/register';
            var address = $("input[name=address1]").val() + " " + $("input[name=address2]").val();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    "fname": $("input[name=first_name]").val().trim(),
                    "lname": $("input[name=last_name]").val().trim(),
                    "country": $("#country").val(),
                    "email": $("input[name=email]").val().toLowerCase(),
                    "password": $("input[name=password]").val(),
                    "conPassword": $("input[name=confirm_password]").val(),
                    "address": address,
                    "city": $("#city").val(),
                    "countryCode": $("#country_code").val(),
                    "phoneNumber": $("input[name=phone]").val(),
                    "dob": $("input[name=dob]").val(),
                    "zipCode": $("#zipCode").val()
                },
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    //open modal.                    
                  //  $('#authontication').modal('show');                
                  let url = '/otpCheck';
                  $.ajax({
                      type: "POST",
                      url: url,
                      data: {
                          "email": $("input[name=email]").val().toLowerCase(),
                      },
                      success: function (data) {
                          //open Congratulations modal.  
                          //$('#authontication').modal('hide');
                          $('#cong-modal').modal('show');
                          $("#loader").hide();
                      },
                      error: function (result) {
                          console.log(result);
                        //   $("#txt1").val('');
                        //   $("#txt2").val('');
                        //   $("#txt3").val('');
                        //   $("#txt4").val('');
                        $("#loader").hide();
                           $("#error").html(result.responseJSON.error_stack[0]);
                      }
                  });
                },
                error: function (result) {
                    $("#error").html(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            });
        }
    }); 

    //OTP check for phone in Signup.
    // $("#submitOTP").click(function () {
    //     $("#OTPerror").html('');
    //     let otp = $("#txt1").val() + $("#txt2").val() + $("#txt3").val() + $("#txt4").val();
    //     let url = '/otpCheck';
    //     $.ajax({
    //         type: "POST",
    //         url: url,
    //         data: {
    //             "email": $("#email").val().trim(),
    //             "otp": otp,
    //         },
    //         success: function (data) {
    //             //open Congratulations modal.  
    //             $('#authontication').modal('hide');
    //             $('#cong-modal').modal('show');
    //         },
    //         error: function (result) {
    //             //console.log(result);
    //             $("#txt1").val('');
    //             $("#txt2").val('');
    //             $("#txt3").val('');
    //             $("#txt4").val('');
    //             $("#OTPerror").html(result.responseJSON.error_stack[0]);
    //         }
    //     });
    // });

    $('#agree').change(function () {
        //If the checkbox is checked.
        if ($(this).is(':checked')) {
            //Enable the submit button.
            $('#submit').attr("disabled", false);
        } else {
            //If it is not checked, disable the button.
            $('#submit').attr("disabled", true);
        }
    });


    $("#forgot-pw-modal").on('show.bs.modal', function () {
        $("#forget_email").val("");
    });

    $("#forget_form").validate({
        rules: {
            forget_email: {
                required: true,
                email: true,
            }
        },
        submitHandler: function (form) {
            $("#forget_error").html('');
            var url = '/forgotPassword';
            var email = $("input[name=forget_email]").val();
            $.ajax({
                type: "POST",
                url: url,
                data: { email: email },
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    $('#forgot-pw-modal').modal('hide');
                    $('#success-msg').modal('show');
                    $("#loader").hide();
                },
                error: function (result, status, err) {
                    $("#forget_error").text(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            });
        }
    })

    // $('form[id="login_form"]').validate({
    //     rules: {
    //         email: {
    //             required: true,
    //             email: true,
    //         },
    //         password: {
    //             required: true,
    //         }
    //     },
    //     submitHandler: function (form) {
    //         $("#error").html('');
    //         var url = '/login';
    //         $.ajax({
    //             type: "POST",
    //             url: url,
    //             data: $("#login_form").serialize(), // serializes the form's elements.
    //             success: function (data) {
    //                 window.location.href = "dashboard1"
    //                 console.log(data.status);
    //                 // $('loader-svg').show();
    //             },
    //             error: function (result, status, err) {
    //                 console.log(result.responseJSON.error_stack[0]);
    //                 $("#error").html(result.responseJSON.error_stack[0]);
    //             }
    //         }); ''
    //     }
    // });

    $('form[id="reset_form"]').validate({
        rules: {
            reset1: {
                required: true,
                passwordRule: true
            },
            reset2: {
                required: true,
                passwordRule: true,  
                equalTo: "#reset1",        
            },
            
        }, 
        messages: {
            passwordRule: 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number',
            reset2 : 'Passwords do not match, please enter the same value again.'
        },   
        submitHandler: function (form) {
            let params = new window.URLSearchParams(window.location.search);
            //console.log(params.get('token'),$("#reset1").val().trim(),$("#reset2").val().trim());
            let url = '/user/reset-password';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'Token': params.get('token'),
                    'newPassword': $("#reset1").val().trim(),
                    'conPassword': $("#reset2").val().trim()
                },
                beforeSend: function () {
                    // Display Loader.
                    $("#loader").show();
                },
                success: function (data) {
                    $('#success-msg').modal('show');
                    $("#loader").hide();
                },
                error: function (result, status, err) {
                    $("#reset_error").html(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            });

        }
    });

    // $('.input-group.date').datepicker({ format: "dd/mm/yyyy" }); 

    $('.signup_password').keyup(function () {
        $('#result').html(checkStrength($('.signup_password').val()))
    })
    function checkStrength(password) {
        var strength = 0
        if (password.length < 8) {
            $('#result').removeClass()
            $('#result').addClass('short')
            return 'Weak'
        }
        if (password.length > 8) strength += 1
        // If password contains both lower and uppercase characters, increase strength value.
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
        // If it has numbers and characters, increase strength value.
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
        // If it has one special character, increase strength value.
        if (password.match(/([!,%,&,@,.,#,$,^,*,?,_,~])/)) strength += 1
        // Calculated strength value, we can return messages
        // If value is less than 2
        if (strength < 2) {
            $('#result').removeClass()
            $('#result').addClass('short')
            return 'Weak'
        } else if (strength == 2) {
            $('#result').removeClass()
            $('#result').addClass('good')
            return 'Good'
        } else if(strength==4){
            $('#result').removeClass()
            $('#result').addClass('strong')
            return 'Strong'
        }
    }

    $('form[id="change_password_form"]').validate({
        rules: {
            old_password: {
                required: true,
            },
            new_password: {
                required: true,
                passwordRule: true,
            },
            confirm_password: {
                required: true,
              //  passwordRule: true,
                equalTo: "#new_password",
            },
        },
        messages: {
            passwordRule: 'Password should be minimum of eight characters and include at least one upper case character, one lower case character, one special character and one number',
            confirm_password : 'Passwords do not match, please enter the same value again.'
        },
        submitHandler: function (form) {
            $("#change-password-error").html('');
            var url = '/user/changePassword';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'oldPassword': $("#old_password").val(),
                    'password': $("#new_password").val(),
                    'conPassword': $("#confirm_password").val()
                },
                success: function (data) {
                    $('#pwd-changed-modal').modal('show');
                    $('#modal-change-pw').modal('hide');
                    $('.modal-backdrop').removeClass("in");
                    $('.modal-backdrop').addClass("out");
                },
                error: function (result, status, err) {
                    console.log(result.responseJSON.error_stack[0]);
                    $("#change-password-error").html(result.responseJSON.error_stack[0]);
                }
            });
        }
    });
    //Close of pwd-changed-modal modal redirct to login.
    /*$("#pwd-changed-modal").on('hide.bs.modal', function () {
        //   location.reload(true);
        $.ajax({
            type: "GET",
            url: "/user/logout",
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                location.reload(true);
            },
            error: function (result, status, err) {
                //location.reload(true);
            }
        });
    });*/
    // $("#changePhone").click(function () {
    //     $("#change-phone-error").html('');
    //     $("#change_phone_form").validate({
    //         rules: {
    //             country_code: {
    //                 required: true,
    //             },
    //             phone: {
    //                 required: true,
    //             }
    //         },
    //         submitHandler: function (form) {
    //             $("#change-phone-error").html('');
    //             var url = '/user/changePhoneNumber';
    //             var country_code = $("#country_code option:selected").val();
    //             var phone = $("input[name=phone]").val();
    //             $.ajax({
    //                 type: "POST",
    //                 url: url,
    //                 data: { 'countryCode': country_code, 'phoneNumber': phone },
    //                 beforeSend: function () {
    //                     $("#loader").show();
    //                 },
    //                 success: function (data) {
    //                     $('#modal-change-phone').modal('hide');
    //                   //  $('#authontication').modal('show');
    //                     $("#loader").hide();
    //                 },
    //                 error: function (result, status, err) {
    //                     $("#change-phone-error").text(result.responseJSON.error_stack[0]);
    //                     $("#loader").hide();
    //                 }
    //             });
    //         }
    //     })
    // })
    
    $("#closeReset").click(function () {
        location.href="/login"
    });

    //OTP check for change Phone.
    $("#changePhone").click(function () {
        $("#OTPerror").html('');
        let otp = $("#txt1").val() + $("#txt2").val() + $("#txt3").val() + $("#txt4").val();
        let url = '/user/checkOtpChangePhoneNo';
        var country_code = $("#country_code option:selected").val();
        var phone = $("input[name=phone]").val();
        $.ajax({
            type: "POST",
            url: url,
            data: { 'countryCode': country_code, 'phoneNumber': phone },
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                //open Congratulations modal.  
                $('#authontication').modal('hide');
                $('#modal-change-phone').modal('hide');
                $("#cong-msg").html("<h2>Your Phone Number has been Updated Successfully.</h2>");
                $('#cong-modal').modal('show');
                $("#loader").hide();
                // $('.modal-backdrop').removeClass("in");
                // $('.modal-backdrop').addClass("out");
            },
            error: function (result) {
                $("#loader").hide();
                console.log(result);
                // $("#txt1").val('');
                // $("#txt2").val('');
                // $("#txt3").val('');
                // $("#txt4").val('');
                $("#change-phone-error").html(result.responseJSON.error_stack[0]);
            }
        });
    });

    $("#reSendOTP").click(function () {
        $("#change-phone-error").html('');
                var url = '/user/changePhoneNumber';
                var country_code = $("#country_code option:selected").val();
                var phone = $("input[name=phone]").val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { 'countryCode': country_code, 'phoneNumber': phone },
                    beforeSend: function () {
                        $("#loader").show();
                    },
                    success: function (data) {
                        // $('#authontication').modal('hide');
                        // $('#authontication').modal('show');

                        $("#loader").hide();
                        $("#re-send-otp").html('OTP sent successfully');
                        // success text-success
                    },
                    error: function (result, status, err) {
                        //alert("444444444")
                        $("#change-phone-error").text(result.responseJSON.error_stack[0]);
                        $("#loader").hide();
                    }
                });
        
    })

    $("#reSendOTPSignUp").click(function () {
        $("#change-phone-error").html('');
        $("#OTPerror").html('');
                var url = '/user/changePhoneNumberSignUp';
                var country_code = $("#country_code option:selected").val();
                var phone = $("input[name=phone]").val();
                var email = $("input[name=email]").val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { 'countryCode': country_code, 'phoneNumber': phone, 'email': email },
                    beforeSend: function () {
                        $("#loader").show();
                    },
                    success: function (data) {
                        // $('#authontication').modal('hide');
                        // $('#authontication').modal('show');

                        $("#loader").hide();
                        $("#re-send-otp").html('OTP sent successfully');
                        // success text-success
                    },
                    error: function (result, status, err) {
                        //alert("444444444")
                        $("#change-phone-error").text(result.responseJSON.error_stack[0]);
                        $("#loader").hide();
                    }
                });
        
    })

});
