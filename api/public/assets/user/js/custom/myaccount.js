$(document).ready(function () {
    //Code for Tabbing in OTP Modal.
    $(function () {
        'use strict';

        var body = $('body');

        function goToNextInput(e) {
            var key = e.which,
                t = $(e.target),
                sib = t.next('#authontication input');

            if (key != 9 && (key < 48 || key > 57)) {
                e.preventDefault();
                return false;
            }

            if (key === 9) {
                return true;
            }

            if (!sib || !sib.length) {
                sib = body.find('#authontication input').eq(0);
            }
            sib.select().focus();
        }

        function onKeyDown(e) {
            var key = e.which;

            if (key === 9 || (key >= 48 && key <= 57)) {
                return true;
            }

            e.preventDefault();
            return false;
        }

        function onFocus(e) {
            $(e.target).select();
        }

        body.on('keyup', '#authontication input', goToNextInput);
        body.on('keydown', '#authontication input', onKeyDown);
        body.on('click', '#authontication input', onFocus);
    })


    var formChanged = false;
    $('form[id="myaccount_form"]').validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            /* phone: {
                 digits: true,
                 required: true,
             },*/
            country_code: {
                required: true,
            },
            dob: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            address: {
                required: true
            },
            country: {
                required: true
            },
            city: {
                required: true
            },
            password: {
                required: true,
                passwordRule: true
            },
            confirm_password: {
                equalTo: "#password",
            }
        },
        submitHandler: function (form) {
            formChanged = true;

            $("#account_error").html('');
            // console.log("11",$('#profile_pic').files[0]);
            var baseurl = $("input[name=baseurl]").val();
            var form = new FormData();
            form.append("profilePicture", $('#profile_pic').get(0).files[0]);
            form.append("fname", $("input[name=first_name]").val().trim());
            form.append("lname", $("input[name=last_name]").val().trim());
            form.append("country", $("#country").val());
            form.append("address", $("#address").val());
            form.append("city", $("#city").val());
            form.append("countryCode", $("#country_code").val());
            //   form.append("phoneNumber", $("input[name=phone]").val());
            form.append("dob", $("input[name=dob]").val());
            form.append("zipCode", $("#zipCode").val());
            form.append("email", $("input[name=email]").val());

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": baseurl + "/user/userProfile",
                "method": "POST",
                "processData": false,
                "contentType": false,
                "data": form,
                beforeSend: function () {
                    // Display Loader.
                    $("#loader").show();
                },
                success: function (data) {
                    //open modal.                    
                    $('#cong-modal').modal('show');
                    $("#loader").hide();
                },
                error: function (result) {
                    $("#account_error").html(result.responseJSON.error_stack[0]);
                    $("#loader").hide();
                }
            }

            $.ajax(settings).done(function (response) {
                // console.log(response);
            });

        }
    });
    $('#myaccount_form').on('keyup change paste', 'input, select, textarea', function(){
        formChanged = true;
    });
    $("#cong-modal").on('hide.bs.modal', function () {
        formChanged = false;
        location.reload(true);
    });

    window.onbeforeunload = function () {
        if (formChanged) {
            return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        }
    };
});