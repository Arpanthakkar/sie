$(document).ready(function () {
    $(function () {
        'use strict';

        var body = $('body');

        function goToNextInput(e) {
           var key = e.which,
              t = $(e.target),
              sib = t.next('#authontication input');

           if (key != 9  && (key < 48 || key > 57) && (key < 96 || key > 105)) {
              e.preventDefault();
              return false;
           }

           if (key === 9 ) {
              //tab
              return true;
           }

           if (!sib || !sib.length) {
             // sib = body.find('#authontication input').eq(0);
           }
           sib.select().focus();
        }

        function onKeyDown(e) {
           var key = e.which;
           
           if (key >= 65 && key <= 90) {
              //letters
              return false;
           }
           if (key === 9 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
              return true;
           }
           if (key == 8) {
              //Backspace Key.
              if ($(e.target).val() != '') {
                 $(e.target).val('');
              }
              else {
                 var key = e.which,
                    t = $(e.target),
                    sib = t.prev('#authontication input');

                 if (!sib || !sib.length) {
                    sib = body.find('#authontication input').eq(0);
                 }
                 sib.select().focus();
              }
           }
           if (key == 46) {
              //Delete Key
              $(e.target).val('');
           }
        }
        
        function onFocus(e) {
           $(e.target).select();
        }

        body.on('keyup', '#authontication input', goToNextInput);
        body.on('click', '#authontication input', onFocus);
        body.on('keydown', '#authontication input', onKeyDown);

        //prevent character input.
        $('.otp').keypress(function (e) {
           var regex = new RegExp("^[0-9-]+$");
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
           if (regex.test(str)) {
              return true;
           }

           e.preventDefault();
           return false;
        });
        $('.input-group.date').datepicker({ format: "dd/mm/yyyy", endDate: "today" });

        //Populate Country dropdowns.
        let countryDropdown = $('#country');
        countryDropdown.empty();
        countryDropdown.append('<option selected="true" disabled>Select Country</option>');
        countryDropdown.prop('selectedIndex', 0);

        let countryCodeDropdown = $('#country_code');
        countryCodeDropdown.empty();
        const url = 'https://restcountries.eu/rest/v2/all';

        // Populate dropdown with list of provinces
        $.getJSON(url, function (data) {
           $.each(data, function (key, entry) {
              if (entry.callingCodes != '') {
                 countryDropdown.append($('<option></option>').attr('value', entry.name).text(entry.name));
                 countryCodeDropdown.append($('<option></option>').attr('value', entry.callingCodes).text("+" + entry.callingCodes + "   (" + entry.alpha2Code + ")"));
              }
           })
        });

     })
     //Google Authenticator.
    $("#submitOTP").click(function (e) {
      $("#OTPerror").html("").hide();
       /// $("#loader").show();
        let otp = $("#txt1").val() + $("#txt2").val() + $("#txt3").val() + $("#txt4").val()+$("#txt5").val()+$("#txt6").val();
         if(otp=='' || otp==null)
         {
            $("#OTPerror").html("Please enter verification code.").show();          
            e.preventDefault();
         }
         else{
            $.ajax({
               type: "GET",
               url: '/user/googleAuthenticator',
               data: {
                   'token': otp,
               },
               beforeSend: function () {
                  $("#loader").show();
              },
               success: function (data) { 

                  $("#loader").hide();
                  if(data)
                  {
                     $.ajax({
                        type: "POST",
                        url: "/user/createWallet",
                       // data : {'userId' : }
                        beforeSend: function () {   
                            $("#loader").show();                 
                        },
                        success: function (data) {                         
                        
                              window.location.href='/user/dashboard1';                      
                          
                        },
                        error: function (result, status, err) {
                            $("#loader").hide();
                        }
                    });
                     //window.location.href='/user/dashboard1'
                  }
                  else{
                     $("#OTPerror").html("Invalid Code.").show();
                     $("#txt1").val('');
                     $("#txt2").val('');
                     $("#txt3").val('');
                     $("#txt4").val('');
                     $("#txt5").val('');
                     $("#txt6").val(''); 
                  }
               },
               error: function (result, status, err) {               
                   $("#loader").hide();
                   $("#OTPerror").html("Invalid Code.").show();
                   $("#txt1").val('');
                  $("#txt2").val('');
                  $("#txt3").val('');
                  $("#txt4").val('');
                  $("#txt5").val('');
                  $("#txt6").val(''); 
   
               }
           });
         }         
    });
    $("#cong-modal").on('hide.bs.modal', function () {
        window.location.href = "dashboard1";
    });
})