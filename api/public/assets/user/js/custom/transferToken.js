
$(document).ready(function () {
    $( "#toAddress" ).change(function() {
        $('#wrongWallet').text('').hide();
      });
      $( "#amount" ).change(function() {
        $('#amountError').text('').hide();
      });      
      $( "#token" ).change(function() {
        $('#amountError').text('').hide();
      });    
      $('form[id="form_token"]').validate({
        rules: {
            toAddress: {
                required: true,
            },
            amount: {
                required: true,
            },
            token: {
                required: true,
            }
        },
        submitHandler: function (form) {     
          
        let a = $('#fromAddress').val();
        let b = $('#toAddress').val();
        let errs=0;
        $('#wrongWallet').text('').hide();
        $('#amountError').text('').hide();
        $('#tokenError').text('').hide(); 
        let compare = a == b ? true : false;
        if (compare) {
                $('#wrongWallet').text('You cannot transfer tokens to your own address.').show();
                errs++;
            }   
        
        if(errs!=0)
        {
          return false;
        }
        else{
              //check if wallet address is valid or not.            
              $.ajax({
                type: "GET",
                url: "../admin/iskyc",
                data: { "toAddress": $("#toAddress").val() },
                beforeSend: function () {
                    $("#loader2").show();
                },
                success: function (data) {
                    //If all or any of the token is true.
                    if (data.data == true) {
                        //Open mnemonic modal.
                        $("#token1").val($('#token').val());
                        $("#toAddress1").val($('#toAddress').val());
                        $("#fromAddress1").val($('#fromAddress').val());
                        $("#amount1").val($('#amount').val());

                        $("#mnemonicModal").modal('show');
                        $("#sent-modal").modal("hide");  $("#loader2").hide();
                    }
                    else{
                        $("#sent-modal").modal("hide");
                        $("#content").html('');
                        $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>The wallet address that you have entered is not valid, only wallet addresses whitelisted by this platform are permitted</h2>")
                        $("#cong-modal").modal("show");
                        $("#loader2").hide();
                    }
                },
                error: function (result, status, err) {
                    $("#sent-modal").modal("hide");
                    $("#content").html('');
                    $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>The wallet address that you have entered is not valid, only wallet addresses whitelisted by this platform are permitted</h2>")
                    $("#cong-modal").modal("show");
                    $("#loader2").hide();
                }
            });
        }              
      },
    }); 

   
      $("#mnemonicBtn").click(function(){  
        $.ajax({
          type: "POST",
          url: '/user/verifyMnemonic',
          data: {
              'mnemonic': $("#mnemonicAddredss").val().trim(),
          },
          beforeSend: function () {
            $("#loader3").show();
           },
          success: function (data) {
           
              $.ajax({
                  type: "POST",
                  url: '/user/addMnemonic',
                  data: {
                      'mnemonicAddredss': $("#mnemonicAddredss").val().trim(), 
                  },
                 
                  success: function (data) {   
                      $.ajax({
                        type: "POST",
                        url: "/user/transfer",
                        data: {
                            'toAddress' : $('#toAddress1').val().trim(),
                            'amount' :$('#amount1').val(),
                            'token' :$("#token1").val(),
                            'fromAddress' : $('#fromAddress1').val(),
                            'mnemonicAddredss' : $('#mnemonicAddredss').val()                            
                        }, // serializes the form's elements.
                        beforeSend: function () {
                        //    $("#loader").show();
                        },
                        success: function (data) {
                            $("#mnemonicModal").modal("hide");
                            $("#content").html('');
                            $("#content").append("<div class=success-msg-icon><i class='fa fa-check-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Tokens Sent Successfully</h2>")
                            $("#cong-modal").modal("show");
                            $("#loader3").hide();
                        },
                        error: function (result, status, err) {
                            $("#mnemonicModal").modal("hide");
                            $("#content").html('');
                            $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message style=line-height:1.5>You do not have sufficient Ethereum or Tokens to complete this transaction.</h2>")
                            $("#cong-modal").modal("show");
                            $("#loader3").hide();
                        }
                    });
                  },
                  error: function (result, status, err) {
                      $("#content").html('');
                      $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Adding mnemonic address failed !!</h2>")
                      $("#cong-modal").modal("show");
                      $("#loader3").hide();
                  }
              });
          },
          error: function (result, status, err) {
              $("#content").html('');
              $("#content").append("<div class=error-msg-icon><i class='fa fa-times-circle m-b-0' aria-hidden='true'></i></div><h2 id=message>Invalid Mnemonic Address!!</h2>")
              $("#cong-modal").modal("show");
              $("#loader3").hide();
          }
      });
     
      });

    $("#loader").hide();    $("#loader2").hide();  $("#loader3").hide();

  //Reload page after successful update.
  $("#cong-modal").on('hide.bs.modal', function () {
    location.reload(true);
    });
    //Display Gas Value in Transfer Modal.
    $( "#amount" ).focusout(function() {
        //call api
        // alert($("#toAddress").val())
        // alert($("#amount").val())
       $.ajax({
        type: "POST",
        url: "../user/getEstimate",
        data: {
            'address': $("#toAddress").val(),
            'amount' : $("#amount").val()
        },
        beforeSend: function () {
        },
        success: function (data) {
            $( "#gasValue" ).text("Gas Value : " +data.data.Gas+", Gas Value in ETH : "+ data.data.GasinEther).show();
        },
        error: function (result, status, err) {
            console.log("error",err)
        }
    });    
    })
    //Reset Modal on Closing.
    $('.modal').on('hidden.bs.modal', function(e)
    {   
        $('#wrongWallet').text('');
        $('#gasValue').text('');
        $(this).find('form').trigger('reset');
    });
    $.extend($.validator.messages, {
        min: 'Please enter a value greater than or equal to 0.00000001',
    });
});
