$(document).ready(function () {
    
    let qrCode = $("#qrCode").val();
    $( "#okButton" ).click(function() {
        $.ajax({
            type: "POST",
            url: "/user/sendSecretORCode",
            data: {'secretCode':qrCode},
         
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {               
               $("#loader").hide();
               window.location.href='/user/mnemonic';           
            },
            error: function (e) {               
                $("#loader").hide();
            },
    
        });
    });
   
});