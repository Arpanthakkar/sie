var moment = require('moment');

module.exports = {
    database: {
        "username": "",
        "password": "",
        "port": "27017",
        "host": "localhost",
        "name": "sie"
    },
    getUtcCurrentDateTime: function () {
        return moment().utc().toDate();
    },
    secretForJWT: "sieSecretStringForJWT",
   //BASEURL :'http://localhost:3000',
   //BASEURL :'http://3.8.116.204:3000',
   BASEURL: 'http://52.210.125.181:3000'
}