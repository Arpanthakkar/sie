const { User, ForgotPass

} = require('../models/dbSchemas');
const md5 = require('md5');
const bcrypt = require('bcryptjs');
const { Sms, otpGenerate, mail } = require('../helpers/commonFunction');
const { setJWTToken } = require('../middleware/authToken');

module.exports = {

    checkTokenInDB: async (email, token) => {
        try {
            let userActiveCheck = await User.findOne({
                jwtToken: token,
                email: email
            });

            //     console.log(userActiveCheck)
            if (userActiveCheck) {
                return true;
            } else {
                return false;
            }
        } catch (err) { console.log('error', err) }


    },
    // removeTokenInDB: async function (token) {
    //     let logoutResult = await User.deleteOne({
    //         token: token
    //     });
    //     if (logoutResult.deletedCount) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // },


}