const mongoose = require('mongoose');
mongoose.set("debug", (collectionName, method, query, doc) => {
    console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
});
var userSchema = new mongoose.Schema({
    firstName: {type: String,required:true},
    lastName: String,
    email: String,
    password: String,
    address: String,
    city: String,
    country: String,
    zipCode: String,
    countryCode: String,
    phoneNumber: Number,
    profileImage: String,
    dob: String,
    wallet: {type:String,'default':null},
    secretQRCode: { type: String, 'default': null },
    isGoogleAuthorised: {
        type: Boolean,
        'default': false
    },
    isUserLoggedIn :{type:Boolean,'default': false},
    flags: {
        agreement: {
            type: Boolean,
            'default': false
        },
        block_user: {
            type: Boolean,
            'default': false
        },
        isVerifyMobile: {
            type: Boolean,
            'default': true
        },
        isVerifyEmail: {
            type: Boolean,
            'default': false
        },
        kyc: {
            status: {
                type: Boolean,
                default: false
            },
            isKyc: {
                type: String,
                enum: ['notVerified', 'pending', 'reject', 'verified', 'suspend'],
                default: 'notVerified',
            },
            kycRejectReason: {
                type: String,
                default: 'null'
            },
        },
        kycUploadAddressProof: {
            status: {
                type: Boolean,
                default: false
            },
            isKyc: {
                type: String,
                enum: ['notVerified', 'pending', 'reject', 'verified'],
                default: 'notVerified',
            },
            kycRejectReason: {
                type: String,
                default: 'null'
            },
        },
        kycUploadIdProof: {
            status: {
                type: Boolean,
                default: false
            },
            isKyc: {
                type: String,
                enum: ['notVerified', 'pending', 'reject', 'verified'],
                default: 'notVerified',
            },
            kycRejectReason: {
                type: String,
                default: 'null'
            },
        },
        KycUploadWithSelfiePhoto: {
            status: {
                type: Boolean,
                default: false
            },
            isKyc: {
                type: String,
                enum: ['notVerified', 'pending', 'reject', 'verified'],
                default: 'notVerified',
            },
            kycRejectReason: {
                type: String,
                default: 'null'
            },
        },

    },
    otpMobile: {
        type: String,
        default: null
    },
    emailLink: String,

    timestamp: {
        type: Date,
        'default': Date.now,
        index: true
    },
    addedBy: {
        type: String,
        default: 'user'
    },
    emailToken: String,
    authToken: String,
    otpExpire: String,
    emailTokenExpire: String,
    jwtToken: String,
    userRole: String,
    kycDetails: {
        chooseId: String,
        kycIdProof: { type: String, default: null },
        chooseAddressId: String,
        KycAddressProof: { type: String, default: null },
        KycPhotoId: { type: String, default: null },
    },
    forgotEmailTokenDetails: {
        emailToken: String,
        emailTokenExpire: Date
    },
    tempField: String,
    tempField2: String,
}, {
    collection: 'user'
});
var contactUs = new mongoose.Schema({

    contactUsDetails: String,
    timestamp: {
        type: Date,
        'default': Date.now,
        index: true
    },


}, {
    collection: "contactUs"
});

var ContactUs = mongoose.model('contactUs', contactUs);
var User = mongoose.model('user', userSchema);
module.exports.User = User;
module.exports.ContactUs = ContactUs;