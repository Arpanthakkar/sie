const web3 = require('./web3')
const myToken = require('./build/contracts/BmtERC20Token.json')
const config = require('config')
const bmtAddress = config.get('common.BmtTokenAddress')

const instance = new web3.eth.Contract(
    JSON.parse(JSON.stringify(myToken.abi)),
    bmtAddress
)

module.exports = instance
