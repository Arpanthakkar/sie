const web3 = require('./web3')
const myToken = require('./build/contracts/OegERC20Token.json')
const config = require('config')
const oegAddress = config.get('common.OegTokenAddress')

const instance = new web3.eth.Contract(
    JSON.parse(JSON.stringify(myToken.abi)),
    oegAddress
)

module.exports = instance
