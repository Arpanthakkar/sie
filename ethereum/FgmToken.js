const web3 = require('./web3')
const myToken = require('./build/contracts/FgmERC20Token.json')
const config = require('config')
const fgmAddress = config.get('common.FgmTokenAddress')

const instance = new web3.eth.Contract(
    JSON.parse(JSON.stringify(myToken.abi)),
    fgmAddress
)

module.exports = instance