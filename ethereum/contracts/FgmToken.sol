pragma solidity 0.5.8;
import "@openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Pausable.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";
contract FgmERC20Token is ERC20Detailed, ERC20Pausable,  ERC20Burnable, Ownable {

    // mapping (address => bool) public kycUser;

    // modifier onlyAuthorized(){
    //     require(kycUser[msg.sender] || isOwner());
    //     _;
    // }
    constructor(
        string memory name,
        string memory symbol,
        uint8 decimals,
        uint256 totalSupply
    )
        public
        ERC20Detailed(name, symbol, decimals)
    {
        totalSupply = totalSupply * (10**uint256(decimals));
        if (totalSupply > 0) {
            _mint(owner(), totalSupply);
        }
    }

    function issueNewTokens( uint256 value) external onlyOwner {
        return _mint(owner(), value);
    }

    // function transfer(address to, uint256 value) public onlyAuthorized  returns (bool) {
    //     require(kycUser[to]);
    //     return super.transfer(to, value);
    // }

    // function addKycUser(address _toAdd) onlyOwner external {
    //     kycUser[_toAdd] = true;
    // }

    // function removeKycUser(address _toRemove) onlyOwner external {
    //     kycUser[_toRemove] = false;
    // }

    // function isKycUser(address _userAddress)  external view returns (bool) {
    //     return kycUser[_userAddress];
    // }

    function burn(uint256 amount) onlyOwner public{
        super.burn(amount);
    }
}
