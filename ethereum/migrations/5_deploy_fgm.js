const FgmToken = artifacts.require('FgmERC20Token')

module.exports = async function(deployer) {
    const name = "Free Global Marketplace"
    const symbol = "FGM"
    const decimals = 8
    const totalSupply = 100000000
  
    await deployer.deploy(FgmToken, name, symbol, decimals, totalSupply)
}