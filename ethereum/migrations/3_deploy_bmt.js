const BmtToken = artifacts.require('BmtERC20Token')

module.exports = async function(deployer) {
    const name = "Blue Morpho Token"
    const symbol = "BMT"
    const decimals = 8
    const totalSupply = 1000000000
  
    await deployer.deploy(BmtToken, name, symbol, decimals, totalSupply)
}