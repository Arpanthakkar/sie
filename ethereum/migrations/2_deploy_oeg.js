const OegToken = artifacts.require('OegERC20Token')

module.exports = async function(deployer) {
  const name = "One Energy Global"
  const symbol = "OEG"
  const decimals = 8
  const totalSupply = 100000000

  await deployer.deploy(OegToken, name, symbol, decimals, totalSupply)
}
