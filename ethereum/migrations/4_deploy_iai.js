const IaiToken = artifacts.require('IaiERC20Token')

module.exports = async function(deployer) {
    const name = "Infinite Artificial Intelligence"
    const symbol = "IAI"
    const decimals = 8
    const totalSupply = 100000000
  
    await deployer.deploy(IaiToken, name, symbol, decimals, totalSupply)
}